package com.palidinodh.nio;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import lombok.Getter;
import lombok.Setter;

public class Session {

  @Getter private SocketChannel socketChannel;
  @Getter private String remoteAddress;
  @Getter private long lastRead;
  @Getter @Setter private Object attachment;
  private SelectionKey selectionKey;
  private Queue<WriteEvent> writeEvents = new ConcurrentLinkedQueue<>();

  public Session(SocketChannel socketChannel, String remoteAddress, SelectionKey selectionKey) {
    this.socketChannel = socketChannel;
    this.remoteAddress = remoteAddress;
    this.selectionKey = selectionKey;
    selectionKey.attach(this);
    updateLastRead();
  }

  public void write(byte[] bytes) {
    addWriteEvent(new WriteEvent(bytes, 0, bytes.length, null));
  }

  public void write(byte[] bytes, WriteEventHandler handler) {
    addWriteEvent(new WriteEvent(bytes, 0, bytes.length, handler));
  }

  public void close() {
    try {
      socketChannel.close();
    } catch (IOException ioe) {
    }
  }

  public boolean isOpen() {
    return socketChannel.isOpen();
  }

  void addWriteEvent(WriteEvent writeEvent) {
    writeEvents.offer(writeEvent);
    if (!selectionKey.isValid()) {
      return;
    }
    selectionKey.selector().wakeup();
  }

  SelectionKey getSelectionKey() {
    return selectionKey;
  }

  boolean idleTimeout(int timeoutMillis) {
    return timeoutMillis > 0 && System.currentTimeMillis() - lastRead > timeoutMillis;
  }

  void updateLastRead() {
    lastRead = System.currentTimeMillis();
  }

  Queue<WriteEvent> getWriteEvents() {
    return writeEvents;
  }
}
