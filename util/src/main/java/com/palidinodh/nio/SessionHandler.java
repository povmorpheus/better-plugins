package com.palidinodh.nio;

public interface SessionHandler {

  default void accept(Session session) {}

  default void read(Session session, byte[] bytes) {}

  default void closed(Session session) {}

  default void error(Exception exception, Session session) {}
}
