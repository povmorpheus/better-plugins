package com.palidinodh.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.palidinodh.util.PLogger;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.security.AccessController;
import java.security.PrivilegedAction;
import lombok.Setter;

public class JsonIO {

  @Setter
  private static Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

  public static <T> T read(String src, Class<T> type) {
    if (src == null || src.isEmpty()) {
      return null;
    }
    try {
      return gson.fromJson(src, (Type) type);
    } catch (Exception e) {
      PLogger.error(e);
      return null;
    }
  }

  public static <T> T read(File file, Class<T> type) {
    try (Reader reader =
        (file.exists()
            ? new FileReader(file)
            : new InputStreamReader(Readers.getResourceAsStream(file), "UTF-8"))) {
      return gson.fromJson(reader, type);
    } catch (Exception e) {
      PLogger.error(e);
      return null;
    }
  }

  public static <T> T readPrivileged(String filename, Class<T> type) {
    return AccessController.doPrivileged(
        (PrivilegedAction<T>)
            () -> {
              File file =
                  filename.replace("\\", "/").startsWith(FileManager.JSON_DIR)
                      ? new File(filename)
                      : new File(FileManager.JSON_DIR, filename);
              if (file.getPath().contains("..")) {
                throw new IllegalArgumentException("File path can't go up levels");
              } else if (file.getPath().contains("settings")) {
                throw new IllegalArgumentException("File path can't contain settings");
              }
              try (Reader reader =
                  (file.exists()
                      ? new FileReader(file)
                      : new InputStreamReader(Readers.getResourceAsStream(file), "UTF-8"))) {
                return gson.fromJson(reader, type);
              } catch (Exception e) {
                PLogger.error(
                    "fromJsonFile: filename="
                        + filename
                        + ", file="
                        + file
                        + ", path="
                        + file.getPath()
                        + ", exists="
                        + file.exists());
                PLogger.error(e.getMessage(), e);
                return null;
              }
            });
  }

  public static <T> T readPrivileged(String filename, TypeToken<T> token) {
    return AccessController.doPrivileged(
        (PrivilegedAction<T>)
            () -> {
              File file =
                  filename.replace("\\", "/").startsWith(FileManager.JSON_DIR)
                      ? new File(filename)
                      : new File(FileManager.JSON_DIR, filename);
              if (file.getPath().contains("..")) {
                throw new IllegalArgumentException("File path can't go up levels");
              } else if (file.getPath().contains("settings")) {
                throw new IllegalArgumentException("File path can't contain settings");
              }
              try (Reader reader =
                  (file.exists()
                      ? new FileReader(file)
                      : new InputStreamReader(Readers.getResourceAsStream(file), "UTF-8"))) {
                return gson.fromJson(reader, token.getType());
              } catch (Exception e) {
                PLogger.error(
                    "fromJsonFile: filename="
                        + filename
                        + ", file="
                        + file
                        + ", path="
                        + file.getPath()
                        + ", exists="
                        + file.exists());
                PLogger.error(e.getMessage(), e);
                return null;
              }
            });
  }

  public static String write(Object src, Type type) {
    return gson.toJson(src, type);
  }

  public static void write(File file, Object src) {
    if (file.getParentFile() != null) {
      file.getParentFile().mkdirs();
    }
    try (Writer writer = new FileWriter(file)) {
      gson.toJson(src, writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
