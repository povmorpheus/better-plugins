package com.palidinodh.rs.adaptive.grandexchange;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class GrandExchangeHistoryItem {

  private int id;
  private int quantity;
  private int price;
  private GrandExchangeHistoryType type;
  private String username;

  public GrandExchangeHistoryItem(
      GrandExchangeItem item, GrandExchangeHistoryType type, String username) {
    id = item.getId();
    quantity = item.getAmount();
    price = item.getPrice();
    this.type = type;
    this.username = username;
  }
}
