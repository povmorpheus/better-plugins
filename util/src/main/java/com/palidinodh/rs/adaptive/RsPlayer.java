package com.palidinodh.rs.adaptive;

import com.palidinodh.rs.setting.UserRank;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
public class RsPlayer {

  public static final int RIGHTS_NONE = 0;
  public static final int RIGHTS_SILVER_CROWN = 1;
  public static final int RIGHTS_GOLD_CROWN = 2;
  public static final String SAVE_MAP_IP = "player.ip";
  public static final String SAVE_MAP_RANDOM_HASH = "player.randomHash";
  public static final String SAVE_MAP_MAC = "player.mac";
  public static final String SAVE_MAP_UUID = "player.uuid";
  public static final String SAVE_MAP_LOGIN_SECURITY = "player.loginSecurity";

  private int id;
  private String username;
  private String password;
  private String ip;
  private int worldId;
  @Setter private int rights;
  @Setter private int icon;
  @Setter private int icon2;
  @Setter private int privateChatStatus;
  private List<RsFriend> friends = new ArrayList<>();
  private List<String> ignores = new ArrayList<>();

  public RsPlayer(int id, String username, String password, String ip, int worldId) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.ip = ip;
    this.worldId = worldId;
  }

  public static int getRights(UserRank rank) {
    switch (rank) {
      case ADMINISTRATOR:
        return RIGHTS_GOLD_CROWN;
      case OVERSEER:
      case FORUM_MODERATOR:
      case MODERATOR:
      case SENIOR_MODERATOR:
      case ADVERTISEMENT_MANAGER:
      case COMMUNITY_MANAGER:
        return RIGHTS_SILVER_CROWN;
      default:
        return RIGHTS_NONE;
    }
  }

  public void loadFriends(List<RsFriend> f, List<String> i, int pcs) {
    friends = f;
    ignores = i;
    privateChatStatus = pcs;
  }

  public void loadFriend(RsFriend friend) {
    if (friends.contains(friend)) {
      friends.remove(friend);
    } else {
      friends.add(friend);
    }
  }

  public void loadFriendClanRank(RsFriend friend) {
    int indexOf = friends.indexOf(friend);
    if (indexOf != -1) {
      RsFriend orig = friends.get(indexOf);
      orig.setClanRank(friend.getClanRank());
    }
  }

  public void loadIgnore(String name) {
    if (ignores.contains(name.toLowerCase())) {
      ignores.remove(name.toLowerCase());
    } else {
      ignores.add(name.toLowerCase());
    }
  }
}
