package com.palidinodh.rs.adaptive.grandexchange;

import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
public class GrandExchangeItem implements Serializable {

  public static final int STATE_BUYING = 1;
  public static final int STATE_SELLING = 2;
  public static final int BUY_EXPIRATION = 7;
  public static final int SELL_EXPIRATION = 90;
  public static final int BOND = 13190;
  private static final long serialVersionUID = 12202016;

  private long creation;
  private int state;
  private String ip;
  @Setter private int id;
  private String name;
  private int amount;
  @Setter private int price;
  private int exchangedAmount;
  private int exchangedPrice;
  private int collectedAmount;
  private int collectedPrice;
  @Setter private boolean aborted;

  public GrandExchangeItem(int state, int id, int amount, int price) {
    this(state, "", id, "", amount, price);
  }

  public GrandExchangeItem(int state, String ip, int id, String name, int amount, int price) {
    creation = PTime.currentTimeMillis();
    this.state = state;
    this.ip = ip;
    this.id = id;
    this.name = name;
    this.amount = amount;
    this.price = price;
  }

  public int getCompletedPercent() {
    if (aborted) {
      return 100;
    }
    return (int) PRandom.getPercent(exchangedAmount, amount);
  }

  public int getRemainingAmount() {
    return Math.max(0, amount - exchangedAmount);
  }

  public int getReturnAmount() {
    if (isStateSelling() && !aborted) {
      return 0;
    }
    if (isStateBuying()) {
      return Math.max(0, exchangedAmount - collectedAmount);
    }
    if (isStateSelling()) {
      return Math.max(0, getRemainingAmount() - collectedAmount);
    }
    return 0;
  }

  public int getReturnCoins() {
    if (isStateBuying() && !aborted) {
      return 0;
    }
    if (isStateBuying()) {
      return Math.max(0, amount * price - exchangedPrice - collectedPrice);
    }
    if (isStateSelling()) {
      var tax = GrandExchangeTaxType.getTax(exchangedPrice);
      var taxedAmount = (int) ((exchangedPrice - collectedPrice) * tax);
      return Math.max(0, exchangedPrice - collectedPrice - taxedAmount);
    }
    return 0;
  }

  public int getTotalCoins() {
    if (isStateBuying() && !aborted) {
      return 0;
    }
    if (isStateBuying()) {
      return Math.max(0, amount * price - exchangedPrice - collectedPrice);
    }
    if (isStateSelling()) {
      return Math.max(0, exchangedPrice - collectedPrice);
    }
    return 0;
  }

  public boolean isExpired() {
    return PTime.betweenMilliToDay(creation)
        > (state == STATE_SELLING ? SELL_EXPIRATION : BUY_EXPIRATION);
  }

  public boolean isStateBuying() {
    return state == STATE_BUYING;
  }

  public boolean isStateSelling() {
    return state == STATE_SELLING;
  }

  public void setExchangedPrice(int exchangedPrice) {
    this.exchangedPrice = exchangedPrice;
    inspect();
  }

  public void increaseExchanged(int _amount, int _price) {
    exchangedAmount += _amount;
    exchangedPrice += _price * _amount;
    inspect();
    if (getRemainingAmount() == 0) {
      aborted = true;
    }
  }

  public void setExchanged(int _amount, int _price) {
    exchangedAmount += _amount;
    exchangedPrice += _price;
    inspect();
    if (getRemainingAmount() == 0) {
      aborted = true;
    }
  }

  public void collected(int _amount, int _price) {
    collectedAmount += _amount;
    collectedPrice += _price;
    inspect();
  }

  public void setCollectedAmount(int collectedAmount) {
    this.collectedAmount = collectedAmount;
    inspect();
  }

  public void setCollectedPrice(int collectedPrice) {
    this.collectedPrice = collectedPrice;
    inspect();
  }

  public boolean canRemove() {
    if (!aborted) {
      return false;
    }
    if (getReturnCoins() > 0) {
      return false;
    }
    return getReturnAmount() == 0;
  }

  public void inspect() {
    if (exchangedAmount < 0) {
      exchangedAmount = 0;
    }
    if (exchangedPrice < 0) {
      exchangedPrice = 0;
    }
    if (collectedAmount < 0) {
      collectedAmount = 0;
    }
    if (collectedPrice < 0) {
      collectedPrice = 0;
    }
  }

  public boolean matches(GrandExchangeItem item) {
    if (item == null) {
      return false;
    }
    if (state != item.getState()) {
      return false;
    }
    if (aborted != item.isAborted()) {
      return false;
    }
    if (amount != item.getAmount()) {
      return false;
    }
    if (exchangedAmount != item.getExchangedAmount()) {
      return false;
    }
    if (exchangedPrice != item.getExchangedPrice()) {
      return false;
    }
    if (collectedAmount != item.getCollectedAmount()) {
      return false;
    }
    if (collectedPrice != item.getCollectedPrice()) {
      return false;
    }
    return id == item.getId();
  }
}
