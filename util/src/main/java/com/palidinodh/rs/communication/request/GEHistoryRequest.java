package com.palidinodh.rs.communication.request;

import com.palidinodh.rs.communication.ServerSession;
import lombok.Getter;

@Getter
public class GEHistoryRequest extends Request {

  private int userId;
  private int itemId;
  private String itemName;
  private String username;

  public GEHistoryRequest(
      ServerSession session, int key, int userId, int itemId, String itemName, String username) {
    super(session, key);
    this.userId = userId;
    this.itemId = itemId;
    this.itemName = itemName;
    this.username = username;
  }
}
