package com.palidinodh.rs.communication.request;

import com.palidinodh.rs.communication.ServerSession;

public class VoteRequest extends Request {

  public enum Type {
    GET,
    REMOVE
  }

  private Type type;
  private int userId;
  private String result;

  public VoteRequest(ServerSession session, int key, Type type, int userId) {
    super(session, key);
    this.type = type;
    this.userId = userId;
  }

  public Type getType() {
    return type;
  }

  public int getUserId() {
    return userId;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }
}
