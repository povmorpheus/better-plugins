package com.palidinodh.rs.setting;

public enum DiscordChannel {
  ANNOUNCEMENTS,
  GENERAL_CHAT,
  EVENTS,
  ACHIEVEMENTS,
  CLAN_CHAT,
  YELL_CHAT_LOG,
  MODERATION_LOG,
  ALERT_LOG,
  NEW_ACCOUNT_LOG
}
