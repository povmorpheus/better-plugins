package com.palidinodh.rs.setting;

import lombok.Getter;

@Getter
public class SqlCustomUserFields {

  private String referral;
  private String pendingBonds;
  private String pendingVotePoints;
  private String voteTimeRunelocus;
  private String voteTimeTopg;
}
