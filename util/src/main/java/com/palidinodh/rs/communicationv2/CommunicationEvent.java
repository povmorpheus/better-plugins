package com.palidinodh.rs.communicationv2;

import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Writers;
import com.palidinodh.nio.WriteEventHandler;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PTime;
import java.nio.ByteBuffer;
import java.util.function.Consumer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
public abstract class CommunicationEvent {

  private static final long WORLD_WRITE_DELAY = 30_000;
  private static int uniqueKey;

  private transient MainCommunicationServer mainServer;
  private transient CommunicationSession mainSession;
  private transient WriteEventHandler writeHandler;
  private transient WorldCommunicationServer worldServer;
  private transient CommunicationEvent updatedEvent;
  private transient Object worldObject;
  private transient Consumer<CommunicationEvent> worldAction;
  private transient long worldWriteTime;
  private int key = getUniqueKey();
  private boolean complete;

  private static synchronized int getUniqueKey() {
    return uniqueKey++;
  }

  public final boolean isComplete() {
    var newestEvent = getNewestEvent();
    return this == newestEvent ? complete : newestEvent.isComplete();
  }

  public final CommunicationEvent getNewestEvent() {
    if (updatedEvent == null) {
      return this;
    }
    return updatedEvent;
  }

  protected abstract void mainAction();

  protected final void loadMain(MainCommunicationServer _server, CommunicationSession _session) {
    mainServer = _server;
    mainSession = _session;
  }

  protected final void loadWorld(WorldCommunicationServer _server) {
    worldServer = _server;
  }

  protected final void mainComplete() {
    mainAction();
    complete = true;
    writeMain();
  }

  protected final <T extends CommunicationEvent> boolean worldComplete() {
    var event = getNewestEvent();
    event.loadWorld(worldServer);
    event.setWorldAction(worldAction);
    if (!event.isComplete()) {
      return false;
    }
    complete = true;
    if (worldAction != null) {
      worldAction.accept(event);
    }
    return true;
  }

  protected void writeWorld() {
    if (worldWriteTime != 0 && PTime.currentTimeMillis() - worldWriteTime < WORLD_WRITE_DELAY) {
      return;
    }
    worldWriteTime = PTime.currentTimeMillis();
    var clazz = getClass();
    var className = clazz.getName();
    var json = Writers.gzCompress(JsonIO.write(this, clazz).getBytes());
    var out = worldServer.getOut();
    out.clear();
    out.writeInt((className.length() + 1) + (json.length + 4));
    out.writeString(className);
    out.writeInt(json.length);
    out.writeBytes(json);
    try {
      worldServer.getSocket().write(ByteBuffer.wrap(out.toByteArray()));
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  private void writeMain() {
    var clazz = getClass();
    var className = clazz.getName();
    var json = Writers.gzCompress(JsonIO.write(this, clazz).getBytes());
    var out = mainSession.getOut();
    out.writeInt((className.length() + 1) + (json.length + 4));
    out.writeString(className);
    out.writeInt(json.length);
    out.writeBytes(json);
    mainSession.write();
  }
}
