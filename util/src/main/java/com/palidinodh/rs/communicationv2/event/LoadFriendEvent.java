package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.CommunicationEvent;
import lombok.Getter;

@Getter
public class LoadFriendEvent extends CommunicationEvent {

  private int userId;
  private RsFriend friend;

  public LoadFriendEvent(int userId, RsFriend friend) {
    this.userId = userId;
    this.friend = friend;
  }

  @Override
  public void mainAction() {
    var player = getMainServer().getPlayersById().get(userId);
    if (player == null) {
      return;
    }
    player.loadFriend(friend);
    var clan = getMainServer().getClan(player);
    var friendPlayer =
        getMainServer().getPlayersByUsername().get(friend.getUsername().toLowerCase());
    if (friendPlayer != null
        && (player.getRights() != RsPlayer.RIGHTS_NONE
            || RsFriend.canRegister(player, friendPlayer))) {
      friend.setWorldId(friendPlayer.getWorldId());
    } else {
      friend.setWorldId(0);
    }
    if (!getMainServer().getClanUpdates().contains(clan)) {
      getMainServer().getClanUpdates().add(clan);
    }
  }
}
