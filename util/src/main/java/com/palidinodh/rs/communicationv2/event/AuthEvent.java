package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.CommunicationEvent;
import com.palidinodh.rs.setting.Settings;
import java.util.List;
import lombok.Getter;

@Getter
public class AuthEvent extends CommunicationEvent {

  private int id;
  private String password;
  private List<RsPlayer> players;

  public AuthEvent(int id, String password, List<RsPlayer> players) {
    this.id = id;
    this.password = password;
    this.players = players;
  }

  @Override
  public void mainAction() {
    var server = getMainServer();
    var worlds = server.getWorlds();
    var session = getMainSession();
    if (!session.isAuthenticated()) {
      session.close();
      return;
    }
    var world = worlds.get(id);
    if (world != null && world.isOpen()) {
      session.close();
      return;
    }
    synchronized (getMainServer()) {
      worlds.put(id, session);
      players.forEach(
          p -> {
            server.getPlayersById().put(p.getId(), p);
            server.getPlayersByUsername().put(p.getUsername().toLowerCase(), p);
          });
    }
  }

  public boolean isAuthenticated() {
    return Settings.getSecure().getPassword().equals(password);
  }
}
