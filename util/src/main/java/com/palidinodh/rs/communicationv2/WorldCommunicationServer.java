package com.palidinodh.rs.communicationv2;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Readers;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.event.AuthEvent;
import com.palidinodh.rs.setting.SecureSettings;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PTime;
import com.palidinodh.util.PUtil;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import lombok.Getter;

@Getter
public class WorldCommunicationServer implements Runnable {

  private static final int CONNECT_DELAY = 10_000;

  @Getter private static WorldCommunicationServer instance;

  private int head;
  private int tail;
  private boolean running;
  private InetSocketAddress address;
  private SocketChannel socket;
  private long lastConnectTime;
  private ByteBuffer byteBuffer = ByteBuffer.allocate(1_024);
  private Stream in = new Stream(32_767);
  private Stream out = new Stream(32_767);
  private int worldId;
  private String password;
  private Object worldLock;
  private ConcurrentLinkedQueue<CommunicationEvent> events = new ConcurrentLinkedQueue<>();
  private ConcurrentLinkedQueue<CommunicationNotification> notifications =
      new ConcurrentLinkedQueue<>();
  // RSPLAYERS TODO
  private List<RsPlayer> players = new ArrayList<>();
  private AuthEvent authEvent;

  private WorldCommunicationServer(SecureSettings settings, Object worldLock, int worldId) {
    address = new InetSocketAddress(settings.getCommunicationIp(), settings.getCommunicationPort());
    this.worldLock = worldLock;
    this.worldId = worldId;
    password = settings.getPassword();
    running = true;
  }

  public static void init(SecureSettings settings, Object worldLock, int world) {
    instance = new WorldCommunicationServer(settings, worldLock, world);
  }

  @Override
  public void run() {
    var completed = new ArrayList<CommunicationEvent>();
    while (running) {
      PUtil.gc();
      PTime.update();
      connect();
      read();
      if (socket != null) {
        completed.forEach(CommunicationEvent::writeWorld);
        synchronized (worldLock) {
          completed.removeIf(e -> !e.worldComplete());
        }
      }
      synchronized (this) {
        events.removeAll(completed);
        completed.clear();
        try {
          wait(200);
          completed.addAll(events);
          head = tail;
        } catch (InterruptedException ie) {
          break;
        }
      }
    }
  }

  public <T extends CommunicationEvent> T addEvent(T event) {
    return addEvent(event, null);
  }

  public <T extends CommunicationEvent> T addEvent(T event, Consumer<T> consumer) {
    if (event instanceof AuthEvent) {
      return null;
    }
    event.loadWorld(this);
    event.setWorldAction((Consumer<CommunicationEvent>) consumer);
    synchronized (this) {
      events.add(event);
      tail++;
      notify();
    }
    return (T) event;
  }

  private void connect() {
    if (socket != null) {
      return;
    }
    if (lastConnectTime != 0 && PTime.currentTimeMillis() - lastConnectTime < CONNECT_DELAY) {
      return;
    }
    lastConnectTime = PTime.currentTimeMillis();
    disconnect();
    authEvent = new AuthEvent(worldId, password, players);
    authEvent.loadWorld(this);
    try {
      socket = SocketChannel.open(address);
      socket.configureBlocking(false);
    } catch (IOException e) {
      socket = null;
      PLogger.error(e);
      return;
    }
    authEvent.writeWorld();
  }

  private void disconnect() {
    if (socket != null) {
      try {
        socket.close();
      } catch (Exception e) {
      }
    }
    socket = null;
    in.clear();
    out.clear();
  }

  private void read() {
    if (socket == null) {
      return;
    }
    try {
      int read;
      do {
        byteBuffer.clear();
        read = socket.read(byteBuffer);
        if (read > 0) {
          byteBuffer.flip();
          in.appendBytes(byteBuffer.array(), 0, read);
        }
      } while (read > 0);
      while (in.available() >= 10) {
        in.mark();
        var length = in.readInt();
        if (length > in.available()) {
          in.reset();
          break;
        }
        var className = in.readString();
        var json = new String(Readers.gzDecompress(in.readBytes(in.readInt())));
        var jsonObject = JsonIO.read(json, Class.forName(className));
        if (jsonObject instanceof CommunicationEvent) {
          CommunicationEvent event = (CommunicationEvent) jsonObject;
          CommunicationEvent existingEvent;
          synchronized (this) {
            existingEvent = PArrayList._getIf(events, e -> event.getKey() == e.getKey());
          }
          if (existingEvent != null) {
            existingEvent.setUpdatedEvent(event);
          }
        } else if (jsonObject instanceof CommunicationNotification) {
          notifications.add((CommunicationNotification) jsonObject);
        } else {
          disconnect();
          break;
        }
      }
    } catch (Exception e) {
      disconnect();
      PLogger.error(e);
    }
  }
}
