package com.palidinodh.rs.communicationv2.notification;

import com.palidinodh.rs.communicationv2.CommunicationNotification;
import lombok.Getter;

@Getter
public class PrivateMessageNotification extends CommunicationNotification {

  private String senderUsername;
  private int senderIcon;
  private int receiverUserId;
  private String message;

  public PrivateMessageNotification(
      int world, String senderUsername, int senderIcon, int receiverUserId, String message) {
    super(world);
    this.senderUsername = senderUsername;
    this.senderIcon = senderIcon;
    this.receiverUserId = receiverUserId;
    this.message = message;
  }
}
