package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.io.Writers;
import com.palidinodh.rs.communicationv2.CommunicationEvent;
import com.palidinodh.rs.communicationv2.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import java.io.File;
import lombok.Getter;

@Getter
public class LogoutEvent extends CommunicationEvent {

  private int userId;
  private String username;
  private byte[] playerFile;

  public LogoutEvent(int userId, String username, byte[] playerFile) {
    this.userId = userId;
    this.username = username;
    this.playerFile = playerFile;
  }

  @Override
  public void mainAction() {
    var player = getMainServer().getPlayersById().get(userId);
    var fileName = Integer.toString(userId);
    if (Settings.getInstance().isLocal()) {
      fileName = username.toLowerCase();
    }
    if (player != null) {
      if (playerFile != null && playerFile.length > 0) {
        var playerDirectory = Settings.getInstance().getPlayerMapDirectory();
        Writers.writeFile(new File(playerDirectory, fileName + ".dat"), playerFile);
      }
      synchronized (MainCommunicationServer.getInstance()) {
        getMainServer().getPlayersById().remove(player.getId());
        getMainServer().getPlayersByUsername().remove(player.getUsername().toLowerCase());
      }
    } else {
      if (playerFile != null && playerFile.length > 0) {
        var playerDirectory = Settings.getInstance().getPlayerMapErrorDirectory();
        Writers.writeFile(new File(playerDirectory, fileName + ".dat"), playerFile);
      }
    }
  }
}
