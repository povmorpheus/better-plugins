package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.CommunicationEvent;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class LoadFriendsEvent extends CommunicationEvent {

  private int userId;
  private int privateChatState;
  private List<RsFriend> friends;
  private List<String> ignores;
  private List<RsFriend> friendsOnline;

  public LoadFriendsEvent(
      int userId, int privateChatState, List<RsFriend> friends, List<String> ignores) {
    this.userId = userId;
    this.privateChatState = privateChatState;
    this.friends = friends;
    this.ignores = ignores;
  }

  @Override
  public void mainAction() {
    var player = getMainServer().getPlayersById().get(userId);
    if (player == null) {
      return;
    }
    player.loadFriends(friends, ignores, privateChatState);
    getMainServer().getClan(player);
    friendsOnline = new ArrayList<>();
    var players = new ArrayList<>(getMainServer().getPlayersById().values());
    for (RsPlayer rp : players) {
      RsFriend friend = new RsFriend(rp.getUsername());
      if (!friends.contains(friend)) {
        continue;
      }
      if (player.getRights() == RsPlayer.RIGHTS_NONE && !RsFriend.canRegister(player, rp)) {
        continue;
      }
      friendsOnline.add(new RsFriend(rp.getUsername(), rp.getWorldId()));
    }
  }
}
