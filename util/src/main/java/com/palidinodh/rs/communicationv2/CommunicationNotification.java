package com.palidinodh.rs.communicationv2;

import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Writers;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter(AccessLevel.PROTECTED)
public class CommunicationNotification {

  private transient MainCommunicationServer server;
  private transient int world = -1;

  public CommunicationNotification(int world) {
    this.world = world;
  }

  protected final void load(MainCommunicationServer _server) {
    server = _server;
  }

  protected final void write(CommunicationSession session) {
    if (world != -1 && world != session.getWorldId()) {
      return;
    }
    var clazz = getClass();
    var className = clazz.getName();
    var json = Writers.gzCompress(JsonIO.write(this, clazz).getBytes());
    var out = session.getOut();
    out.writeInt((className.length() + 1) + (json.length + 4));
    out.writeString(className);
    out.writeInt(json.length);
    out.writeBytes(json);
    session.write();
  }
}
