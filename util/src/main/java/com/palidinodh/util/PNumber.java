package com.palidinodh.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PNumber {

  private static List<Matcher> decimalMatchers =
      PCollection.toList(
          Pattern.compile("\\.([0]*[1-9]{1})").matcher(""),
          Pattern.compile("\\.([0]*[1-9]{1,2})").matcher(""),
          Pattern.compile("\\.([0]*[1-9]{1,3})").matcher(""),
          Pattern.compile("\\.([0]*[1-9]{1,4})").matcher(""),
          Pattern.compile("\\.([0]*[1-9]{1,5})").matcher(""));

  public static double reduceDecimals(double value, int max) {
    if (max <= 0 || value == Math.floor(value)) {
      return Math.floor(value);
    }
    String toString = Double.toString(value);
    if (toString.matches(".*[A-Z]+.*")) {
      return round(value, max);
    }
    Matcher matcher =
        max < decimalMatchers.size()
            ? decimalMatchers.get(max).reset(toString)
            : Pattern.compile("\\.([0]*[1-9]{1," + max + "})").matcher(toString);
    return matcher.find() ? round(value, matcher.group(1).length()) : value;
  }

  public static int greatestCommonDenominator(int a, int b) {
    return b == 0 ? a : greatestCommonDenominator(b, a % b);
  }

  public static double formatDouble1(double d) {
    return formatDouble(d, 1);
  }

  public static double formatDouble2(double d) {
    return formatDouble(d, 2);
  }

  public static double formatDouble(double d, int decimalPlaces) {
    if (d == Math.floor(d)) {
      return d;
    }
    String value = String.valueOf(d);
    if (value.contains("E")) {
      return 0;
    }
    String decimal = value.substring(value.indexOf(".") + 1);
    if (decimal.length() > decimalPlaces) {
      String whole = value.substring(0, value.indexOf("."));
      decimal = decimal.substring(0, decimalPlaces);
      return Double.parseDouble(whole + "." + decimal);
    }
    return d;
  }

  public static String formatNumber(long i) {
    String s = String.valueOf(i);
    for (int k = s.length() - 3; k > 0; k -= 3) {
      s = s.substring(0, k) + "," + s.substring(k);
    }
    return s;
  }

  public static String abbreviateNumber(long i) {
    String s = String.valueOf(i);
    for (int k = s.length() - 3; k > 0; k -= 3) {
      s = s.substring(0, k) + "," + s.substring(k);
    }
    if (s.length() > 12) {
      int decimalValue = Integer.parseInt(s.substring(s.length() - 11, s.length() - 10));
      if (decimalValue > 0) {
        s = s.substring(0, s.length() - 12) + "." + decimalValue + "B";
      } else {
        s = s.substring(0, s.length() - 12) + "B";
      }
    } else if (s.length() > 8) {
      int decimalValue = Integer.parseInt(s.substring(s.length() - 7, s.length() - 6));
      if (decimalValue > 0) {
        s = s.substring(0, s.length() - 8) + "." + decimalValue + "M";
      } else {
        s = s.substring(0, s.length() - 8) + "M";
      }
    } else if (s.length() > 4) {
      s = s.substring(0, s.length() - 4) + "K";
    }
    return s;
  }

  public static int getNumber(String text) {
    switch (text.charAt(text.length() - 1)) {
      case 'k':
      case 'K':
        text = text.substring(0, text.length() - 1) + "000";
        break;
      case 'm':
      case 'M':
        text = text.substring(0, text.length() - 1) + "000000";
        break;
    }
    return Integer.parseInt(text);
  }

  public static double round(double value, int places) {
    return new BigDecimal(Double.toString(value))
        .setScale(places, RoundingMode.HALF_UP)
        .doubleValue();
  }

  public static int round(int value) {
    if (value < 10) {
      return value;
    }
    if (value > 100000000) {
      return (int) (Math.round(value / 10000000.0) * 10000000);
    } else if (value > 10000000) {
      return (int) (Math.round(value / 1000000.0) * 1000000);
    } else if (value > 1000000) {
      return (int) (Math.round(value / 100000.0) * 100000);
    } else if (value > 100000) {
      return (int) (Math.round(value / 10000.0) * 10000);
    } else if (value > 10000) {
      return (int) (Math.round(value / 1000.0) * 1000);
    } else if (value > 1000) {
      return (int) (Math.round(value / 100.0) * 100);
    }
    return (int) (Math.round(value / 10.0) * 10);
  }

  public static int fullRound(int value) {
    if (value > 100000000) {
      return (int) (Math.round(value / 100000000.0) * 100000000);
    } else if (value > 10000000) {
      return (int) (Math.round(value / 10000000.0) * 10000000);
    } else if (value > 1000000) {
      return (int) (Math.round(value / 1000000.0) * 1000000);
    } else if (value > 100000) {
      return (int) (Math.round(value / 100000.0) * 100000);
    } else if (value > 10000) {
      return (int) (Math.round(value / 10000.0) * 10000);
    } else if (value > 1000) {
      return (int) (Math.round(value / 1000.0) * 1000);
    } else if (value > 100) {
      return (int) (Math.round(value / 100.0) * 100);
    }
    return (int) (Math.round(value / 10.0) * 10);
  }

  public static int fullCeil(int value) {
    if (value > 100000000) {
      return (int) (Math.ceil(value / 100000000.0) * 100000000);
    } else if (value > 10000000) {
      return (int) (Math.ceil(value / 10000000.0) * 10000000);
    } else if (value > 1000000) {
      return (int) (Math.ceil(value / 1000000.0) * 1000000);
    } else if (value > 100000) {
      return (int) (Math.ceil(value / 100000.0) * 100000);
    } else if (value > 10000) {
      return (int) (Math.ceil(value / 10000.0) * 10000);
    } else if (value > 1000) {
      return (int) (Math.ceil(value / 1000.0) * 1000);
    } else if (value > 100) {
      return (int) (Math.ceil(value / 100.0) * 100);
    }
    return (int) (Math.ceil(value / 10.0) * 10);
  }

  public static double addDoubles(double... input) {
    if (input.length == 0) {
      return 0;
    } else if (input.length == 1) {
      return input[0];
    }
    double sum = input[0];
    double c = 0;
    for (int i = 1; i < input.length; i++) {
      double t = sum + input[i];
      if (Math.abs(sum) >= Math.abs(input[i])) {
        c += sum - t + input[i];
      } else {
        c += input[i] - t + sum;
      }
      sum = t;
    }
    return sum + c;
  }

  public static boolean canAddInt(int initial, int adding) {
    return canAddInt(initial, adding, Integer.MAX_VALUE);
  }

  public static boolean canAddInt(int initial, int adding, int max) {
    return getAddIntLimit(initial, adding, max) == adding;
  }

  public static int getAddIntLimit(int initial, int adding) {
    return getAddIntLimit(initial, adding, Integer.MAX_VALUE);
  }

  public static int getAddIntLimit(int initial, int adding, int max) {
    if (adding > max - initial) {
      adding = max - initial;
    }
    int added = initial + adding;
    if (((initial ^ added) & (adding ^ added)) < 0 || added > max) {
      return 0;
    }
    return adding;
  }

  public static int addInt(int initial, int adding) {
    return addInt(initial, adding, Integer.MAX_VALUE);
  }

  public static int addInt(int initial, int adding, int max) {
    if (initial >= max || adding >= max) {
      return max;
    }
    if (adding > max - initial) {
      adding = max - initial;
    }
    int addedResult = initial + adding;
    if (((initial ^ addedResult) & (adding ^ addedResult)) < 0 || addedResult > max) {
      return max;
    }
    return addedResult;
  }

  public static boolean canMultiplyInt(int initial, int multiplying) {
    return canMultiplyInt(initial, multiplying, Integer.MAX_VALUE);
  }

  public static boolean canMultiplyInt(int initial, int multiplying, int max) {
    if (initial >= max || multiplying >= max) {
      return false;
    }
    long multiplied = (long) initial * multiplying;
    return multiplied == (int) multiplied && multiplied <= max;
  }

  public static int multiplyInt(int initial, int multiplying) {
    return multiplyInt(initial, multiplying, Integer.MAX_VALUE);
  }

  public static int multiplyInt(int initial, int multiplying, int max) {
    if (initial >= max || multiplying >= max) {
      return max;
    }
    if (multiplying > max / initial) {
      multiplying = max / initial;
    }
    long multipliedResult = (long) initial * multiplying;
    int returnableMultipledResult = (int) multipliedResult;
    if (multipliedResult != returnableMultipledResult || multipliedResult > max) {
      return max;
    }
    return returnableMultipledResult;
  }
}
