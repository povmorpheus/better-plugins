package com.palidinodh.util;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

public class PPolygon {

  private static final double BIG_VALUE = java.lang.Double.MAX_VALUE / 10.0;

  private Point2D[] points;
  private Rectangle bounds;

  public PPolygon(Point2D... points) {
    this.points = points;
  }

  public PPolygon(int[] xPoints, int[] yPoints) {
    points = new Point2D[xPoints.length];
    for (var i = 0; i < xPoints.length; i++) {
      points[i] = getPoint(xPoints[i], yPoints[i]);
    }
  }

  public PPolygon(int[][] points) {
    this.points = new Point2D[points.length];
    for (var i = 0; i < points.length; i++) {
      this.points[i] = getPoint(points[i][0], points[i][1]);
    }
  }

  public static Point2D getPoint(double x, double y) {
    return new Point2D.Double(x, y);
  }

  public boolean contains(Point p) {
    return contains(p.x, p.y);
  }

  public boolean contains(Point2D p) {
    return contains((int) p.getX(), (int) p.getY());
  }

  public boolean contains(int x, int y) {
    if (points.length <= 2 || !getBounds().contains(x, y)) {
      return false;
    }
    var pointCount = points.length;
    var hits = 0;
    var lastx = (int) points[pointCount - 1].getX();
    var lasty = (int) points[pointCount - 1].getY();
    int curx;
    int cury;
    for (var i = 0; i < pointCount; lastx = curx, lasty = cury, i++) {
      curx = (int) points[i].getX();
      cury = (int) points[i].getY();
      if (cury == lasty) {
        continue;
      }
      int leftx;
      if (curx < lastx) {
        if (x >= lastx) {
          continue;
        }
        leftx = curx;
      } else {
        if (x >= curx) {
          continue;
        }
        leftx = lastx;
      }
      double test1;
      double test2;
      if (cury < lasty) {
        if (y < cury || y >= lasty) {
          continue;
        }
        if (x < leftx) {
          hits++;
          continue;
        }
        test1 = x - curx;
        test2 = y - cury;
      } else {
        if (y < lasty || y >= cury) {
          continue;
        }
        if (x < leftx) {
          hits++;
          continue;
        }
        test1 = x - lastx;
        test2 = y - lasty;
      }
      if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
        hits++;
      }
    }
    return ((hits & 1) != 0);
  }

  private Rectangle getBounds() {
    if (points.length == 0) {
      return new Rectangle();
    }
    if (bounds == null) {
      calculateBounds();
    }
    return bounds.getBounds();
  }

  private void calculateBounds() {
    var boundsMinX = Integer.MAX_VALUE;
    var boundsMinY = Integer.MAX_VALUE;
    var boundsMaxX = Integer.MIN_VALUE;
    var boundsMaxY = Integer.MIN_VALUE;
    for (var point : points) {
      var x = (int) point.getX();
      boundsMinX = Math.min(boundsMinX, x);
      boundsMaxX = Math.max(boundsMaxX, x);
      var y = (int) point.getY();
      boundsMinY = Math.min(boundsMinY, y);
      boundsMaxY = Math.max(boundsMaxY, y);
    }
    bounds =
        new Rectangle(boundsMinX, boundsMinY, boundsMaxX - boundsMinX, boundsMaxY - boundsMinY);
  }
}
