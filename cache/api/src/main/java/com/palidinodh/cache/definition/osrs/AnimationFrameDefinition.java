package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import lombok.Getter;

@Getter
public class AnimationFrameDefinition implements Definition {
  private static final AnimationFrameDefinition DEFAULT = new AnimationFrameDefinition(-1);
  private static final int[] INDEX_FRAME_IDS = new int[500];
  private static final int[] SCRATCH_TRANSLATOR_X = new int[500];
  private static final int[] SCRATCH_TRANSLATOR_Y = new int[500];
  private static final int[] SCRATCH_TRANSLATOR_Z = new int[500];

  @Getter private static AnimationFrameDefinition[] definitions;

  private int id;
  private int skeletonId = -1;
  private int[] indexFrameIds;
  private int[] translatorX;
  private int[] translatorY;
  private int[] translatorZ;
  private int translatorCount = -1;
  private boolean showing;

  public AnimationFrameDefinition(int id) {
    this.id = id;
  }

  public static AnimationFrameDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static synchronized void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var archives = store.getIndex(IndexType.ANIMATION).getArchives();
      definitions = new AnimationFrameDefinition[archives.get(archives.size() - 1).getId() + 1];
      for (var archive : archives) {
        var def = definitions[archive.getId()] = new AnimationFrameDefinition(archive.getId());
        var file = archive.loadFiles().get(0);
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return id + "-" + skeletonId;
  }

  @Override
  public void load(Stream stream) {
    skeletonId = (stream.getArray()[0] & 255) << 8 | stream.getArray()[1] & 255;
    AnimationFrameSkeletonDefinition map = null;
    if (map == null) {
      return;
    }
    Stream bitsIn = new Stream(stream.getArray());
    Stream dataIn = new Stream(stream.getArray());
    int framemapArchiveIndex = bitsIn.readUnsignedShort();
    int length = bitsIn.readUnsignedByte();
    dataIn.skip(3 + length); // framemapArchiveIndex + length + data
    int lastI = -1;
    int index = 0;
    for (int i = 0; i < length; i++) {
      int translatorBits = bitsIn.readUnsignedByte();
      if (translatorBits <= 0) {
        continue;
      }
      if (map.getTypes()[i] != 0) {
        for (int i2 = i - 1; i2 > lastI; i2--) {
          if (map.getTypes()[i2] == 0) {
            INDEX_FRAME_IDS[index] = i2;
            SCRATCH_TRANSLATOR_X[index] = 0;
            SCRATCH_TRANSLATOR_Y[index] = 0;
            SCRATCH_TRANSLATOR_Z[index] = 0;
            index++;
            break;
          }
        }
      }
      INDEX_FRAME_IDS[index] = i;
      short defaultTranslator = 0;
      if (map.getTypes()[i] == 3) {
        defaultTranslator = 128;
      }
      if ((translatorBits & 1) == 0) {
        SCRATCH_TRANSLATOR_X[index] = defaultTranslator;
      } else {
        SCRATCH_TRANSLATOR_X[index] = dataIn.readSmart();
      }
      if ((translatorBits & 2) == 0) {
        SCRATCH_TRANSLATOR_Y[index] = defaultTranslator;
      } else {
        SCRATCH_TRANSLATOR_Y[index] = dataIn.readSmart();
      }
      if ((translatorBits & 4) == 0) {
        SCRATCH_TRANSLATOR_Z[index] = defaultTranslator;
      } else {
        SCRATCH_TRANSLATOR_Z[index] = dataIn.readSmart();
      }
      lastI = i;
      index++;
      if (map.getTypes()[i] == 5) {
        showing = true;
      }
    }
    if (dataIn.getPosition() != stream.getArray().length) {
      throw new RuntimeException("Bad AnimationFrameDefinition position and length");
    }
    translatorCount = index;
    indexFrameIds = new int[index];
    translatorX = new int[index];
    translatorY = new int[index];
    translatorZ = new int[index];
    for (int i = 0; i < index; i++) {
      indexFrameIds[i] = INDEX_FRAME_IDS[i];
      translatorX[i] = SCRATCH_TRANSLATOR_X[i];
      translatorY[i] = SCRATCH_TRANSLATOR_Y[i];
      translatorZ[i] = SCRATCH_TRANSLATOR_Z[i];
    }
  }

  @Override
  public Stream save(Stream stream) {
    return null;
  }

  @Override
  public Definition getDefaultDefinition() {
    return DEFAULT;
  }

  @Override
  public AnimationFrameDefinition[] allDefinitions() {
    return definitions;
  }
}
