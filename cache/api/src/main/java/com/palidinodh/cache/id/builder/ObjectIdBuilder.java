package com.palidinodh.cache.id.builder;

import com.palidinodh.cache.definition.osrs.ObjectDefinition;
import com.palidinodh.cache.id.NameIdLookup;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.cache.id.ObjectId;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ObjectIdBuilder {

  public static void build() {
    var namesUsed = new ArrayList<String>();
    var nullFields = new LinkedHashMap<Integer, String>();
    var nonNullFields = new LinkedHashMap<Integer, String>();
    for (var def : ObjectDefinition.getDefinitions()) {
      if (def == null) {
        continue;
      }
      var objectId = def.getId();
      if (def.getName() == null
          || def.getName().contains("null")
          || def.getName().contains("Null")) {
        nullFields.put(objectId, "NULL_" + objectId);
        continue;
      }
      var name = def.getName();
      name = name.replace("+", "_plus").replace("(-)", "_minus");
      name = name.replace("(", "_").replace(" ", "_").replace("-", "_");
      while (name.endsWith("_")) {
        name = name.substring(0, name.length() - 1);
      }
      name = name.replace("/", "_");
      name = IdBuilder.removeHtml(name);
      name = IdBuilder.cleanName(name);
      name = name.toUpperCase();
      if (name.matches("[0-9]_*[a-zA-z0-9]*")) {
        name = "_" + name;
      }
      while (name.contains("__")) {
        name = name.replace("__", "_");
      }
      if (name.length() == 0) {
        continue;
      }
      if (objectId >= 65000 || namesUsed.contains(name)) {
        name = name + "_" + objectId;
      }
      nonNullFields.put(objectId, name);
      namesUsed.add(name);
    }
    var lookup = new NameIdLookup(ObjectId.class);
    lookup.generateClass(nonNullFields, IdBuilder.TO_DIR);
    lookup = new NameIdLookup(NullObjectId.class);
    lookup.generateClass(nullFields, IdBuilder.TO_DIR);
  }
}
