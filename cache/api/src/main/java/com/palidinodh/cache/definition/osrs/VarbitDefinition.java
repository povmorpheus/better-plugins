package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import lombok.Getter;

@Getter
public class VarbitDefinition implements Definition {

  private static final VarbitDefinition DEFAULT = new VarbitDefinition(-1);

  @Getter private static VarbitDefinition[] definitions;

  private int id;
  private int varpId = -1;
  private int leastSignificantBit;
  private int mostSignificantBit;

  public VarbitDefinition(int id) {
    this.id = id;
  }

  public static void printDetails() {
    if (definitions == null) {
      return;
    }
    for (var def : definitions) {
      if (def == null) {
        continue;
      }
      System.out.println(
          "varbit id: "
              + def.getId()
              + ", varp: "
              + def.getVarpId()
              + ", value: "
              + def.getValue(0, 1)
              + ", least: "
              + def.getLeastSignificantBit()
              + ", most: "
              + def.getMostSignificantBit());
    }
  }

  public static VarbitDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static synchronized void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.VARBIT);
      definitions = new VarbitDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new VarbitDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return Integer.toString(id);
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          {
            varpId = stream.readUnsignedShort();
            leastSignificantBit = stream.readUnsignedByte();
            mostSignificantBit = stream.readUnsignedByte();
            break;
          }
        default:
          System.out.println("Varbit Definitions Unknown Opcode: " + opcode);
          break;
      }
    }
  }

  @Override
  public Stream save(Stream stream) {
    if (varpId != -1) {
      if (varpId < 0 || varpId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": varpId");
      }
      if (leastSignificantBit < 0 || leastSignificantBit > 0xFF) {
        throw new IllegalArgumentException(id + ": leastSignificantBit");
      }
      if (mostSignificantBit < 0 || mostSignificantBit > 0xFF) {
        throw new IllegalArgumentException(id + ": mostSignificantBit");
      }
      stream.writeByte(1);
      stream.writeShort(varpId);
      stream.writeByte(leastSignificantBit);
      stream.writeByte(mostSignificantBit);
    }
    stream.writeByte(0);
    return stream;
  }

  @Override
  public Definition getDefaultDefinition() {
    return DEFAULT;
  }

  @Override
  public VarbitDefinition[] allDefinitions() {
    return definitions;
  }

  public int getValue(int varpValue, int bitValue) {
    var mask = (1 << mostSignificantBit - leastSignificantBit + 1) - 1;
    return varpValue & ~(mask << leastSignificantBit) | (bitValue & mask) << leastSignificantBit;
  }
}
