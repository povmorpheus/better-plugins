package com.palidinodh.cache.store.index;

import com.palidinodh.cache.store.util.Djb2;
import lombok.Getter;
import lombok.Setter;

@Getter
public class FileData {

  private int index;
  private int archiveId;
  private int id;
  @Setter private int nameHash;
  @Setter private byte[] contents;

  public FileData(int index, int archiveId, int id, int nameHash) {
    this.index = index;
    this.archiveId = archiveId;
    this.id = id;
    this.nameHash = nameHash;
  }

  @Override
  public String toString() {
    var name = Integer.toString(id);
    if (nameHash != 0) {
      var unhashedName = Djb2.getUnhashedName(nameHash);
      if (unhashedName != null) {
        name += "-" + unhashedName;
      } else {
        name += "-(" + nameHash + ")";
      }
    }
    return name;
  }
}
