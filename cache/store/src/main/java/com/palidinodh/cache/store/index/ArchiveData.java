package com.palidinodh.cache.store.index;

import java.util.LinkedList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class ArchiveData {

  private int index;
  private int id;
  @Setter private int nameHash;
  @Setter private int compression;
  @Setter private int crc;
  @Setter private int revision;
  private List<FileData> files = new LinkedList<>();

  public ArchiveData(int index, int id, int nameHash, int compression) {
    this.index = index;
    this.id = id;
    this.nameHash = nameHash;
    this.compression = compression;
  }

  public void addFile(FileData file) {
    files.add(file);
  }
}
