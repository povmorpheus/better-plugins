package com.palidinodh.cache.store.fs.jagex;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class IndexEntry {

  private final IndexFile indexFile;
  private final int id, sector, length;

  @Override
  public int hashCode() {
    var hash = 7;
    hash = 19 * hash + Objects.hashCode(indexFile);
    hash = 19 * hash + id;
    hash = 19 * hash + sector;
    hash = 19 * hash + length;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (IndexEntry) obj;
    if (!Objects.equals(indexFile, other.indexFile)) {
      return false;
    }
    if (id != other.id) {
      return false;
    }
    if (sector != other.sector) {
      return false;
    }
    if (length != other.length) {
      return false;
    }
    return true;
  }
}
