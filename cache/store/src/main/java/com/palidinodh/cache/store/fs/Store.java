package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.fs.jagex.DiskStorage;
import com.palidinodh.cache.store.util.ArchiveUpdater;
import com.palidinodh.cache.store.util.CachedArchive;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.Getter;

public class Store implements Closeable {

  @Getter private final Storage storage;
  private final List<Index> indexes = new ArrayList<>();
  private final CachedArchive cachedArchive = new CachedArchive();

  public Store(String folderName) throws IOException {
    storage = new DiskStorage(new File(folderName));
    storage.init(this);
    load();
  }

  public Store(File folder) throws IOException {
    storage = new DiskStorage(folder);
    storage.init(this);
    load();
  }

  public Store(Storage storage) throws IOException {
    this.storage = storage;
    storage.init(this);
    load();
  }

  @Override
  public void close() throws IOException {
    storage.close();
  }

  @Override
  public int hashCode() {
    var hash = 5;
    hash = 79 * hash + Objects.hashCode(indexes);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (Store) obj;
    return Objects.equals(indexes, other.indexes);
  }

  public Index addIndex(int id) {
    if (getIndex(id) != null) {
      throw new IllegalArgumentException("index " + id + " already exists");
    }
    var index = new Index(id, storage);
    indexes.add(index);
    return index;
  }

  public void removeIndex(Index index) {
    if (!indexes.contains(index)) {
      throw new IllegalArgumentException("Index " + index.getId() + " doesn't exist");
    }
    indexes.remove(index);
  }

  public void load() throws IOException {
    storage.load(this);
  }

  public void save() throws IOException {
    storage.save(this);
  }

  public int getIndexCount() {
    return indexes.size();
  }

  public Index getIndex(IndexType type) {
    return getIndex(type.getId());
  }

  public Index getIndex(int id) {
    for (var i : indexes) {
      if (i.getId() != id) {
        continue;
      }
      return i;
    }
    return null;
  }

  public byte[] getIndexData(int indexId) throws IOException {
    return storage.readIndex(indexId);
  }

  public byte[] getArchiveData(int index, int archiveId) throws IOException {
    return storage.loadArchive(getIndex(index).getArchive(archiveId));
  }

  public byte[] getFileData(int indexId, int archiveId, int fileId, int[] keys) throws IOException {
    var file = getIndex(indexId).getArchive(archiveId).loadFile(fileId, keys);
    return file != null ? file.getContents() : null;
  }

  public byte[] getFileDataByArchiveName(IndexType indexType, String archiveName, int fileId)
      throws IOException {
    var file = getIndex(indexType.getId()).getArchiveByName(archiveName).loadFile(fileId, null);
    return file != null ? file.getContents() : null;
  }

  public List<FsFile> getFiles(IndexType indexType, ConfigType configType) throws IOException {
    if (cachedArchive.getIndexId() == indexType.getId()
        && cachedArchive.getArchiveId() == configType.getId()) {
      return cachedArchive.getFiles();
    }
    var index = getIndex(indexType);
    var archive = index.getArchive(configType.getId());
    var files = archive.loadFiles(null);
    cachedArchive.set(index.getId(), configType.getId(), files);
    return cachedArchive.getFiles();
  }

  public ArchiveUpdater getArchiveUpdater(IndexType indexType) {
    return getIndex(indexType).getArchiveUpdater();
  }
}
