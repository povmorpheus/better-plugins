package com.palidinodh.skill.woodcutting;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class WoodcuttingHatchet {

  private int itemId;
  private int level;
  private int animation;
}
