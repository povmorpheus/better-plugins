package com.palidinodh.skill.thieving;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.thieving.ThievingPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.Arrays;
import java.util.List;

class Thieving extends SkillContainer {

  private static final List<Integer> FARMERS =
      Arrays.asList(
          NpcId.MASTER_FARMER, NpcId.MASTER_FARMER_3258, NpcId.MARTIN_THE_MASTER_GARDENER);

  @Override
  public int getSkillId() {
    return Skills.THIEVING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return ThievingEntries.getEntries();
  }

  @Override
  public int getDefaultMakeAmount(Player player, Npc npc, MapObject mapObject) {
    return 1;
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null) {
      player.getGameEncoder().sendMessage("You pick the " + npc.getName() + "'s pocket.");
    } else if (mapObject != null) {
      player.getGameEncoder().sendMessage("You steal from the " + mapObject.getName() + ".");
      setTemporaryMapObject(player, mapObject, entry);
    }
  }

  @Override
  public Item createHook(
      Player player, PEvent event, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    var farmer = npc != null && FARMERS.contains(npc.getId());
    if (player.getEquipment().wearingRogueOutfit() && (PRandom.randomE(10) == 0 || farmer)) {
      item = new Item(item.getId(), item.getAmount() * 2);
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingRogueOutfit()) {
      experience *= 1.1;
    }
    // if (player.getArea().is(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
    // experience *= 1.1;
    // }
    return experience;
  }

  @Override
  public boolean failedActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var chance =
        Math.min(128.0 - entry.getLevel() + player.getSkills().getLevel(getSkillId()) * 1.4, 240.0)
            / 255.0
            * 100.0;
    chance *= 1 + getMembershipBoost(player);
    if (player.inArdougne() && player.getEquipment().wearingRogueOutfit()) {
      chance += 20;
    }
    if (npc != null && player.getWidgetManager().isDiaryComplete(NameType.ARDOUGNE, DifficultyType.HARD)) {
      chance += 10;
    } else if (player.getArea().is("EastArdougneArea") || player.getArea().is("WestArdougneArea")) {
      if (mapObject != null && mapObject.getName().endsWith("stall") && player.getWidgetManager().isDiaryComplete(NameType.ARDOUGNE, DifficultyType.EASY)) {
        chance += 10;
      }
      if (npc != null && player.getWidgetManager().isDiaryComplete(NameType.ARDOUGNE, DifficultyType.MEDIUM)) {
        chance += 10;
      }
    }
    if (player.getEquipment().wearingAccomplishmentCape(getSkillId())) {
      chance += 10;
    }
    chance = Math.min(chance, 100);
    if (npc != null && PRandom.randomE(100) > chance) {
      npc.setForceMessage("What do you think you're doing?");
      npc.setAnimation(npc.getCombatDef().getAttackAnimation());
      npc.setFaceTile(player);
      player.setAnimation(player.getCombat().getBlockAnimation());
      if (player.getPlugin(ThievingPlugin.class).deincrimentDodgyNecklaceCharges()) {
        return false;
      }
      player.setGraphic(80, 100);
      player.getGameEncoder().sendMessage("You've been stunned!");
      player.getMovement().setMoveDelay(8);
      player.getSkills().setSkillDelay(8);
      return true;
    }
    return false;
  }

  @Override
  public String deathReasonHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null) {
      return "pickpocketing a " + npc.getName();
    }
    return "thieving";
  }
}
