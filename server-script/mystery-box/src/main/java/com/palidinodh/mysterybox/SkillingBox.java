package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.List;

@ReferenceId(ItemId.SKILLING_MYSTERY_BOX_32380)
class SkillingBox extends MysteryBox {

  private static final List<RandomItem> weightless = RandomItem.minWeight(ItemTables.SKILLING);

  @Override
  public int getRolls() {
    return 2;
  }

  @Override
  public List<Item> getAlwaysItems(int itemId, Player player) {
    var list = new ArrayList<Item>();
    list.add(new Item(ItemId.UNCUT_DRAGONSTONE_NOTED));
    if (PRandom.randomE(4) == 0) {
      list.add(new Item(PRandom.arrayRandom(ClueScrollType.values()).getBoxId()));
    }
    return list;
  }

  @Override
  public Item getRandomItem(Player player) {
    return RandomItem.getItem(ItemTables.SKILLING);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
