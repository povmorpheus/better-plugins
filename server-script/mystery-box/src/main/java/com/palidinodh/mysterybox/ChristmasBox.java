package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.CHRISTMAS_MYSTERY_BOX_32399)
class ChristmasBox extends MysteryBox {

  public static final List<RandomItem> ITEMS =
      RandomItem.buildList(
          new RandomItem(ItemId.BOBBLE_HAT),
          new RandomItem(ItemId.BOBBLE_SCARF),
          new RandomItem(ItemId.JESTER_HAT),
          new RandomItem(ItemId.JESTER_SCARF),
          new RandomItem(ItemId.TRI_JESTER_HAT),
          new RandomItem(ItemId.TRI_JESTER_SCARF),
          new RandomItem(ItemId.WOOLLY_HAT),
          new RandomItem(ItemId.WOOLLY_SCARF),
          new RandomItem(ItemId.SNOW_IMP_COSTUME_HEAD),
          new RandomItem(ItemId.SNOW_IMP_COSTUME_BODY),
          new RandomItem(ItemId.SNOW_IMP_COSTUME_LEGS),
          new RandomItem(ItemId.SNOW_IMP_COSTUME_TAIL),
          new RandomItem(ItemId.SNOW_IMP_COSTUME_GLOVES),
          new RandomItem(ItemId.SNOW_IMP_COSTUME_FEET),
          new RandomItem(ItemId.STAR_FACE),
          new RandomItem(ItemId.TREE_TOP),
          new RandomItem(ItemId.TREE_SKIRT),
          new RandomItem(ItemId.SANTA_MASK),
          new RandomItem(ItemId.SANTA_JACKET),
          new RandomItem(ItemId.SANTA_PANTALOONS),
          new RandomItem(ItemId.SANTA_GLOVES),
          new RandomItem(ItemId.SANTA_BOOTS),
          new RandomItem(ItemId.ANTISANTA_MASK),
          new RandomItem(ItemId.ANTISANTA_JACKET),
          new RandomItem(ItemId.ANTISANTA_PANTALOONS),
          new RandomItem(ItemId.ANTISANTA_GLOVES),
          new RandomItem(ItemId.ANTISANTA_BOOTS),
          new RandomItem(ItemId.REINDEER_HAT),
          new RandomItem(ItemId.CANDY_CANE),
          new RandomItem(ItemId.SACK_OF_PRESENTS),
          new RandomItem(ItemId.GIANT_PRESENT),
          new RandomItem(ItemId.GREEN_GINGERBREAD_SHIELD),
          new RandomItem(ItemId.RED_GINGERBREAD_SHIELD),
          new RandomItem(ItemId.BLUE_GINGERBREAD_SHIELD),
          new RandomItem(ItemId.GINGERBREAD_SHIELD));

  private static final List<RandomItem> weightless = RandomItem.minWeight(ITEMS);

  @Override
  public boolean shouldSendItemDropNews(Item item) {
    return item.getId() == ItemId.PARTYHAT_CRACKER_32391
        || item.getId() == ItemId.SANTA_CRACKER_32398;
  }

  @Override
  public Item getRandomItem(Player player) {
    player.getPlugin(FamiliarPlugin.class).rollPet(ItemId.KING_PENGUIN_60000, 1);
    if (PRandom.randomE(384) == 0) {
      return new Item(ItemId.PARTYHAT_CRACKER_32391);
    }
    if (PRandom.randomE(256) == 0) {
      return new Item(ItemId.SANTA_CRACKER_32398);
    }
    return RandomItem.getItem(ITEMS);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
