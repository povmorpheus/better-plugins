package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DRAGON_CLAWS, ItemId.DRAGON_CLAWS_20784, ItemId.DRAGON_CLAWS_BEGINNER_32327})
class DragonClawsSpecialAttack extends SpecialAttack {

  DragonClawsSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(7514);
    entry.castGraphic(new Graphic(1171));
    entry.castSound(new Sound(2548));
    addEntry(entry);
  }
}
