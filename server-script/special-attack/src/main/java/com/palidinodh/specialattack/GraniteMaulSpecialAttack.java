package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.GRANITE_MAUL, ItemId.GRANITE_MAUL_12848, ItemId.GRANITE_MAUL_20557})
class GraniteMaulSpecialAttack extends SpecialAttack {

  GraniteMaulSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(1667);
    entry.castGraphic(new Graphic(340, 100));
    entry.castSound(new Sound(2715));
    entry.instant(true);
    addEntry(entry);
  }
}
