package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ZARYTE_CROSSBOW)
class ZaryteCrossbowSpecialAttack extends SpecialAttack {

  ZaryteCrossbowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(75);
    entry.animation(9166);
    entry.projectileId(1995);
    entry.castSound(new Sound(3892));
    entry.accuracyModifier(2);
    addEntry(entry);
  }
}
