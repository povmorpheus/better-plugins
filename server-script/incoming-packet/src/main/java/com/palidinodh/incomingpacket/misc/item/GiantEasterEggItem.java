package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.GIANT_EASTER_EGG)
class GiantEasterEggItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.setAnimation(8332);
    player.setGraphic(1680);
  }
}
