package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.DIGSITE_PENDANT_1,
  ItemId.DIGSITE_PENDANT_2,
  ItemId.DIGSITE_PENDANT_3,
  ItemId.DIGSITE_PENDANT_4,
  ItemId.DIGSITE_PENDANT_5
})
class DigsitePendantItem implements ItemHandler {

  private static void digsiteTeleport(Player player) {
    if (!player.getController().canTeleport(20, true)) {
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(3339, 3446));
  }

  private static void fossilIslandTeleport(Player player) {
    if (!player.getController().canTeleport(20, true)) {
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(3764, 3869, 1));
  }

  private static void lithkrenTeleport(Player player) {
    if (!player.getController().canTeleport(20, true)) {
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(1568, 5063));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "rub":
        {
          player.openOptionsDialogue(
              new DialogueOption("Digsite", (c, s) -> digsiteTeleport(player)),
              new DialogueOption("Fossil Island", (c, s) -> fossilIslandTeleport(player)),
              new DialogueOption("Lithkren Dungeon", (c, s) -> lithkrenTeleport(player)));
          break;
        }
      case "digsite":
        digsiteTeleport(player);
        break;
      case "fossil island":
        fossilIslandTeleport(player);
        break;
      case "lithkren dungeon":
        lithkrenTeleport(player);
        break;
    }
  }
}
