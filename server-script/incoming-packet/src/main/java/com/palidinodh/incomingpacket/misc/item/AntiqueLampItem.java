package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ANTIQUE_LAMP_13145,
  ItemId.ANTIQUE_LAMP_13146,
  ItemId.ANTIQUE_LAMP_13147,
  ItemId.ANTIQUE_LAMP_13148
})
class AntiqueLampItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    int fixedExp;
    int minimumLevel;
    switch (item.getId()) {
      case ItemId.ANTIQUE_LAMP_13145:
        fixedExp = 2_500;
        minimumLevel = 31;
        break;
      case ItemId.ANTIQUE_LAMP_13146:
        fixedExp = 7_500;
        minimumLevel = 41;
        break;
      case ItemId.ANTIQUE_LAMP_13147:
        fixedExp = 15_000;
        minimumLevel = 51;
        break;
      case ItemId.ANTIQUE_LAMP_13148:
        fixedExp = 50_000;
        minimumLevel = 71;
        break;
      default:
        return;
    }
    player.getWidgetManager().sendChooseAdvanceSkill(item.getId(), fixedExp, 0, minimumLevel);
  }
}
