package com.palidinodh.incomingpacket.misc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.Smithing;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.combat.WarriorsGuild;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;

public class UseWidgetAction {

  public static void doActionMapObject(
      Player player, int widgetId, int childId, int itemSlot, MapObject object) {
    if (widgetId == WidgetId.INVENTORY) {
      Item item = player.getInventory().getItem(itemSlot);
      if (item == null) {
        return;
      }
      int itemId = player.getInventory().getId(itemSlot);
      switch (object.getId()) {
        case 34628: // Machinery
          if (!player.getInventory().hasItem(ItemId.HAMMER)) {
            player.getGameEncoder().sendMessage("You need a hammer to use this machinery.");
            break;
          }
          if (itemId == ItemId.HYDRA_LEATHER) {
            player.getInventory().deleteItem(itemId, 1, itemSlot);
            player.getInventory().addItem(ItemId.FEROCIOUS_GLOVES, 1, itemSlot);
            player
                .getGameEncoder()
                .sendMessage(
                    "By feeding the tough to work leather through the machine, you manage to form a pair of gloves.");
          } else if (itemId == ItemId.FEROCIOUS_GLOVES) {
            player.getInventory().deleteItem(itemId, 1, itemSlot);
            player.getInventory().addItem(ItemId.HYDRA_LEATHER, 1, itemSlot);
            player
                .getGameEncoder()
                .sendMessage(
                    "By feeding the gloves through the machine, you manage to revert them into leather.");
          }
          break;
        case 65001: // fountain
        case 24004: // Waterpump
        case 8927: // Rellekka well
          if (itemId == 1925) {
            player.getInventory().deleteItem(itemId, 1, itemSlot);
            player.getInventory().addItem(1929, 1, itemSlot);
            Diary.getDiaries(player)
                .forEach(d -> d.makeItem(player, -1, new Item(1929, 1), null, null));
          } else if (itemId == 5331) {
            player.getInventory().deleteItem(itemId, 1, itemSlot);
            player.getInventory().addItem(5340, 1, itemSlot);
          }
          break;
        case 733: // Web
          if (!WeaponType.getById(itemId).getConfig().hasSlash() && itemId != 946) {
            player
                .getGameEncoder()
                .sendMessage("Only a sharp blade can cut through this sticky web.");
            return;
          }
          player.setAnimation(player.getCombat().getAttackAnimation());
          if (PRandom.randomE(4) != 0) {
            player.getGameEncoder().sendMessage("You fail to cut through it.");
            return;
          }
          player.getGameEncoder().sendMessage("You slash the web apart.");
          MapObject newWeb = new MapObject(object.getId() + 1, object);
          player.getWorld().addEvent(new TempMapObject(100, player.getController(), newWeb));
          break;
        case 4974: // Mine cart
          if (object.getX() == 2778 && object.getY() == 4506) {
            if (player.getCombat().getHauntedMine() == 0) {
              if (!player.getInventory().hasItem(4075)) {
                player
                    .getGameEncoder()
                    .sendMessage("You don't see a reason to add this to the mine cart.");
                break;
              }
              player.getCombat().setHauntedMine(1);
              player.getInventory().deleteItem(4075, 1);
              player.getGameEncoder().sendMessage("You place the glowing fungus in the mine cart.");
              player
                  .getGameEncoder()
                  .sendMessage("You push the mine cart and it travels deeper into the mine.");
              player.setAnimation(4343);
              player.getGameEncoder().sendRemoveMapObject(object);
            } else {
              player
                  .getGameEncoder()
                  .sendMessage("You have already pushed a mine cart deeper into the mine.");
            }
          } else {
            player.getGameEncoder().sendMessage("Nothing interesting happens.");
          }
          break;
        case 2638: // Fountain of Heroes
          if (itemId != 1704 && itemId != 1705) {
            player.getGameEncoder().sendMessage("Nothing interesting happens.");
            break;
          }
          int gloryCount = player.getInventory().getCount(itemId);
          player.getInventory().deleteItem(itemId, gloryCount);
          player.getInventory().addItem(ItemDef.getNoted(itemId) ? 1713 : 1712, gloryCount);
          break;
        case 6943:
          player.getInventory().noteItems(itemSlot);
          break;
        case 28900: // Catacombs of Kourend Altar
          if (itemId == 6746 || itemId == 19675) {
            player.getCharges().chargeFromInventory(19675, itemSlot, 1, new Item(19677, 3), 1000);
          } else if (itemId == 19685) {
            player.openOldDialogue("catacombsofkourend", 1);
          }
          break;
        case 2965: // Legends Quest Mossy rock
          if (itemId == 744) {
            player.getInventory().deleteItem(744, 1, itemSlot);
            player.getInventory().addItem(745, 1, itemSlot);
            player
                .getGameEncoder()
                .sendMessage("The rocks vibrate and hum and the crystal starts to glow.");
          } else {
            player
                .getGameEncoder()
                .sendMessage("Nothing interesting happens. Maybe I should try something else...");
          }
          break;
        case 2966: // Legends Quest Furnace
          if (itemId == 741 || itemId == 742 || itemId == 743) {
            if (player.getInventory().getCount(741) < 1
                || player.getInventory().getCount(742) < 1
                || player.getInventory().getCount(743) < 1) {
              player.getGameEncoder().sendMessage("Nothing interesting happens.");
              player.getGameEncoder().sendMessage("Maybe there is more to this crystal....");
              break;
            }
            player.getInventory().deleteItem(741, 1);
            player.getInventory().deleteItem(742, 1);
            player.getInventory().deleteItem(743, 1);
            player.getInventory().addItem(744, 1, itemSlot);
            player
                .getGameEncoder()
                .sendMessage("The heat in the furnace rises and fuses the parts together.");
          } else {
            player.getGameEncoder().sendMessage("Perhaps I should use this to forge something...");
          }
          break;
        case 2934: // Legends Quest Winch
          if (itemId == 954) {
            player.getGameEncoder().sendMessage("You shimmy down the rope into the darkness.");
            player.getMovement().ladderUpTeleport(new Tile(2377, 4712, 0));
          } else {
            player.getGameEncoder().sendMessage("Nothing interesting happens.");
          }
          break;
        case 23955: // Magical animator
          switch (itemId) {
            case 1155:
            case 1117:
            case 1075:
              player
                  .getCombat()
                  .getWarriorsGuild()
                  .spawnArmour(object, WarriorsGuild.BRONZE_ARMOUR);
              break;
            case 1153:
            case 1115:
            case 1067:
              player.getCombat().getWarriorsGuild().spawnArmour(object, WarriorsGuild.IRON_ARMOUR);
              break;
            case 1157:
            case 1119:
            case 1069:
              player.getCombat().getWarriorsGuild().spawnArmour(object, WarriorsGuild.STEEL_ARMOUR);
              break;
            case 1165:
            case 1125:
            case 1077:
              player.getCombat().getWarriorsGuild().spawnArmour(object, WarriorsGuild.BLACK_ARMOUR);
              break;
            case 1159:
            case 1121:
            case 1071:
              player
                  .getCombat()
                  .getWarriorsGuild()
                  .spawnArmour(object, WarriorsGuild.MITHRIL_ARMOUR);
              break;
            case 1161:
            case 1123:
            case 1073:
              player
                  .getCombat()
                  .getWarriorsGuild()
                  .spawnArmour(object, WarriorsGuild.ADAMANT_ARMOUR);
              break;
            case 1163:
            case 1127:
            case 1079:
              player.getCombat().getWarriorsGuild().spawnArmour(object, WarriorsGuild.RUNE_ARMOUR);
              break;
          }
          break;
        case 13179:
        case 13180:
        case 13181:
        case 13182:
        case 13183:
        case 13184:
        case 13185:
        case 13186:
        case 13187:
        case 13188:
        case 13189:
        case 13190:
        case 13191:
        case 13192:
        case 13193:
        case 13194:
        case 13195:
        case 13196:
        case 13197:
        case 13198:
        case 13199:
        case 14860:
        case 411: // Altar
          if (!Prayer.isBone(itemId)) {
            break;
          }
          if (object.getId() == ObjectId.CHAOS_ALTAR_411
              && player.getRegionId() == 11835
              && !player.within(2948, 3819, 2953, 3822)) {
            break;
          }
          player.openOldDialogue("prayer", 0);
          player
              .getWidgetManager()
              .sendChatboxOverlay(
                  WidgetId.MAKE_X,
                  new WidgetManager.CloseEvent() {
                    @Override
                    public void execute() {
                      player.removeAttribute("map_object");
                      player.removeAttribute("item_id");
                    }
                  });
          player
              .getGameEncoder()
              .sendMakeX(
                  "How many would you like to use?",
                  14,
                  player.getInventory().getCount(itemId),
                  itemId);
          player.putAttribute("item_id", itemId);
          player.putAttribute("map_object", object);
          break;
        case 16469:
        case 26300:
        case 21303:
        case 4304:
        case 24009: // Furnace
          if (itemId == Smithing.COPPER_ORE_ID
              || itemId == Smithing.TIN_ORE_ID
              || itemId == Smithing.IRON_ORE_ID
              || itemId == Smithing.SILVER_ORE_ID
              || itemId == Smithing.GOLD_ORE_ID
              || itemId == Smithing.MITHRIL_ORE_ID
              || itemId == Smithing.ADAMANT_ORE_ID
              || itemId == Smithing.RUNE_ORE_ID
              || itemId == Smithing.COAL_ID
              || itemId == ItemId.BLURITE_ORE) {
            Smithing.openSmelt(player);
          }
          break;
        case 2031:
        case 2097:
        case 4306: // Anvil
          if (itemId == Smithing.BRONZE_BAR_ID
              || itemId == Smithing.IRON_BAR_ID
              || itemId == Smithing.STEEL_BAR_ID
              || itemId == Smithing.MITHRIL_BAR_ID
              || itemId == Smithing.ADAMANT_BAR_ID
              || itemId == Smithing.RUNE_BAR_ID
              || itemId == Smithing.BLURITE_BAR_ID) {
            Smithing.openSmith(player, itemId);
          }
          break;
        default:
          player.getGameEncoder().sendMessage("Nothing interesting happens.");
          break;
      }
    }
  }
}
