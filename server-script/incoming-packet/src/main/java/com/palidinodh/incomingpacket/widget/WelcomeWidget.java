package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.WELCOME)
class WelcomeWidget implements WidgetHandler {

  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 81:
        player.getWidgetManager().removeWidget(ViewportContainer.WELCOME);
        player.getWidgetManager().sendGameViewport();
        if (player.isNewAccount()) {
          player.getWidgetManager().sendInteractiveOverlay(WidgetId.CHARACTER_DESIGN);
        }
        break;
    }
  }
}
