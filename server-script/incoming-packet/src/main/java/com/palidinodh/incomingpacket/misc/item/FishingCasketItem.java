package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId(ItemId.CASKET_25590)
class FishingCasketItem implements ItemHandler {

  private static final List<SubtableItem> FISH =
      Arrays.asList(
          new SubtableItem(45, new RandomItem(ItemId.RAW_PIKE_NOTED, 30, 40)),
          new SubtableItem(45, new RandomItem(ItemId.RAW_SALMON_NOTED, 20, 30)),
          new SubtableItem(45, new RandomItem(ItemId.RAW_TUNA_NOTED, 10, 20)),
          new SubtableItem(45, new RandomItem(ItemId.RAW_LOBSTER_NOTED, 7, 12)),
          new SubtableItem(45, new RandomItem(ItemId.RAW_BASS_NOTED, 5, 10)),
          new SubtableItem(50, new RandomItem(ItemId.RAW_PIKE_NOTED, 30, 40)),
          new SubtableItem(50, new RandomItem(ItemId.RAW_TUNA_NOTED, 20, 30)),
          new SubtableItem(50, new RandomItem(ItemId.RAW_LOBSTER_NOTED, 10, 20)),
          new SubtableItem(50, new RandomItem(ItemId.RAW_BASS_NOTED, 7, 12)),
          new SubtableItem(50, new RandomItem(ItemId.RAW_SWORDFISH_NOTED, 5, 10)),
          new SubtableItem(76, new RandomItem(ItemId.RAW_TUNA_NOTED, 30, 40)),
          new SubtableItem(76, new RandomItem(ItemId.RAW_LOBSTER_NOTED, 20, 30)),
          new SubtableItem(76, new RandomItem(ItemId.RAW_BASS_NOTED, 10, 20)),
          new SubtableItem(76, new RandomItem(ItemId.RAW_SWORDFISH_NOTED, 7, 12)),
          new SubtableItem(76, new RandomItem(ItemId.RAW_SHARK_NOTED, 5, 10)),
          new SubtableItem(79, new RandomItem(ItemId.RAW_LOBSTER_NOTED, 30, 40)),
          new SubtableItem(79, new RandomItem(ItemId.RAW_BASS_NOTED, 20, 30)),
          new SubtableItem(79, new RandomItem(ItemId.RAW_SWORDFISH_NOTED, 10, 20)),
          new SubtableItem(79, new RandomItem(ItemId.RAW_SHARK_NOTED, 7, 12)),
          new SubtableItem(79, new RandomItem(ItemId.RAW_SEA_TURTLE_NOTED, 5, 10)),
          new SubtableItem(81, new RandomItem(ItemId.RAW_BASS_NOTED, 30, 40)),
          new SubtableItem(81, new RandomItem(ItemId.RAW_SWORDFISH_NOTED, 20, 30)),
          new SubtableItem(81, new RandomItem(ItemId.RAW_SHARK_NOTED, 10, 20)),
          new SubtableItem(81, new RandomItem(ItemId.RAW_SEA_TURTLE_NOTED, 7, 12)),
          new SubtableItem(81, new RandomItem(ItemId.RAW_MANTA_RAY_NOTED, 5, 10)));
  private static final List<RandomItem> OTHER =
      RandomItem.buildList(
          new RandomItem(ItemId.SPIRIT_FLAKES, 32, 64),
          new RandomItem(ItemId.PLANK_NOTED, 20, 30),
          new RandomItem(ItemId.OAK_PLANK_NOTED, 15, 25),
          new RandomItem(ItemId.SEAWEED_NOTED, 20, 60),
          new RandomItem(ItemId.STEEL_NAILS, 300, 500),
          new RandomItem(ItemId.FEATHER, 800, 1_600),
          new RandomItem(ItemId.FISHING_BAIT, 800, 1_600));

  private static Item getItem(Player player) {
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(8_000, ItemId.DRAGON_HARPOON, NpcId.CAVE_KRAKEN_127))) {
      return new Item(ItemId.DRAGON_HARPOON);
    }
    if (PRandom.inRange(
            1,
            player
                .getCombat()
                .getDropRateDenominator(5_000, ItemId.TINY_TEMPOR, NpcId.CAVE_KRAKEN_127))
        && !player.hasItem(ItemId.TINY_TEMPOR)) {
      var item = new Item(ItemId.TINY_TEMPOR);
      player.getWorld().sendItemDropNews(player, item, "from a casket");
      return item;
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(512, ItemId.TOME_OF_WATER_EMPTY, NpcId.CAVE_KRAKEN_127))) {
      return new Item(ItemId.TOME_OF_WATER_EMPTY);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(1_600, ItemId.BIG_HARPOONFISH, NpcId.CAVE_KRAKEN_127))) {
      return player.hasItem(ItemId.BIG_HARPOONFISH)
          ? new Item(ItemId.SOAKED_PAGE, 25)
          : new Item(ItemId.BIG_HARPOONFISH);
    }
    if (PRandom.inRange(
        1,
        player.getCombat().getDropRateDenominator(54, ItemId.SOAKED_PAGE, NpcId.CAVE_KRAKEN_127))) {
      return new Item(ItemId.SOAKED_PAGE, PRandom.randomI(5, 9));
    }
    if (PRandom.inRange(9, 16)) {
      return getFishItem(player);
    }
    return RandomItem.getItem(OTHER);
  }

  private static Item getFishItem(Player player) {
    var builder = new RandomItem[FISH.size()];
    for (var i = 0; i < FISH.size(); i++) {
      var subtableItem = FISH.get(i);
      var weight =
          120 - (player.getSkills().getLevelForXP(Skills.FISHING) - subtableItem.getLevel());
      builder[i] = new RandomItem(subtableItem.getItem()).weight(weight);
    }
    return RandomItem.getItem(RandomItem.buildList(builder));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove(1);
    player.getInventory().addOrDropItem(ItemId.SPIRIT_FLAKES, PRandom.randomI(32, 64));
    var casketCount = 2 + PRandom.randomI(2);
    for (var i = 0; i < casketCount; i++) {
      var casketItem = getItem(player);
      if (casketItem == null) {
        continue;
      }
      player.getInventory().addOrDropItem(casketItem);
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentFishingCaskets();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getFishingCaskets() + " caskets!");
  }

  @AllArgsConstructor
  @Getter
  private static class SubtableItem {

    private int level;
    private RandomItem item;
  }
}
