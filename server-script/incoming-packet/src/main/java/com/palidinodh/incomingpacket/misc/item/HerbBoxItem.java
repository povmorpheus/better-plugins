package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.HERB_BOX)
class HerbBoxItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(199, 1) /* Guam */,
            new RandomItem(207, 1) /* Ranaar */,
            new RandomItem(3049, 1) /* Toadflax */,
            new RandomItem(209, 1) /* Irit */,
            new RandomItem(211, 1) /* Avantoe */,
            new RandomItem(213, 1) /* Kwuarm */,
            new RandomItem(3051, 1) /* Snapdragon */,
            new RandomItem(215, 1) /* Cadantine */,
            new RandomItem(2485, 1) /* Lantadyme */,
            new RandomItem(217, 1) /* Dwarf weed */,
            new RandomItem(219, 1) /* Torstol */);
    for (var i = 0; i < 30; i++) {
      var herbItem = RandomItem.getItem(randomItems);
      player.getInventory().addOrDropItem(herbItem.getNotedId(), herbItem.getAmount());
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentHerbBoxes();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getHerbBoxes() + " herb boxes!");
  }
}
