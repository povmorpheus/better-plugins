package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId({ItemId.AMULET_OF_FURY, ItemId.AMULET_OF_BLOOD_FURY})
class AmuletOfFuryItem implements ItemHandler {

  private static final int BLOOD_SHARD_CHARGES = 10_000;

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (item.getId() == ItemId.AMULET_OF_BLOOD_FURY) {
      switch (option.getText()) {
        case "check":
          player
              .getGameEncoder()
              .sendMessage(
                  "You currently have "
                      + PNumber.formatNumber(item.getCharges())
                      + " charges stored.");
          break;
        case "revert":
          player.openDialogue(
              new OptionsDialogue(
                  "This will turn it back into an Amulet of Fury. <br>The Blood shard will be permanently lost.",
                  new DialogueOption(
                      "Yes, turn it back into an Amulet of Fury!",
                      (c, s) -> item.replace(new Item(ItemId.AMULET_OF_FURY))),
                  new DialogueOption("No!")));
          break;
      }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.AMULET_OF_FURY, ItemId.BLOOD_SHARD)) {
      var furyItem = new Item(ItemId.AMULET_OF_BLOOD_FURY);
      furyItem.setCharges(BLOOD_SHARD_CHARGES);
      onItem.replace(furyItem);
      useItem.remove();
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.AMULET_OF_BLOOD_FURY, ItemId.BLOOD_SHARD)) {
      var furyItem = ItemHandler.matchItem(useItem, onItem, ItemId.AMULET_OF_BLOOD_FURY);
      var newCharges =
          Math.min(
              furyItem.getCharges() + BLOOD_SHARD_CHARGES, furyItem.getInfoDef().getDegradeTime());
      var addingCharges = newCharges - furyItem.getCharges();
      if (addingCharges <= 0) {
        player.getGameEncoder().sendMessage("No charges can be added to this amulet.");
        return true;
      }
      player.openDialogue(
          new OptionsDialogue(
              "Sacrifice a blood shard and add "
                  + PNumber.formatNumber(addingCharges)
                  + " charges?",
              new DialogueOption(
                  "Yes!",
                  (c, s) -> {
                    furyItem.setCharges(newCharges);
                    ItemHandler.matchItem(useItem, onItem, ItemId.BLOOD_SHARD).remove();
                  }),
              new DialogueOption("No!")));
      return true;
    }
    return false;
  }
}
