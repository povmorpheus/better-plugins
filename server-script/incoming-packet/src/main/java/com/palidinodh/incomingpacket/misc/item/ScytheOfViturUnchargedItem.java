package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SCYTHE_OF_VITUR_UNCHARGED,
  ItemId.HOLY_SCYTHE_OF_VITUR_UNCHARGED,
  ItemId.SANGUINE_SCYTHE_OF_VITUR_UNCHARGED
})
class ScytheOfViturUnchargedItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.HOLY_SCYTHE_OF_VITUR_UNCHARGED:
        switch (option.getText()) {
          case "dismantle":
            player.getInventory().deleteItem(ItemId.HOLY_SCYTHE_OF_VITUR_UNCHARGED);
            player.getInventory().addOrDropItem(ItemId.SCYTHE_OF_VITUR_UNCHARGED);
            player.getInventory().addOrDropItem(ItemId.HOLY_ORNAMENT_KIT);
            break;
          case "charge":
            player
                .getGameEncoder()
                .sendMessage("This is charged with vials of blood and blood runes.");
            break;
        }
        break;
      case ItemId.SANGUINE_SCYTHE_OF_VITUR_UNCHARGED:
        switch (option.getText()) {
          case "dismantle":
            player.getInventory().deleteItem(ItemId.SANGUINE_SCYTHE_OF_VITUR_UNCHARGED);
            player.getInventory().addOrDropItem(ItemId.SCYTHE_OF_VITUR_UNCHARGED);
            player.getInventory().addOrDropItem(ItemId.SANGUINE_ORNAMENT_KIT);
            break;
          case "charge":
            player
                .getGameEncoder()
                .sendMessage("This is charged with vials of blood and blood runes.");
            break;
        }
        break;
      case ItemId.SCYTHE_OF_VITUR_UNCHARGED:
        switch (option.getText()) {
          case "charge":
            player
                .getGameEncoder()
                .sendMessage("This is charged with vials of blood and blood runes.");
            break;
        }
        break;
    }
  }
}
