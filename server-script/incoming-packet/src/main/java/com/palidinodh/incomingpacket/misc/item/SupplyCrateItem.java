package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId(ItemId.SUPPLY_CRATE)
class SupplyCrateItem implements ItemHandler {

  private static final List<SubtableItem> LOGS =
      Arrays.asList(
          new SubtableItem(15, new RandomItem(ItemId.OAK_LOGS_NOTED, 10, 20)),
          new SubtableItem(15, new RandomItem(ItemId.WILLOW_LOGS_NOTED, 10, 20)),
          new SubtableItem(35, new RandomItem(ItemId.TEAK_LOGS_NOTED, 10, 20)),
          new SubtableItem(45, new RandomItem(ItemId.MAPLE_LOGS_NOTED, 10, 20)),
          new SubtableItem(50, new RandomItem(ItemId.MAHOGANY_LOGS_NOTED, 10, 20)),
          new SubtableItem(60, new RandomItem(ItemId.YEW_LOGS_NOTED, 10, 20)),
          new SubtableItem(75, new RandomItem(ItemId.MAGIC_LOGS_NOTED, 10, 20)));
  private static final List<SubtableItem> GEMS =
      Arrays.asList(
          new SubtableItem(20, new RandomItem(ItemId.UNCUT_SAPPHIRE_NOTED, 2, 4)),
          new SubtableItem(27, new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 2, 4)),
          new SubtableItem(34, new RandomItem(ItemId.UNCUT_RUBY_NOTED, 2, 4)),
          new SubtableItem(43, new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 2, 4)));
  private static final List<SubtableItem> ORES =
      Arrays.asList(
          new SubtableItem(10, new RandomItem(ItemId.LIMESTONE_NOTED, 3, 7)),
          new SubtableItem(15, new RandomItem(ItemId.IRON_ORE_NOTED, 5, 15)),
          new SubtableItem(20, new RandomItem(ItemId.SILVER_ORE_NOTED, 10, 12)),
          new SubtableItem(30, new RandomItem(ItemId.COAL_NOTED, 10, 14)),
          new SubtableItem(40, new RandomItem(ItemId.GOLD_ORE_NOTED, 8, 11)),
          new SubtableItem(55, new RandomItem(ItemId.MITHRIL_ORE_NOTED, 3, 5)),
          new SubtableItem(70, new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 2, 3)),
          new SubtableItem(85, new RandomItem(ItemId.RUNITE_ORE_NOTED, 1, 2)));
  private static final List<SubtableItem> HERBS =
      Arrays.asList(
          new SubtableItem(3, new RandomItem(ItemId.GRIMY_GUAM_LEAF_NOTED, 3, 6)),
          new SubtableItem(5, new RandomItem(ItemId.GRIMY_MARRENTILL_NOTED, 3, 6)),
          new SubtableItem(11, new RandomItem(ItemId.GRIMY_TARROMIN_NOTED, 3, 6)),
          new SubtableItem(20, new RandomItem(ItemId.GRIMY_HARRALANDER_NOTED, 3, 6)),
          new SubtableItem(25, new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 1, 3)),
          new SubtableItem(40, new RandomItem(ItemId.GRIMY_IRIT_LEAF_NOTED, 3, 5)),
          new SubtableItem(48, new RandomItem(ItemId.GRIMY_AVANTOE_NOTED, 3, 5)),
          new SubtableItem(54, new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 2, 4)),
          new SubtableItem(65, new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 2, 4)),
          new SubtableItem(67, new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 2, 4)),
          new SubtableItem(70, new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 2, 4)),
          new SubtableItem(75, new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 3)));
  private static final List<SubtableItem> TREE_SEEDS =
      Arrays.asList(
          new SubtableItem(15, new RandomItem(ItemId.ACORN, 1)),
          new SubtableItem(30, new RandomItem(ItemId.WILLOW_SEED, 1, 2)),
          new SubtableItem(33, new RandomItem(ItemId.BANANA_TREE_SEED, 1, 2)),
          new SubtableItem(35, new RandomItem(ItemId.TEAK_SEED, 1, 2)),
          new SubtableItem(45, new RandomItem(ItemId.MAPLE_SEED, 1, 2)),
          new SubtableItem(55, new RandomItem(ItemId.MAHOGANY_SEED, 1, 2)),
          new SubtableItem(60, new RandomItem(ItemId.YEW_SEED, 1, 2)),
          new SubtableItem(75, new RandomItem(ItemId.MAGIC_SEED, 1, 3)));
  private static final List<SubtableItem> HERB_SEEDS =
      Arrays.asList(
          new SubtableItem(32, new RandomItem(ItemId.RANARR_SEED, 1, 3)),
          new SubtableItem(38, new RandomItem(ItemId.TOADFLAX_SEED, 1, 3)),
          new SubtableItem(44, new RandomItem(ItemId.IRIT_SEED, 1, 3)),
          new SubtableItem(50, new RandomItem(ItemId.AVANTOE_SEED, 1, 3)),
          new SubtableItem(56, new RandomItem(ItemId.KWUARM_SEED, 1, 3)),
          new SubtableItem(62, new RandomItem(ItemId.SNAPDRAGON_SEED, 1, 3)),
          new SubtableItem(67, new RandomItem(ItemId.CADANTINE_SEED, 1, 3)),
          new SubtableItem(73, new RandomItem(ItemId.LANTADYME_SEED, 1, 3)),
          new SubtableItem(79, new RandomItem(ItemId.DWARF_WEED_SEED, 1, 3)),
          new SubtableItem(85, new RandomItem(ItemId.TORSTOL_SEED, 1, 3)));
  private static final List<SubtableItem> OTHER_SEEDS =
      Arrays.asList(
          new SubtableItem(47, new RandomItem(ItemId.WATERMELON_SEED, 3, 7)),
          new SubtableItem(61, new RandomItem(ItemId.SNAPE_GRASS_SEED, 3, 7)),
          new SubtableItem(83, new RandomItem(ItemId.SPIRIT_SEED, 1)));
  private static final List<SubtableItem> FISH =
      Arrays.asList(
          new SubtableItem(15, new RandomItem(ItemId.RAW_ANCHOVIES_NOTED, 6, 11)),
          new SubtableItem(20, new RandomItem(ItemId.RAW_TROUT_NOTED, 6, 11)),
          new SubtableItem(30, new RandomItem(ItemId.RAW_SALMON_NOTED, 6, 11)),
          new SubtableItem(35, new RandomItem(ItemId.RAW_TUNA_NOTED, 6, 11)),
          new SubtableItem(40, new RandomItem(ItemId.RAW_LOBSTER_NOTED, 6, 11)),
          new SubtableItem(50, new RandomItem(ItemId.RAW_SWORDFISH_NOTED, 6, 11)),
          new SubtableItem(76, new RandomItem(ItemId.RAW_SHARK_NOTED, 6, 11)));
  private static final List<RandomItem> OTHER =
      RandomItem.buildList(
          new RandomItem(ItemId.COINS, 2_000, 5_000),
          new RandomItem(ItemId.SALTPETRE_NOTED, 1, 30),
          new RandomItem(ItemId.DYNAMITE_NOTED, 3, 5),
          new RandomItem(ItemId.BURNT_PAGE, 7, 43),
          new RandomItem(ItemId.PURE_ESSENCE_NOTED, 20, 70));

  private static Item getItem(Player player) {
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(10_000, ItemId.DRAGON_AXE, NpcId.SMOKE_DEVIL_160))) {
      return new Item(ItemId.DRAGON_AXE);
    }
    if (PRandom.inRange(
            1,
            player.getCombat().getDropRateDenominator(5_000, ItemId.PHOENIX, NpcId.SMOKE_DEVIL_160))
        && !player.hasItem(ItemId.PHOENIX)) {
      var item = new Item(ItemId.PHOENIX);
      player.getWorld().sendItemDropNews(player, item, "from a supply crate");
      return item;
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(512, ItemId.TOME_OF_FIRE_EMPTY, NpcId.SMOKE_DEVIL_160))) {
      return new Item(ItemId.TOME_OF_FIRE_EMPTY);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(150, ItemId.BRUMA_TORCH, NpcId.SMOKE_DEVIL_160))) {
      return new Item(ItemId.BRUMA_TORCH);
    }
    var table = PRandom.randomI(8);
    if (table >= 0 && table <= 2) {
      return RandomItem.getItem(OTHER);
    }
    if (table == 3) {
      return getSubtableItem(player, Skills.WOODCUTTING, LOGS);
    }
    if (table == 4) {
      return getSubtableItem(player, Skills.CRAFTING, GEMS);
    }
    if (table == 5) {
      return getSubtableItem(player, Skills.MINING, ORES);
    }
    if (table == 6) {
      return getSubtableItem(player, Skills.HERBLORE, HERBS);
    }
    if (table == 7) {
      var subtable = PRandom.randomI(4);
      if (subtable >= 0 && subtable <= 2) {
        return getSubtableItem(player, Skills.FARMING, HERB_SEEDS);
      }
      if (subtable == 3) {
        return getSubtableItem(player, Skills.FARMING, TREE_SEEDS);
      }
      if (subtable == 4) {
        return getSubtableItem(player, Skills.FARMING, OTHER_SEEDS);
      }
    }
    if (table == 8) {
      return getSubtableItem(player, Skills.FISHING, FISH);
    }
    return null;
  }

  private static Item getSubtableItem(Player player, int skill, List<SubtableItem> items) {
    var builder = new RandomItem[items.size()];
    for (var i = 0; i < items.size(); i++) {
      var subtableItem = items.get(i);
      var weight = 120 - (player.getSkills().getLevelForXP(skill) - subtableItem.getLevel());
      builder[i] = new RandomItem(subtableItem.getItem()).weight(weight);
    }
    return RandomItem.getItem(RandomItem.buildList(builder));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 3) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    var supplyCount = 2 + PRandom.randomI(2);
    for (var i = 0; i < supplyCount; i++) {
      var supplyItem = getItem(player);
      if (supplyItem == null) {
        continue;
      }
      player.getInventory().addOrDropItem(supplyItem);
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentSupplyBoxes();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getSupplyBoxes() + " Supply boxes!");
  }

  @AllArgsConstructor
  @Getter
  private static class SubtableItem {

    private int level;
    private RandomItem item;
  }
}
