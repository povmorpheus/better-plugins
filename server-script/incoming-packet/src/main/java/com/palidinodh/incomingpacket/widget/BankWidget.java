package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Bank;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.lootingbag.LootingBagPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  WidgetId.BANK,
  WidgetId.BANK_INVENTORY,
  WidgetId.DEPOSIT_BOX,
  WidgetId.BANK_PIN_SETTINGS
})
class BankWidget implements WidgetHandler {

  private static void bank(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 11:
        if (option == 0) {
          player.getBank().setViewingTab(slot - 10);
          player.getGameEncoder().sendScript(101, 11);
        } else if (option == 5) {
          player.getBank().collapseTab(slot - 10, 0);
        }
        break;
      case 13:
        var realSlot = player.getBank().getRealSlot(slot);
        switch (option) {
          case 0:
            if (player.getBank().getDefaultQuantity() == Bank.DefaultQuantity.X) {
              player.getBank().withdraw(itemId, realSlot, player.getBank().getLastEnteredX());
            } else {
              player
                  .getBank()
                  .withdraw(itemId, realSlot, player.getBank().getDefaultQuantity().quantity);
            }
            break;
          case 1:
            player.getBank().withdraw(itemId, realSlot, 1);
            break;
          case 2:
            player.getBank().withdraw(itemId, realSlot, 5);
            break;
          case 3:
            player.getBank().withdraw(itemId, realSlot, 10);
            break;
          case 4:
            player.getBank().withdraw(itemId, realSlot, player.getBank().getLastEnteredX());
            break;
          case 5:
            player
                .getGameEncoder()
                .sendEnterAmount(
                    value -> {
                      player.getBank().setLastEnteredX(value);
                      player.getBank().withdraw(itemId, realSlot, value);
                    });
            break;
          case 6:
            player.getBank().withdraw(itemId, realSlot, Item.MAX_AMOUNT);
            break;
          case 7:
            var item = player.getBank().getItem(realSlot);
            if (item != null) {
              player.getBank().withdraw(itemId, realSlot, item.getAmount() - 1);
            }
            break;
          case 8:
            player.getBank().withdraw(itemId, realSlot, -1);
            break;
        }
        break;
      case 17:
        player.getBank().setInsertMode(false);
        break;
      case 19:
        player.getBank().setInsertMode(true);
        break;
      case 22:
        player.getBank().setTakeAsNote(false);
        break;
      case 24:
        player.getBank().setTakeAsNote(true);
        break;
      case 28:
        player.getBank().setDefaultQuantity(Bank.DefaultQuantity.ONE);
        break;
      case 30:
        player.getBank().setDefaultQuantity(Bank.DefaultQuantity.FIVE);
        break;
      case 32:
        player.getBank().setDefaultQuantity(Bank.DefaultQuantity.TEN);
        break;
      case 34:
        player.getBank().setDefaultQuantity(Bank.DefaultQuantity.X);
        break;
      case 36:
        player.getBank().setDefaultQuantity(Bank.DefaultQuantity.ALL);
        break;
      case 38:
        player.getBank().setAlwaysSetPlaceholders(!player.getBank().getAlwaysSetPlaceholders());
        break;
      case 42:
        player.getBank().storeInventory();
        break;
      case 44:
        player.getBank().storeEquipment();
        break;
      case 47:
        player.getBank().incinerateItem(itemId, slot - 1);
        break;
      case 50:
        switch (slot) {
          case 0:
            player.getBank().setTabDisplay(Bank.TAB_DISPLAY_FIRST_ITEM);
            break;
          case 1:
            player.getBank().setTabDisplay(Bank.TAB_DISPLAY_DIGIT);
            break;
          case 2:
            player.getBank().setTabDisplay(Bank.TAB_DISPLAY_ROMAN_NUMERAL);
            break;
          case 3:
            player.getBank().setTabDisplay(Bank.TAB_DISPLAY_HIDDEN);
            break;
        }
        break;
      case 51:
        player.getBank().setIncinerator(!player.getBank().getIncinerator());
        break;
      case 52:
        player.getBank().setHideWornItems(!player.getBank().getHideWornItems());
        break;
    }
  }

  private static void inventory(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var lootingBagPlugin = player.getPlugin(LootingBagPlugin.class);
    switch (childId) {
      case 3:
        switch (option) {
          case 1:
            if (player.getBank().getDefaultQuantity() == Bank.DefaultQuantity.X) {
              player.getBank().deposit(itemId, slot, player.getBank().getLastEnteredX());
            } else {
              player
                  .getBank()
                  .deposit(itemId, slot, player.getBank().getDefaultQuantity().quantity);
            }
            break;
          case 2:
            player.getBank().deposit(itemId, slot, 1);
            break;
          case 3:
            player.getBank().deposit(itemId, slot, 5);
            break;
          case 4:
            player.getBank().deposit(itemId, slot, 10);
            break;
          case 5:
            player.getBank().deposit(itemId, slot, player.getBank().getLastEnteredX());
            break;
          case 6:
            player
                .getGameEncoder()
                .sendEnterAmount(
                    value -> {
                      player.getBank().setLastEnteredX(value);
                      player.getBank().deposit(itemId, slot, value);
                    });
            break;
          case 7:
            player.getBank().deposit(itemId, slot, Item.MAX_AMOUNT);
            break;
          case 8:
            if (itemId == player.getInventory().getId(slot) && itemId == ItemId.RUNE_POUCH) {
              player.getController().removeRunePouchRunes();
            }
            break;
        }
        break;
      case 5:
        lootingBagPlugin.bankDeposit();
        break;
      case 10:
        switch (option) {
          case 0:
            lootingBagPlugin.bankDeposit(slot, itemId, 1);
            break;
          case 1:
            lootingBagPlugin.bankDeposit(slot, itemId, 5);
            break;
          case 2:
            lootingBagPlugin.bankDeposit(slot, itemId, Item.MAX_AMOUNT);
            break;
          case 3:
            player
                .getGameEncoder()
                .sendEnterAmount(value -> lootingBagPlugin.bankDeposit(slot, itemId, value));
            break;
        }
        break;
    }
  }

  private static void depositBox(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 2:
        switch (option) {
          case 1:
            player.getBank().deposit(itemId, slot, 1);
            break;
          case 2:
            player.getBank().deposit(itemId, slot, 5);
            break;
          case 3:
            player.getBank().deposit(itemId, slot, 10);
            break;
          case 4:
            player.getBank().deposit(itemId, slot, Item.MAX_AMOUNT);
            break;
          case 5:
            player
                .getGameEncoder()
                .sendEnterAmount(
                    value -> {
                      player.getBank().deposit(itemId, slot, value);
                    });
            break;
        }
        break;
      case 4:
        player.getBank().storeInventory();
        break;
      case 6:
        player.getBank().storeEquipment();
        break;
      case 8:
        player.getPlugin(LootingBagPlugin.class).bankDeposit();
        break;
    }
  }

  private static void pinSettings(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 19:
      case 22:
        player.getBank().openPinSettingsConfirm(false);
        break;
      case 23:
      case 26:
        player.getBank().openPinSettingsConfirm(true);
        break;
      case 33:
        if (player.getAttributeBool("remove_bank_pin")) {
          player.getBank().deletePin();
        } else {
          player.getBank().openPinEnter(true);
        }
        break;
      case 36:
        player.removeAttribute("remove_bank_pin");
        break;
    }
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    switch (widgetId) {
      case WidgetId.BANK:
        bank(player, option, widgetId, childId, slot, itemId);
        break;
      case WidgetId.BANK_INVENTORY:
        inventory(player, option, widgetId, childId, slot, itemId);
        break;
      case WidgetId.DEPOSIT_BOX:
        depositBox(player, option, widgetId, childId, slot, itemId);
        break;
      case WidgetId.BANK_PIN_SETTINGS:
        pinSettings(player, option, widgetId, childId, slot, itemId);
        break;
    }
  }

  @Override
  public boolean rotateWidget(
      Player player,
      int useWidgetId,
      int useChildId,
      int onWidgetId,
      int onChildId,
      int useSlot,
      int useItemId,
      int onSlot,
      int onItemId) {
    if (useWidgetId == WidgetId.BANK && onWidgetId == WidgetId.BANK) {
      if (useChildId == 13 && onChildId == 11) {
        player.getBank().moveItemToTab(onItemId, -1, useSlot, onSlot - 10);
      } else if (useChildId == 13 && onChildId == 13) {
        player
            .getBank()
            .rotateItems(
                useItemId,
                -1,
                player.getBank().getRealSlot(useSlot),
                player.getBank().getRealSlot(onSlot));
      } else if (useChildId == 11 && onChildId == 11) {
        if (onSlot - 10 == 0) {
          player.getBank().collapseTab(useSlot - 10, onSlot - 10);
        } else {
          player.getBank().rotateTabs(useSlot - 10, onSlot - 10);
        }
      }
      return true;
    }
    if (useWidgetId == WidgetId.BANK_INVENTORY && onWidgetId == WidgetId.BANK_INVENTORY) {
      player.getInventory().rotateItems(useSlot, onSlot);
      return true;
    }
    return false;
  }
}
