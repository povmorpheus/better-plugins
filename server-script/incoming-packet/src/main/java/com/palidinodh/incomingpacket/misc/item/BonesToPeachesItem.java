package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BONES_TO_PEACHES_8015)
class BonesToPeachesItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var bonesCount = player.getInventory().getCount(ItemId.BONES);
    var bigBonesCount = player.getInventory().getCount(ItemId.BIG_BONES);
    if (bonesCount == 0 && bigBonesCount == 0) {
      player.getGameEncoder().sendMessage("You aren't holding any bones!");
      return;
    }
    item.remove(1);
    player.getInventory().deleteItem(ItemId.BONES, bonesCount);
    player.getInventory().deleteItem(ItemId.BIG_BONES, bigBonesCount);
    player.getInventory().addItem(ItemId.PEACH, bonesCount + bigBonesCount);
  }
}
