package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class VerzikWebCombat extends NpcCombat {

  @Inject private Npc npc;
  private int timer = 16;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var pillar = NpcCombatDefinition.builder();
    pillar.id(NpcId.WEB);
    pillar.hitpoints(NpcCombatHitpoints.builder().total(10).build());
    pillar.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    return Arrays.asList(pillar.build());
  }

  @Override
  public void spawnHook() {
    npc.getController().addMapObject(new MapObject(NullObjectId.NULL_32734, 10, 0, npc));
  }

  @Override
  public void despawnHook() {
    npc.getController().removeMapObject(new MapObject(NullObjectId.NULL_32734, 10, 0, npc));
    for (var player : npc.getController().getPlayers()) {
      if (!npc.withinDistance(player, 0)) {
        continue;
      }
      player.getController().clearMagicBind();
    }
  }

  @Override
  public void tickEndHook() {
    if (npc.isLocked()) {
      return;
    }
    if (timer-- >= 14) {
      return;
    }
    for (var player : npc.getController().getPlayers()) {
      if (!npc.withinDistance(player, 0)) {
        continue;
      }
      if (timer == 0) {
        player.getCombat().addHitEvent(new HitEvent(new Hit(PRandom.randomI(30, 50))));
      } else {
        player.getController().setMagicBind(20);
      }
    }
    if (timer == 0) {
      startDeath();
    }
  }
}
