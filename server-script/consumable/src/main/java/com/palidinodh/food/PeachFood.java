package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableHeal;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PEACH)
class PeachFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(8));
    return builder;
  }
}
