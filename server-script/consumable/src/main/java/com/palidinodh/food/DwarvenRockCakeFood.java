package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DWARVEN_ROCK_CAKE, ItemId.DWARVEN_ROCK_CAKE_7510})
class DwarvenRockCakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.keepItem(true);
    builder.skipDelays(true);
    builder.action(
        (p, c) -> {
          var damage = 1;
          if (p.hasAttribute("guzzle_dwarven_rock_cake")) {
            damage = (int) (p.getSkills().getLevel(Skills.HITPOINTS) * 0.1);
            p.removeAttribute("guzzle_dwarven_rock_cake");
          }
          if (damage >= p.getCombat().getHitpoints()) {
            damage = p.getCombat().getHitpoints() - 1;
          }
          p.getCombat().applyHit(new Hit(damage));
          if (p.getCombat().isDead()) {
            p.putAttribute("death_reason", "eating a Dwarven rock cake");
          }
        });
    return builder;
  }
}
