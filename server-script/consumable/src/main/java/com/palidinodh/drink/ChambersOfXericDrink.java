package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEvent;
import lombok.AllArgsConstructor;

@ReferenceId({
  ItemId.ELDER_MINUS_1,
  ItemId.ELDER_MINUS_2,
  ItemId.ELDER_MINUS_3,
  ItemId.ELDER_MINUS_4
})
class CoxElderMinusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 4, 10, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 4, 10, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 4, 10, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.ELDER_POTION_1,
  ItemId.ELDER_POTION_2,
  ItemId.ELDER_POTION_3,
  ItemId.ELDER_POTION_4
})
class CoxElderPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 5, 13, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 13, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 5, 13, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({ItemId.ELDER_PLUS_1, ItemId.ELDER_PLUS_2, ItemId.ELDER_PLUS_3, ItemId.ELDER_PLUS_4})
class CoxElderPlusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 6, 16, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 6, 16, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 6, 16, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.TWISTED_MINUS_1,
  ItemId.TWISTED_MINUS_2,
  ItemId.TWISTED_MINUS_3,
  ItemId.TWISTED_MINUS_4
})
class CoxTwistedMinusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 4, 10, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.TWISTED_POTION_1,
  ItemId.TWISTED_POTION_2,
  ItemId.TWISTED_POTION_3,
  ItemId.TWISTED_POTION_4
})
class CoxTwistedPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 5, 13, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.TWISTED_PLUS_1,
  ItemId.TWISTED_PLUS_2,
  ItemId.TWISTED_PLUS_3,
  ItemId.TWISTED_PLUS_4
})
class CoxTwistedPlusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 6, 16, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.KODAI_MINUS_1,
  ItemId.KODAI_MINUS_2,
  ItemId.KODAI_MINUS_3,
  ItemId.KODAI_MINUS_4
})
class CoxKodaiMinusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.MAGIC, 4, 10, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.KODAI_POTION_1,
  ItemId.KODAI_POTION_2,
  ItemId.KODAI_POTION_3,
  ItemId.KODAI_POTION_4
})
class CoxKodaiPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.MAGIC, 5, 13, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({ItemId.KODAI_PLUS_1, ItemId.KODAI_PLUS_2, ItemId.KODAI_PLUS_3, ItemId.KODAI_PLUS_4})
class CoxKodaiPlusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.MAGIC, 6, 16, true));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.REVITALISATION_MINUS_1,
  ItemId.REVITALISATION_MINUS_2,
  ItemId.REVITALISATION_MINUS_3,
  ItemId.REVITALISATION_MINUS_4
})
class CoxRevitalisationMinusPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      if (i == Skills.HITPOINTS) {
        continue;
      }
      builder.stat(new ConsumableStat(i, 10, 18, false));
    }
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.REVITALISATION_POTION_1,
  ItemId.REVITALISATION_POTION_2,
  ItemId.REVITALISATION_POTION_3,
  ItemId.REVITALISATION_POTION_4
})
class CoxRevitalisationPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      if (i == Skills.HITPOINTS) {
        continue;
      }
      builder.stat(new ConsumableStat(i, 12, 20, false));
    }
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.REVITALISATION_PLUS_1,
  ItemId.REVITALISATION_PLUS_2,
  ItemId.REVITALISATION_PLUS_3,
  ItemId.REVITALISATION_PLUS_4
})
class CoxRevitalisationPlusPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      if (i == Skills.HITPOINTS) {
        continue;
      }
      builder.stat(new ConsumableStat(i, 15, 25, false));
    }
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.PRAYER_ENHANCE_MINUS_1,
  ItemId.PRAYER_ENHANCE_MINUS_2,
  ItemId.PRAYER_ENHANCE_MINUS_3,
  ItemId.PRAYER_ENHANCE_MINUS_4
})
class CoxPrayerEnhanceMinusPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.addItem(new Item(-1));
    builder.action(
        (p, c) -> {
          p.getSkills().setPrayerEnhance(new CoxPrayerEnhanceEvent(p, 15));
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.PRAYER_ENHANCE_1,
  ItemId.PRAYER_ENHANCE_2,
  ItemId.PRAYER_ENHANCE_3,
  ItemId.PRAYER_ENHANCE_4
})
class CoxPrayerEnhancePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.addItem(new Item(-1));
    builder.action(
        (p, c) -> {
          p.getSkills().setPrayerEnhance(new CoxPrayerEnhanceEvent(p, 10));
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.PRAYER_ENHANCE_PLUS_1,
  ItemId.PRAYER_ENHANCE_PLUS_2,
  ItemId.PRAYER_ENHANCE_PLUS_3,
  ItemId.PRAYER_ENHANCE_PLUS_4
})
class CoxPrayerEnhancePlusPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.addItem(new Item(-1));
    builder.action(
        (p, c) -> {
          p.getSkills().setPrayerEnhance(new CoxPrayerEnhanceEvent(p, 5));
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.XERICS_AID_MINUS_1,
  ItemId.XERICS_AID_MINUS_2,
  ItemId.XERICS_AID_MINUS_3,
  ItemId.XERICS_AID_MINUS_4
})
class CoxXericsAidMinusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 2, 12, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.HITPOINTS, 2, 8, true));
    builder.stat(new ConsumableStat(Skills.RANGED, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.MAGIC, 0, -10, false));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({ItemId.XERICS_AID_1, ItemId.XERICS_AID_2, ItemId.XERICS_AID_3, ItemId.XERICS_AID_4})
class CoxXericsAidPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 3, 15, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.HITPOINTS, 3, 10, true));
    builder.stat(new ConsumableStat(Skills.RANGED, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.MAGIC, 0, -10, false));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.XERICS_AID_PLUS_1,
  ItemId.XERICS_AID_PLUS_2,
  ItemId.XERICS_AID_PLUS_3,
  ItemId.XERICS_AID_PLUS_4
})
class CoxXericsAidPlusPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 20, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.HITPOINTS, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.RANGED, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.MAGIC, 0, -10, false));
    builder.addItem(new Item(-1));
    return builder;
  }
}

@ReferenceId({
  ItemId.OVERLOAD_MINUS_1,
  ItemId.OVERLOAD_MINUS_2,
  ItemId.OVERLOAD_MINUS_3,
  ItemId.OVERLOAD_MINUS_4
})
class CoxOverloadMinusPotionPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.addItem(new Item(-1));
    builder.complete(
        (p, c) -> {
          if (p.getSkills().getOverload() != null) {
            p.getGameEncoder()
                .sendMessage("You are still suffering the effects of a fresh dose of overload.");
            return false;
          } else if (p.getCombat().getHitpoints() < 50) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to drink this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getSkills().setOverload(0.1, 4);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.OVERLOAD_1_20989,
  ItemId.OVERLOAD_2_20990,
  ItemId.OVERLOAD_3_20991,
  ItemId.OVERLOAD_4_20992
})
class CoxOverloadPotionPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.addItem(new Item(-1));
    builder.complete(
        (p, c) -> {
          if (p.getSkills().getOverload() != null) {
            p.getGameEncoder()
                .sendMessage("You are still suffering the effects of a fresh dose of overload.");
            return false;
          } else if (p.getCombat().getHitpoints() < 50) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to drink this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getSkills().setOverload(0.13, 5);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.OVERLOAD_PLUS_1,
  ItemId.OVERLOAD_PLUS_2,
  ItemId.OVERLOAD_PLUS_3,
  ItemId.OVERLOAD_PLUS_4
})
class CoxOverloadPlusPotionPotion implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.addItem(new Item(-1));
    builder.complete(
        (p, c) -> {
          if (p.getSkills().getOverload() != null) {
            p.getGameEncoder()
                .sendMessage("You are still suffering the effects of a fresh dose of overload.");
            return false;
          } else if (p.getCombat().getHitpoints() < 50) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to drink this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getSkills().setOverload(0.16, 6);
        });
    return builder;
  }
}

@AllArgsConstructor
class CoxPrayerEnhanceEvent extends PEvent {
  private Player player;
  private int interval;

  @Override
  public void execute() {
    if (!player.isVisible() || player.getCombat().isDead()) {
      stop();
      return;
    }
    if (getExecutions() == 433) {
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>Your prayer enhance effect has almost worn off!</col>");
    }
    if (getExecutions() == 458) {
      stop();
      if (getExecutions() == 458) {
        player
            .getGameEncoder()
            .sendMessage("<col=ff0000>Your prayer enhance effect has worn off.</col>");
      }
    } else if ((getExecutions() % interval) == 0) {
      player.getPrayer().changePoints(1);
    }
  }
}
