package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.PRAYER_POTION_1,
  ItemId.PRAYER_POTION_2,
  ItemId.PRAYER_POTION_3,
  ItemId.PRAYER_POTION_4
})
class PrayerPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.PRAYER, 7, 25, false));
    return builder;
  }
}

@ReferenceId({
  ItemId.PRAYER_POTION_1_20396,
  ItemId.PRAYER_POTION_2_20395,
  ItemId.PRAYER_POTION_3_20394,
  ItemId.PRAYER_POTION_4_20393
})
class LastManStandingPrayerPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.PRAYER, 7, 25, false));
    return builder;
  }
}
