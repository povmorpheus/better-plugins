package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;

@ReferenceId(ItemId.IMBUED_HEART)
class ImbuedHeartDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.DRINK_AS_FOOD);
    builder.stat(new ConsumableStat(Skills.MAGIC, 1, 10, true));
    builder.keepItem(true);
    builder.animation(-1);
    builder.complete(
        (p, c) -> {
          if (p.getMagic().getImbuedHeartTime() > 0) {
            if (p.getMagic().getImbuedHeartTime() > 100) {
              p.getGameEncoder()
                  .sendMessage(
                      "The heart is still drained of its power. Judging by how it feels, it will be ready in around "
                          + PTime.tickToMin(p.getMagic().getImbuedHeartTime())
                          + " minutes.");
            } else {
              p.getGameEncoder()
                  .sendMessage(
                      "The heart is still drained of its power. Judging by how it feels, it will be ready in around "
                          + PTime.tickToSec(p.getMagic().getImbuedHeartTime())
                          + " seconds.");
            }
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.setGraphic(1316);
          p.getMagic().setImbuedHeartTime(700);
        });
    return builder;
  }
}
