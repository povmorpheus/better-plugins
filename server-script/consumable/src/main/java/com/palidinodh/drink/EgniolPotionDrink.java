package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.EGNIOL_POTION_1,
  ItemId.EGNIOL_POTION_2,
  ItemId.EGNIOL_POTION_3,
  ItemId.EGNIOL_POTION_4
})
class EgniolPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.PRAYER, 7, 25, false));
    builder.action(
        (p, c) -> {
          var restore = (int) (PMovement.MAX_ENERGY * 0.4);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
          p.getSkills().setStaminaTime(200);
        });
    return builder;
  }
}
