package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ENERGY_POTION_1,
  ItemId.ENERGY_POTION_2,
  ItemId.ENERGY_POTION_3,
  ItemId.ENERGY_POTION_4
})
class EnergyPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          var restore = (int) (PMovement.MAX_ENERGY * 0.1);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.SUPER_ENERGY_1,
  ItemId.SUPER_ENERGY_2,
  ItemId.SUPER_ENERGY_3,
  ItemId.SUPER_ENERGY_4
})
class SuperEnergyPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          var restore = (int) (PMovement.MAX_ENERGY * 0.2);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
        });
    return builder;
  }
}
