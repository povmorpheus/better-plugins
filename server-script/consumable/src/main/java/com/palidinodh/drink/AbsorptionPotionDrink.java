package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ABSORPTION_1, ItemId.ABSORPTION_2, ItemId.ABSORPTION_3, ItemId.ABSORPTION_4})
class AbsorptionPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    return Consumable.builder();
  }
}
