package com.palidinodh.maparea.wilderness.godwarsdungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class GodWarsDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(3053, 10165, 3), NpcId.BOULDER_6621));
    spawns.add(new NpcSpawn(4, new Tile(3022, 10126), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3020, 10121), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3026, 10117), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3032, 10123), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3026, 10124), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3030, 10129), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3027, 10134), NpcId.SPIRITUAL_MAGE_120));
    spawns.add(new NpcSpawn(4, new Tile(3059, 10120), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3062, 10124), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3054, 10124), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3051, 10128), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3058, 10128), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3052, 10134), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3047, 10122), NpcId.SPIRITUAL_MAGE_121));
    spawns.add(new NpcSpawn(4, new Tile(3042, 10147), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3041, 10142), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3045, 10140), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3049, 10144), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3035, 10144), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3039, 10136), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3038, 10151), NpcId.SPIRITUAL_MAGE_121_3161));
    spawns.add(new NpcSpawn(4, new Tile(3026, 10130), NpcId.SARADOMIN_PRIEST_113));
    spawns.add(new NpcSpawn(4, new Tile(3031, 10126), NpcId.KNIGHT_OF_SARADOMIN_103));
    spawns.add(new NpcSpawn(4, new Tile(3036, 10125), NpcId.SARADOMIN_PRIEST_113));
    spawns.add(new NpcSpawn(4, new Tile(3037, 10130), NpcId.KNIGHT_OF_SARADOMIN_103));
    spawns.add(new NpcSpawn(4, new Tile(3033, 10132), NpcId.SARADOMIN_PRIEST_113));
    spawns.add(new NpcSpawn(4, new Tile(3023, 10119), NpcId.KNIGHT_OF_SARADOMIN_103));
    spawns.add(new NpcSpawn(4, new Tile(3050, 10141), NpcId.HELLHOUND_127));
    spawns.add(new NpcSpawn(4, new Tile(3043, 10134), NpcId.WEREWOLF_93));
    spawns.add(new NpcSpawn(4, new Tile(3036, 10139), NpcId.GORAK_149));
    spawns.add(new NpcSpawn(4, new Tile(3037, 10147), NpcId.IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3046, 10148), NpcId.WEREWOLF_93));
    spawns.add(new NpcSpawn(4, new Tile(3055, 10137), NpcId.IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3042, 10124), NpcId.CYCLOPS_81));
    spawns.add(new NpcSpawn(4, new Tile(3043, 10130), NpcId.ORK_107));
    spawns.add(new NpcSpawn(4, new Tile(3047, 10132), NpcId.HOBGOBLIN_47));
    spawns.add(new NpcSpawn(4, new Tile(3047, 10136), NpcId.JOGRE_58));
    spawns.add(new NpcSpawn(4, new Tile(3057, 10133), NpcId.CYCLOPS_81));
    spawns.add(new NpcSpawn(4, new Tile(3057, 10124), NpcId.ORK_107));
    spawns.add(new NpcSpawn(4, new Tile(3063, 10121), NpcId.HOBGOBLIN_47));
    spawns.add(new NpcSpawn(4, new Tile(3051, 10120), NpcId.JOGRE_58));
    spawns.add(new NpcSpawn(4, new Tile(3034, 10152), NpcId.AVIANSIE_69));
    spawns.add(new NpcSpawn(4, new Tile(3031, 10148), NpcId.AVIANSIE_79));
    spawns.add(new NpcSpawn(4, new Tile(3027, 10144), NpcId.AVIANSIE_92));
    spawns.add(new NpcSpawn(4, new Tile(3020, 10147), NpcId.AVIANSIE_137));
    spawns.add(new NpcSpawn(4, new Tile(3018, 10154), NpcId.AVIANSIE_69));
    spawns.add(new NpcSpawn(4, new Tile(3025, 10152), NpcId.AVIANSIE_79));
    spawns.add(new NpcSpawn(4, new Tile(3023, 10157), NpcId.AVIANSIE_92));
    spawns.add(new NpcSpawn(4, new Tile(3019, 10161), NpcId.AVIANSIE_137));
    spawns.add(new NpcSpawn(4, new Tile(3030, 10156), NpcId.AVIANSIE_69));
    spawns.add(new NpcSpawn(4, new Tile(3027, 10159), NpcId.AVIANSIE_79));
    spawns.add(new NpcSpawn(4, new Tile(3024, 10164), NpcId.AVIANSIE_92));

    return spawns;
  }
}
