package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.barrows.BarrowsPlugin;
import com.palidinodh.playerplugin.barrows.BrotherType;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;

@ReferenceId(ObjectId.KEY_CHEST_65003)
class KeyChestMapObject implements MapObjectHandler {

  private static final List<Integer> KEYS =
      Arrays.asList(
          ItemId.RARE_BARROWS_KEY_60068,
          ItemId.COMMON_BARROWS_KEY_60069,
          ItemId.BLOODIER_KEY_32305,
          ItemId.BLOODIER_KEY_32397,
          ItemId.BLOODY_KEY_32304,
          ItemId.DIAMOND_KEY_32309,
          ItemId.GOLD_KEY_32308,
          ItemId.SILVER_KEY_32307,
          ItemId.BRONZE_KEY_32306);

  private static void open(Player player, Item item) {
    switch (item.getId()) {
      case ItemId.RARE_BARROWS_KEY_60068:
        {
          item.remove(1);
          for (var brother : BrotherType.values()) {
            player.getPlugin(BarrowsPlugin.class).reward(brother.getItemIds());
          }
          break;
        }
      case ItemId.COMMON_BARROWS_KEY_60069:
        {
          item.remove(1);
          player.getPlugin(BarrowsPlugin.class).reward(BrotherType.getAllItemIds());
          break;
        }
      default:
        MysteryBox.open(player, item.getId());
        break;
    }
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var item = player.getInventory().getItemById(KEYS);
    if (item == null) {
      player.getGameEncoder().sendMessage("You need a key to open this chest.");
      return;
    }
    open(player, item);
  }
}
