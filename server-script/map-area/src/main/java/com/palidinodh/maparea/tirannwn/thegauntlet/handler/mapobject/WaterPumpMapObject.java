package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.WidgetManager.MakeXType;
import com.palidinodh.osrscore.model.entity.player.dialogue.MakeXDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.WATER_PUMP_36078, ObjectId.WATER_PUMP_35981})
class WaterPumpMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.getController().is(BossInstanceController.class)) {
      return;
    }
    player.openDialogue(
        new MakeXDialogue(
            MakeXType.MAKE,
            28,
            (c, s) -> {
              if (!player.getController().is(BossInstanceController.class)) {
                return;
              }
              player.putAttribute("count", s);
              player.setAction(
                  1,
                  e -> {
                    int count =
                        (int) player.putAttribute("count", player.getAttributeInt("count") - 1);
                    if (player.getInventory().isFull() || count == 0) {
                      e.stop();
                      return;
                    }
                    player.getInventory().addItem(ItemId.EGNIOL_POTION_4);
                    player.setAnimation(3283);
                  });
            },
            ItemId.EGNIOL_POTION_4));
  }
}
