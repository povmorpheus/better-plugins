package com.palidinodh.maparea.karamja;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11823)
public class ShipYardArea extends Area {}
