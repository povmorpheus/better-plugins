package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import java.util.Map;

@ReferenceId(NpcId.WATSON)
class WatsonNpc implements NpcHandler {

  private static final Map<ClueScrollType, Integer> SOLVE_COSTS =
      PCollection.toLinkedMap(
          ClueScrollType.EASY, 250_000,
          ClueScrollType.MEDIUM, 500_000,
          ClueScrollType.HARD, 1_000_000,
          ClueScrollType.ELITE, 2_500_000,
          ClueScrollType.MASTER, 5_000_000);

  private static void scrollsToMaster(Player player) {
    if (!player.getInventory().hasItem(ClueScrollType.EASY.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a easy clue scroll.");
      return;
    }
    if (!player.getInventory().hasItem(ClueScrollType.MEDIUM.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a medium clue scroll.");
      return;
    }
    if (!player.getInventory().hasItem(ClueScrollType.HARD.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a hard clue scroll.");
      return;
    }
    if (!player.getInventory().hasItem(ClueScrollType.ELITE.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a elite clue scroll.");
      return;
    }
    player.getInventory().deleteItem(ClueScrollType.EASY.getScrollId());
    player.getInventory().deleteItem(ClueScrollType.MEDIUM.getScrollId());
    player.getInventory().deleteItem(ClueScrollType.HARD.getScrollId());
    player.getInventory().deleteItem(ClueScrollType.ELITE.getScrollId());
    player.getInventory().addOrDropItem(ClueScrollType.MASTER.getScrollId());
  }

  private static void completeClueDialogue(Player player) {
    var isFree = player.getPlugin(TreasureTrailPlugin.class).hasFreeClueSolve();
    var dialogueOptions = new ArrayList<DialogueOption>();
    for (var entry : SOLVE_COSTS.entrySet()) {
      var cost = entry.getValue();
      if (player
          .getPlugin(BondPlugin.class)
          .isRelicUnlocked(BondRelicType.TREASURE_SEEKER_WATSON_FEE)) {
        cost /= 2;
      }
      var text = entry.getKey().getFormattedName() + ": " + PNumber.abbreviateNumber(cost);
      if (isFree) {
        text = entry.getKey().getFormattedName() + ": Free";
      }
      var finalCost = cost;
      dialogueOptions.add(
          new DialogueOption(
              text,
              (c, s) -> {
                solveClue(player, entry.getKey(), finalCost);
              }));
    }
    player.openDialogue(new OptionsDialogue(dialogueOptions));
  }

  private static void solveClue(Player player, ClueScrollType type, int cost) {
    var plugin = player.getPlugin(TreasureTrailPlugin.class);
    var isFree = plugin.hasFreeClueSolve();
    if (!isFree && player.getInventory().getCount(ItemId.COINS) < cost) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(cost) + " to do this.");
      return;
    }
    if (!player.getInventory().hasItem(type.getScrollId())) {
      player.getGameEncoder().sendMessage("You need a clue scroll to do this.");
      return;
    }
    if (isFree) {
      plugin.incrimientFreeClueSolveDailyCount();
    } else {
      player.getInventory().deleteItem(ItemId.COINS, cost);
    }
    plugin.clueComplete(type);
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Exchange clues for a master clue.",
            (c, s) -> {
              scrollsToMaster(player);
            }),
        new DialogueOption(
            "Exchange coins to solve a clue.",
            (c, s) -> {
              completeClueDialogue(player);
            }));
  }
}
