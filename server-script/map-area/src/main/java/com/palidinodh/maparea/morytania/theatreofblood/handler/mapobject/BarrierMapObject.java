package com.palidinodh.maparea.morytania.theatreofblood.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.BARRIER_32755)
class BarrierMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Leave.",
            (c, s) -> {
              player.getCombat().clearHitEvents();
              player.getController().stop();
            }),
        new DialogueOption("Nevermind."));
  }
}
