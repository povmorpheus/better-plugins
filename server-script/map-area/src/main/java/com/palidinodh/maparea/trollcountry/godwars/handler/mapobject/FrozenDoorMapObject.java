package com.palidinodh.maparea.trollcountry.godwars.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.godwars.GodWarsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ObjectId.FROZEN_DOOR,
  ObjectId.FROZEN_DOOR_42841,
  ObjectId.FROZEN_DOOR_42931,
  ObjectId.FROZEN_DOOR_42932
})
public class FrozenDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.FROZEN_DOOR:
      case ObjectId.FROZEN_DOOR_42841:
        {
          var plugin = player.getPlugin(GodWarsPlugin.class);
          if (!plugin.isGodWarsFrozenDoor()
              && !player.getInventory().hasItem(ItemId.FROZEN_KEY_26356)) {
            player.getGameEncoder().sendMessage("You need a key to unlock this door.");
            break;
          }
          player.getInventory().deleteItem(ItemId.FROZEN_KEY_26356);
          plugin.setGodWarsFrozenDoor(true);
          player.getMovement().teleport(2855, 5227);
          break;
        }
      case ObjectId.FROZEN_DOOR_42931:
      case ObjectId.FROZEN_DOOR_42932:
        player.getMovement().teleport(2883, 5280, 2);
        break;
    }
  }
}
