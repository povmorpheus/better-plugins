package com.palidinodh.maparea.trollcountry.godwars.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ANCIENT_FORGE)
public class AncientForgeMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.getGameEncoder().sendMessage("This forge will break down some items.");
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    var components = 0;
    switch (item.getId()) {
      case ItemId.BANDOS_CHESTPLATE:
        components = 3;
        break;
      case ItemId.BANDOS_TASSETS:
        components = 2;
        break;
    }
    if (components == 0) {
      player.getGameEncoder().sendMessage("The forge won't accept this item.");
      return;
    }
    item.remove();
    player.getInventory().addOrDropItem(ItemId.BANDOSIAN_COMPONENTS, components);
    player.setAnimation(3243);
  }
}
