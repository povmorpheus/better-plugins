package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class RangingGuildNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2643, 3450), NpcId.MORRIS));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2641, 3437), NpcId.BONZO));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2631, 3435), NpcId.SINISTER_STRANGER));
    spawns.add(new NpcSpawn(4, new Tile(2628, 3414), NpcId.JOSHUA));
    spawns.add(new NpcSpawn(4, new Tile(2626, 3393), NpcId.GOBLIN_2));

    return spawns;
  }
}
