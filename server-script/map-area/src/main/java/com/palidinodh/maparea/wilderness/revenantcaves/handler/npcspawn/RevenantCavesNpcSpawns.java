package com.palidinodh.maparea.wilderness.revenantcaves.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class RevenantCavesNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3180, 10204), NpcId.EMBLEM_TRADER_7942));
    spawns.add(new NpcSpawn(4, new Tile(3254, 10184), NpcId.REVENANT_IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3231, 10178), NpcId.REVENANT_IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3235, 10182), NpcId.REVENANT_GOBLIN_15));
    spawns.add(new NpcSpawn(4, new Tile(3234, 10171), NpcId.REVENANT_PYREFIEND_52));
    spawns.add(new NpcSpawn(4, new Tile(3248, 10164), NpcId.REVENANT_PYREFIEND_52));
    spawns.add(new NpcSpawn(4, new Tile(3252, 10156), NpcId.REVENANT_HOBGOBLIN_60));
    spawns.add(new NpcSpawn(4, new Tile(3246, 10158), NpcId.REVENANT_CYCLOPS_82));
    spawns.add(new NpcSpawn(4, new Tile(3251, 10168), NpcId.REVENANT_HELLHOUND_90));
    spawns.add(new NpcSpawn(4, new Tile(3247, 10179), NpcId.REVENANT_DEMON_98));
    spawns.add(new NpcSpawn(4, new Tile(3236, 10161), NpcId.REVENANT_DEMON_98));
    spawns.add(new NpcSpawn(4, new Tile(3237, 10167), NpcId.REVENANT_ORK_105));
    spawns.add(new NpcSpawn(4, new Tile(3247, 10151), NpcId.REVENANT_ORK_105));
    spawns.add(new NpcSpawn(4, new Tile(3251, 10142), NpcId.REVENANT_DARK_BEAST_120));
    spawns.add(new NpcSpawn(4, new Tile(3244, 10170), NpcId.REVENANT_KNIGHT_126));
    spawns.add(new NpcSpawn(4, new Tile(3239, 10177), NpcId.REVENANT_DRAGON_135));
    spawns.add(new NpcSpawn(4, new Tile(3195, 10067), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3202, 10067), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3204, 10071), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3203, 10076), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3198, 10075), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3194, 10073), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3196, 10070), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3200, 10072), NpcId.CURSED_CAVE_HORROR_206_16005));
    spawns.add(new NpcSpawn(4, new Tile(3217, 10064), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3222, 10062), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3228, 10066), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3222, 10070), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3218, 10075), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3230, 10072), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3225, 10075), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3219, 10080), NpcId.CURSED_GARGOYLE_349_16007));
    spawns.add(new NpcSpawn(4, new Tile(3207, 10091), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3204, 10097), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3211, 10100), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3215, 10094), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3222, 10092), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3225, 10099), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3220, 10097), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3209, 10096), NpcId.CURSED_BASILISK_KNIGHT_358_16059));
    spawns.add(new NpcSpawn(4, new Tile(3148, 10110), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3156, 10110), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3167, 10109), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3172, 10117), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3151, 10118), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3158, 10117), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3162, 10112), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3167, 10116), NpcId.CURSED_DARK_BEAST_374_16009));
    spawns.add(new NpcSpawn(4, new Tile(3212, 10131), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3215, 10123), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3225, 10123), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3223, 10133), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3220, 10140), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3230, 10141), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3233, 10133), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3232, 10122), NpcId.CURSED_SMOKE_DEVIL_280_16006));
    spawns.add(new NpcSpawn(4, new Tile(3162, 10152), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3167, 10157), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3166, 10165), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3172, 10152), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3182, 10150), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3179, 10157), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3182, 10164), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3189, 10152), NpcId.CURSED_KRAKEN_LEECH_209_16060));
    spawns.add(new NpcSpawn(4, new Tile(3197, 10166), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3203, 10162), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3202, 10169), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3210, 10160), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3208, 10166), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3217, 10167), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3214, 10163), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3198, 10161), NpcId.CURSED_ABYSSAL_DEMON_342_16010));
    spawns.add(new NpcSpawn(4, new Tile(3159, 10187), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3169, 10184), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3167, 10195), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3179, 10199), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3186, 10191), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3178, 10185), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3172, 10190), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3180, 10194), NpcId.CURSED_HYDRA_400_16012));
    spawns.add(new NpcSpawn(4, new Tile(3209, 10215), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3207, 10222), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3214, 10218), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3220, 10216), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3224, 10219), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3229, 10221), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3210, 10219), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3203, 10224), NpcId.CURSED_HELLHOUND_260_16000));
    spawns.add(new NpcSpawn(4, new Tile(3230, 10196), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3237, 10196), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3245, 10198), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3244, 10205), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3238, 10211), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3232, 10205), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3238, 10205), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(3234, 10200), NpcId.DEMONIC_GORILLA_275));

    return spawns;
  }
}
