package com.palidinodh.maparea.kandarin.grandtreetunnels;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9882)
public class GrandTreeTunnelsArea extends Area {}
