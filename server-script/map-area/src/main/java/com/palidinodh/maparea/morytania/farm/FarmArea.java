package com.palidinodh.maparea.morytania.farm;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(14391)
public class FarmArea extends Area {}
