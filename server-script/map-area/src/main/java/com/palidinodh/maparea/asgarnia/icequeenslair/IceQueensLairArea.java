package com.palidinodh.maparea.asgarnia.icequeenslair;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({11418, 11419, 11675})
public class IceQueensLairArea extends Area {}
