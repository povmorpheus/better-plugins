package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.STAIRS_40388, ObjectId.STAIRS_40390})
class SlayerCaveEntranceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getController().isMagicBound()) {
      player
          .getGameEncoder()
          .sendMessage(
              "A magical force stops you from moving for "
                  + player.getMovement().getMagicBindSeconds()
                  + " more seconds.");
      return;
    }
    player.setObjectOptionDelay(4);
    switch (mapObject.getId()) {
      case ObjectId.STAIRS_40388:
        player.getMovement().ladderDownTeleport(new Tile(3385, 10052));
        break;
      case ObjectId.STAIRS_40390:
        player.getMovement().ladderDownTeleport(new Tile(3406, 10145));
        break;
    }
  }
}
