package com.palidinodh.maparea.kharidiandesert.templeoflight.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TempleOfLightNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(1989, 4663), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1996, 4663), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1988, 4658), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1996, 4658), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1992, 4660), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1989, 4652), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1994, 4651), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2021, 4664), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2022, 4658), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2027, 4655), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2032, 4659), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2034, 4665), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2027, 4664), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2027, 4660), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2003, 4614), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2008, 4615), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(2007, 4620), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1992, 4625), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1997, 4629), NpcId.DARK_BEAST_182));
    spawns.add(new NpcSpawn(4, new Tile(1999, 4635), NpcId.DARK_BEAST_182));

    return spawns;
  }
}
