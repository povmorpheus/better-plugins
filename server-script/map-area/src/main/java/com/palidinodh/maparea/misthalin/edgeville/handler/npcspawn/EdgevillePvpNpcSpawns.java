package com.palidinodh.maparea.misthalin.edgeville.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class EdgevillePvpNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(3094, 3499, 20), NpcId.SURGEON_GENERAL_TAFANI));
    spawns.add(new NpcSpawn(new Tile(3095, 3499, 20), NpcId.HEAD_CHEF));
    spawns.add(new NpcSpawn(new Tile(3096, 3499, 20), NpcId.AJJAT));
    spawns.add(new NpcSpawn(new Tile(3097, 3499, 20), NpcId.WIZARD_4399));

    return spawns;
  }
}
