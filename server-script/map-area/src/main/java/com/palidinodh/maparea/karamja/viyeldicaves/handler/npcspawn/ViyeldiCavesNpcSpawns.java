package com.palidinodh.maparea.karamja.viyeldicaves.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class ViyeldiCavesNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2810, 9309), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(4, new Tile(2812, 9299), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(4, new Tile(2800, 9307), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(4, new Tile(2800, 9301), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(4, new Tile(2803, 9295), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(4, new Tile(2810, 9294), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(4, new Tile(2806, 9292), NpcId.DEATH_WING_83));
    spawns.add(new NpcSpawn(2, new Tile(2792, 9327), NpcId.UNGADULU_70));
    spawns.add(new NpcSpawn(4, new Tile(2393, 4712), NpcId.SAN_TOJALON_106));
    spawns.add(new NpcSpawn(4, new Tile(2385, 4706), NpcId.SAN_TOJALON_106));
    spawns.add(new NpcSpawn(4, new Tile(2403, 4711), NpcId.SAN_TOJALON_106));
    spawns.add(new NpcSpawn(4, new Tile(2406, 4703), NpcId.IRVIG_SENAY_100));
    spawns.add(new NpcSpawn(4, new Tile(2416, 4703), NpcId.IRVIG_SENAY_100));
    spawns.add(new NpcSpawn(4, new Tile(2413, 4710), NpcId.IRVIG_SENAY_100));
    spawns.add(new NpcSpawn(4, new Tile(2423, 4705), NpcId.RANALPH_DEVERE_92));
    spawns.add(new NpcSpawn(4, new Tile(2421, 4713), NpcId.RANALPH_DEVERE_92));
    spawns.add(new NpcSpawn(4, new Tile(2417, 4723), NpcId.RANALPH_DEVERE_92));
    spawns.add(new NpcSpawn(4, new Tile(2419, 4681), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(4, new Tile(2409, 4682), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(4, new Tile(2412, 4677), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(new Tile(2391, 4677), NpcId.BOULDER_3967));

    return spawns;
  }
}
