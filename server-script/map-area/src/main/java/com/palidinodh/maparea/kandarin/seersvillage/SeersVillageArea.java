package com.palidinodh.maparea.kandarin.seersvillage;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({10806, 11062})
@ReferenceIdSet(
    primary = 10550,
    secondary = {115, 116})
public class SeersVillageArea extends Area {}
