package com.palidinodh.maparea.wilderness.revenantcaves.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.OPENING_31557, ObjectId.OPENING_31558, ObjectId.OPENING_40387})
public class ExitMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getController().isMagicBound()) {
      player
          .getGameEncoder()
          .sendMessage(
              "A magical force stops you from moving for "
                  + player.getMovement().getMagicBindSeconds()
                  + " more seconds.");
      return;
    }
    switch (mapObject.getId()) {
      case ObjectId.OPENING_31557:
        player.getMovement().teleport(new Tile(3075, 3653));
        break;
      case ObjectId.OPENING_31558:
        player.getMovement().teleport(new Tile(3126, 3832));
        break;
      case ObjectId.OPENING_40387:
        player.getGameEncoder().sendMessage("This exit is too small to squeeze through!");
        break;
    }
  }
}
