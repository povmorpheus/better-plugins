package com.palidinodh.maparea.karamja.tzhaar.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.TZHAAR_KET_ZUH)
class TzhaarKetZuhNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.openOldDialogue("bank", 1);
    } else if (option.getIndex() == 2) {
      player.getBank().open();
    }
  }
}
