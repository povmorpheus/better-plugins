package com.palidinodh.maparea.morytania.temple;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13622)
public class TempleArea extends Area {}
