package com.palidinodh.maparea.wilderness.edgevilledungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class EdgevilleDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3129, 9929), NpcId.THUG_10));
    spawns.add(new NpcSpawn(4, new Tile(3131, 9933), NpcId.THUG_10));
    spawns.add(new NpcSpawn(4, new Tile(3133, 9936), NpcId.THUG_10));
    spawns.add(new NpcSpawn(4, new Tile(3132, 9927), NpcId.THUG_10));
    spawns.add(new NpcSpawn(4, new Tile(3125, 9930), NpcId.THUG_10));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9929), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3118, 9925), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3113, 9926), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3110, 9929), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3114, 9930), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3111, 9933), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3106, 9934), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(3127, 9949), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3123, 9950), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3119, 9950), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3122, 9954), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3127, 9954), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3124, 9957), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3119, 9956), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3115, 9958), NpcId.DEADLY_RED_SPIDER_34));
    spawns.add(new NpcSpawn(4, new Tile(3109, 9954), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3105, 9951), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3105, 9947), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3105, 9956), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3098, 9955), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3092, 9952), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3087, 9949), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3080, 9953), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3086, 9955), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3091, 9957), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3087, 9962), NpcId.BLACK_DEMON_172));
    spawns.add(new NpcSpawn(4, new Tile(3085, 9934), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3088, 9935), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3089, 9938), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3085, 9938), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3089, 9942), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3085, 9941), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3089, 9932), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3091, 9934), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3118, 9972), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9974), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3121, 9971), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3124, 9974), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3122, 9976), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9979), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9985), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3119, 9988), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3123, 9988), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3124, 9991), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9992), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9995), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3125, 9994), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3122, 9998), NpcId.EARTH_WARRIOR_51));
    spawns.add(new NpcSpawn(4, new Tile(3116, 9991), NpcId.EARTH_WARRIOR_51));

    return spawns;
  }
}
