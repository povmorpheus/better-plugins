package com.palidinodh.maparea.kharidiandesert.kalphitehive.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class KalphiteHiveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3277, 9512), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3273, 9514), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3274, 9520), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3278, 9523), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3282, 9527), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3282, 9522), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3278, 9518), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3282, 9516), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3318, 9496), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3322, 9494), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3325, 9497), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3328, 9500), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3330, 9505), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3325, 9508), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3321, 9509), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3319, 9504), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3321, 9499), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3325, 9504), NpcId.KALPHITE_WORKER_28));
    spawns.add(new NpcSpawn(4, new Tile(3269, 9483), NpcId.KALPHITE_GUARDIAN_141));
    spawns.add(new NpcSpawn(4, new Tile(3277, 9479), NpcId.KALPHITE_GUARDIAN_141));
    spawns.add(new NpcSpawn(4, new Tile(3285, 9481), NpcId.KALPHITE_GUARDIAN_141));
    spawns.add(new NpcSpawn(4, new Tile(3279, 9486), NpcId.KALPHITE_GUARDIAN_141));
    spawns.add(new NpcSpawn(4, new Tile(3277, 9492), NpcId.KALPHITE_GUARDIAN_141));
    spawns.add(new NpcSpawn(4, new Tile(3307, 9518), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3306, 9522), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3303, 9526), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3300, 9530), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3301, 9537), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3307, 9537), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3312, 9533), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3314, 9528), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3317, 9524), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3312, 9520), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3310, 9526), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3306, 9532), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3311, 9481), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3317, 9477), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3317, 9484), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3324, 9486), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3323, 9478), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3330, 9484), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3332, 9490), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3337, 9495), NpcId.KALPHITE_SOLDIER_85_957));
    spawns.add(new NpcSpawn(4, new Tile(3297, 9494), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3294, 9497), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3294, 9502), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3300, 9503), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3301, 9499), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3300, 9494), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3296, 9507), NpcId.KALPHITE_LARVA));
    spawns.add(new NpcSpawn(4, new Tile(3300, 9510), NpcId.KALPHITE_LARVA));

    return spawns;
  }
}
