package com.palidinodh.maparea.dragonkinislands.fossilisland.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.WidgetManager.MakeXType;
import com.palidinodh.osrscore.model.entity.player.dialogue.MakeXDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ObjectId.SPACE,
  ObjectId.SPACE_30565,
  ObjectId.SPACE_30566,
  ObjectId.SPACE_30567,
  ObjectId.SPACE_30568,
  ObjectId.BIRDHOUSE_EMPTY,
  ObjectId.BIRDHOUSE,
  ObjectId.BIRDHOUSE_30555,
  ObjectId.OAK_BIRDHOUSE_EMPTY,
  ObjectId.OAK_BIRDHOUSE,
  ObjectId.OAK_BIRDHOUSE_30558,
  ObjectId.WILLOW_BIRDHOUSE_EMPTY,
  ObjectId.WILLOW_BIRDHOUSE,
  ObjectId.WILLOW_BIRDHOUSE_30561,
  ObjectId.TEAK_BIRDHOUSE_EMPTY,
  ObjectId.TEAK_BIRDHOUSE,
  ObjectId.TEAK_BIRDHOUSE_30564,
  ObjectId.MAPLE_BIRDHOUSE_EMPTY,
  ObjectId.MAPLE_BIRDHOUSE,
  ObjectId.MAPLE_BIRDHOUSE_31829,
  ObjectId.MAHOGANY_BIRDHOUSE_EMPTY,
  ObjectId.MAHOGANY_BIRDHOUSE,
  ObjectId.MAHOGANY_BIRDHOUSE_31832,
  ObjectId.YEW_BIRDHOUSE_EMPTY,
  ObjectId.YEW_BIRDHOUSE,
  ObjectId.YEW_BIRDHOUSE_31835,
  ObjectId.MAGIC_BIRDHOUSE_EMPTY,
  ObjectId.MAGIC_BIRDHOUSE,
  ObjectId.MAGIC_BIRDHOUSE_31838,
  ObjectId.REDWOOD_BIRDHOUSE_EMPTY,
  ObjectId.REDWOOD_BIRDHOUSE,
  ObjectId.REDWOOD_BIRDHOUSE_31841
})
class BirdHouseMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var trap = player.getPlugin(HunterPlugin.class).getBirdHouseTrap(mapObject);
    if (trap == null) {
      player.getGameEncoder().sendMessage("Maybe you can build something here.");
      return;
    }
    switch (option.getText()) {
      case "interact":
        {
          player
              .getGameEncoder()
              .sendMessage("Your birdhouse trap has caught " + trap.getBirdsCaught() + " birds.");
          break;
        }
      case "seeds":
        {
          if (trap.isStarted()) {
            player
                .getGameEncoder()
                .sendMessage("Your birdhouse trap is full of seed and is catching birds.");
          } else {
            player
                .getGameEncoder()
                .sendMessage("Your birdhouse trap has space for seed and is not catching birds.");
          }
          break;
        }
      case "empty":
      case "dismantle":
        player.getPlugin(HunterPlugin.class).dismantleBirdHouse(trap);
        break;
    }
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    var trap = player.getPlugin(HunterPlugin.class).getBirdHouseTrap(mapObject);
    if (trap == null) {
      player.getPlugin(HunterPlugin.class).buildBirdHouse(mapObject, item);
      return;
    }
    player.openDialogue(
        new MakeXDialogue(
            "How many do you wish to use?",
            MakeXType.MAKE,
            10,
            (c, s) -> {
              var useItem = new Item(item.getId(), s);
              player.getPlugin(HunterPlugin.class).addSeedsBirdHouse(trap, useItem);
            },
            item.getId()));
  }
}
