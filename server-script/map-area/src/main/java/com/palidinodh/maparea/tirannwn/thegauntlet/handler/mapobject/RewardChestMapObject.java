package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tirannwn.TheGauntletSubPlugin.Reward;
import com.palidinodh.playerplugin.tirannwn.TirannwnPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.REWARD_CHEST_37341)
class RewardChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.getGameEncoder().sendMessage("You open the chest.");
    var gauntlet = player.getPlugin(TirannwnPlugin.class).getTheGauntlet();
    var reward = gauntlet.getReward();
    if (reward == Reward.NONE) {
      player.getGameEncoder().sendMessage("You don't find anything in the chest.");
      return;
    }
    player.getGameEncoder().sendMessage("You find some treasure in the chest!");
    gauntlet.setReward(Reward.NONE);
    switch (reward) {
      case CRYSTALLINE:
        gauntlet.openCrystalline();
        break;
      case CORRUPTED:
        gauntlet.openCorrupted();
        break;
    }
  }
}
