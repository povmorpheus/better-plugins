package com.palidinodh.maparea.fremennikprovince.fremennikslayerdungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10907, 10908, 11164})
public class FremennikSlayerDungeonArea extends Area {}
