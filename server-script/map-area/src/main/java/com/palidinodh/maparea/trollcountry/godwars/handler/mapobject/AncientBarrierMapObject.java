package com.palidinodh.maparea.trollcountry.godwars.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.maparea.trollcountry.godwars.GodWarsDungeonArea;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ANCIENT_BARRIER_42967)
public class AncientBarrierMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getController().is(BossInstanceController.class)) {
      player.getController().stop();
    } else {
      GodWarsDungeonArea.joinPublicNex(player);
    }
  }
}
