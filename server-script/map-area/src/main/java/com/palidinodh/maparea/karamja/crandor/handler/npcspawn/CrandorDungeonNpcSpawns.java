package com.palidinodh.maparea.karamja.crandor.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CrandorDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2834, 9654), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2838, 9652), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2841, 9649), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2841, 9645), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2843, 9641), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2840, 9639), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2843, 9634), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2841, 9631), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2844, 9629), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2840, 9625), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2837, 9623), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2839, 9613), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(4, new Tile(2839, 9609), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(4, new Tile(2842, 9608), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(4, new Tile(2838, 9604), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(4, new Tile(2835, 9601), NpcId.LESSER_DEMON_82));
    spawns.add(new NpcSpawn(8, new Tile(2852, 9636), NpcId.ELVARG_83_6349));

    return spawns;
  }
}
