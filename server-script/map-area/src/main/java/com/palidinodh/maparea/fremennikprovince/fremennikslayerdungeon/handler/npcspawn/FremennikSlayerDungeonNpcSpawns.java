package com.palidinodh.maparea.fremennikprovince.fremennikslayerdungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class FremennikSlayerDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2798, 9996), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2792, 9999), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2793, 9993), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2787, 9992), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2790, 9996), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2783, 9993), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2785, 9997), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2782, 10000), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2778, 10000), NpcId.CAVE_CRAWLER_23));
    spawns.add(new NpcSpawn(4, new Tile(2790, 10018), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2794, 10015), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2795, 10019), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2799, 10013), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2803, 10014), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2806, 10017), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2806, 10022), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2801, 10022), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2798, 10017), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2803, 10019), NpcId.ROCKSLUG_29));
    spawns.add(new NpcSpawn(4, new Tile(2802, 10030), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2800, 10035), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2794, 10038), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2784, 10038), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2782, 10034), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2788, 10031), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2794, 10030), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2798, 10033), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2792, 10034), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2787, 10036), NpcId.COCKATRICE_37));
    spawns.add(new NpcSpawn(4, new Tile(2761, 10012), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2757, 10010), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2763, 10007), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2766, 10002), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2765, 9998), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2761, 9996), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2757, 9995), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2757, 10000), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2756, 10005), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2760, 10005), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2761, 10000), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(2743, 10001), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2738, 10006), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2747, 10006), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2739, 10013), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2744, 10017), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2745, 10012), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2742, 10010), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2742, 10005), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2712, 10030), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2706, 10031), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2700, 10031), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2697, 10026), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2700, 10022), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2705, 10021), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2709, 10022), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2712, 10025), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2708, 10027), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2702, 10027), NpcId.JELLY_78));
    spawns.add(new NpcSpawn(4, new Tile(2715, 10010), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2720, 10012), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2726, 10010), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2728, 10004), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2730, 9998), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2726, 9995), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2721, 9996), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2718, 10001), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2716, 10005), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2722, 10006), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2724, 10000), NpcId.TUROTH_89));
    spawns.add(new NpcSpawn(4, new Tile(2705, 9994), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2701, 9992), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2695, 9993), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2693, 10000), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2699, 10002), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2704, 10000), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2697, 9999), NpcId.KURASK_106));
    spawns.add(new NpcSpawn(4, new Tile(2702, 9997), NpcId.KURASK_106));

    return spawns;
  }
}
