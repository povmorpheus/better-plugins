package com.palidinodh.maparea.karamja.tzhaar.handler.mapobject;

import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tzhaar.TzhaarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CAVE_EXIT_30283)
class TheInfernoExitMapObject implements MapObjectHandler {

  private static void practiceDialogue(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Unpause the minigame.",
                (c, s) -> {
                  plugin.setMinigamePaused(false);
                  plugin.infernoWaveTeleport();
                }),
            new DialogueOption(
                "Get items.",
                (c, s) -> {
                  if (player.isLocked()) {
                    return;
                  }
                  player.openShop("spawn");
                }),
            new DialogueOption(
                "Set skill levels.",
                (c, s) -> {
                  skillsDialogue(player);
                }),
            new DialogueOption(
                "Exit the minigame.",
                (c, s) -> {
                  plugin.endInferno();
                })));
  }

  private static void skillsDialogue(Player player) {
    var cs2 = new ScreenSelectionCs2();
    player.openDialogue(
        new LargeOptions2Dialogue(
            ScreenSelectionCs2.builder().title("Choose a Skill"),
            new DialogueOption(
                "Attack",
                (c, s) -> {
                  setLevel(player, Skills.ATTACK);
                }),
            new DialogueOption(
                "Strength",
                (c, s) -> {
                  setLevel(player, Skills.STRENGTH);
                }),
            new DialogueOption(
                "Ranged",
                (c, s) -> {
                  setLevel(player, Skills.RANGED);
                }),
            new DialogueOption(
                "Magic",
                (c, s) -> {
                  setLevel(player, Skills.MAGIC);
                }),
            new DialogueOption(
                "Defence",
                (c, s) -> {
                  setLevel(player, Skills.DEFENCE);
                }),
            new DialogueOption(
                "Hitpoints",
                (c, s) -> {
                  setLevel(player, Skills.HITPOINTS);
                }),
            new DialogueOption(
                "Prayer",
                (c, s) -> {
                  setLevel(player, Skills.PRAYER);
                })));
  }

  private static void setLevel(Player player, int skillId) {
    player
        .getGameEncoder()
        .sendEnterAmount(
            "Choose a Level",
            ie -> {
              skillsDialogue(player);
              if (ie < 1 || ie > 99) {
                return;
              }
              var plugin = player.getPlugin(TzhaarPlugin.class);
              if (plugin.getMinigameStats() == null) {
                return;
              }
              plugin.getMinigameStats()[skillId] = ie;
              player.getSkills().setLevel(skillId, ie);
              player.getGameEncoder().sendSkillLevel(skillId);
            });
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    if (plugin.getInferno().getWave() == 0) {
      return;
    }
    if (!player.getController().is("TzhaarInfernoController")) {
      return;
    }
    if (plugin.isMinigamePractice() && plugin.isMinigamePaused()) {
      practiceDialogue(player);
      return;
    }
    plugin.endInferno();
  }
}
