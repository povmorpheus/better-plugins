package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CombatTrainingCampNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2532, 3375), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2530, 3375), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2528, 3375), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2526, 3375), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2524, 3375), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2523, 3371), NpcId.GUARD_1112));
    spawns.add(new NpcSpawn(4, new Tile(2530, 3367), NpcId.GUARD_1112));
    spawns.add(new NpcSpawn(4, new Tile(2524, 3362), NpcId.GUARD_1111));
    spawns.add(new NpcSpawn(4, new Tile(2519, 3366), NpcId.GUARD_1112));
    spawns.add(new NpcSpawn(4, new Tile(2516, 3354), NpcId.GUARD_1111));
    spawns.add(new NpcSpawn(4, new Tile(2520, 3355), NpcId.GUARD_1111));
    spawns.add(new NpcSpawn(4, new Tile(2516, 3368), NpcId.GUARD_1113));
    spawns.add(new NpcSpawn(4, new Tile(2504, 3368), NpcId.GUARD_1113));
    spawns.add(new NpcSpawn(4, new Tile(2511, 3378), NpcId.GUARD_1111));
    spawns.add(new NpcSpawn(4, new Tile(2511, 3383), NpcId.GUARD_1111));
    spawns.add(new NpcSpawn(4, new Tile(2522, 3382), NpcId.GUARD_1111));
    spawns.add(new NpcSpawn(4, new Tile(2528, 3379), NpcId.GUARD_1113));

    return spawns;
  }
}
