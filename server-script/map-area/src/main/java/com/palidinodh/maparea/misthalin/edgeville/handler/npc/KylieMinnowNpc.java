package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.KYLIE_MINNOW)
class KylieMinnowNpc implements NpcHandler {

  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player
        .getGameEncoder()
        .sendMessage("I can upgrade your angler pieces to the Spirit angler versions");
    return;
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    switch (itemId) {
      case ItemId.ANGLER_HAT:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow wants </col>2,400 spirit flakes<col=000000> to upgrade your hat.<br><col=000000> If you change back later,</col> you will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      var spiritFlakeCount = player.getInventory().getCount(ItemId.SPIRIT_FLAKES);
                      if (spiritFlakeCount > 2399) {
                        player.getInventory().deleteItem(itemId, 1);
                        player.getInventory().deleteItem(ItemId.SPIRIT_FLAKES, 2400);
                        player.getInventory().addOrDropItem(ItemId.SPIRIT_ANGLER_HEADBAND);
                        player
                            .getGameEncoder()
                            .sendMessage("Kylie Minnow exchanges your hat for Spirit Angler kit.");
                      } else {
                        player
                            .getGameEncoder()
                            .sendMessage("You do not have enough Spirit flakes.");
                        return;
                      }
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.ANGLER_TOP:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow wants </col>2,400 spirit flakes<col=000000> to upgrade your top.<br><col=000000> If you change back later,</col> you will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      var spiritFlakeCount = player.getInventory().getCount(ItemId.SPIRIT_FLAKES);
                      if (spiritFlakeCount > 2399) {
                        player.getInventory().deleteItem(itemId, 1);
                        player.getInventory().deleteItem(ItemId.SPIRIT_FLAKES, 2400);
                        player.getInventory().addOrDropItem(ItemId.SPIRIT_ANGLER_TOP);
                        player
                            .getGameEncoder()
                            .sendMessage("Kylie Minnow exchanges your top for Spirit Angler kit.");
                      } else {
                        player
                            .getGameEncoder()
                            .sendMessage("You do not have enough Spirit flakes.");
                        return;
                      }
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.ANGLER_WADERS:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow wants </col>2,400 spirit flakes<col=000000> to upgrade your waders.<br><col=000000> If you change back later,</col> you will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      var spiritFlakeCount = player.getInventory().getCount(ItemId.SPIRIT_FLAKES);
                      if (spiritFlakeCount > 2399) {
                        player.getInventory().deleteItem(itemId, 1);
                        player.getInventory().deleteItem(ItemId.SPIRIT_FLAKES, 2400);
                        player.getInventory().addOrDropItem(ItemId.SPIRIT_ANGLER_WADERS);
                        player
                            .getGameEncoder()
                            .sendMessage(
                                "Kylie Minnow exchanges your waders for Spirit Angler kit.");
                      } else {
                        player.getGameEncoder().sendMessage("You do not have enough Spirit flakes");
                        return;
                      }
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.ANGLER_BOOTS:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow wants </col>2,400 spirit flakes<col=000000> to upgrade your boots.<br><col=000000> If you change back later,</col> you will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      var spiritFlakeCount = player.getInventory().getCount(ItemId.SPIRIT_FLAKES);
                      if (spiritFlakeCount > 2399) {
                        player.getInventory().deleteItem(itemId, 1);
                        player.getInventory().deleteItem(ItemId.SPIRIT_FLAKES, 2400);
                        player.getInventory().addOrDropItem(ItemId.SPIRIT_ANGLER_BOOTS);
                        player
                            .getGameEncoder()
                            .sendMessage(
                                "Kylie Minnow exchanges your boots for Spirit Angler kit.");
                      } else {
                        player
                            .getGameEncoder()
                            .sendMessage("You do not have enough Spirit flakes.");
                        return;
                      }
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.SPIRIT_ANGLER_HEADBAND:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow can change your headband back to normal.<br>You will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      player.getInventory().deleteItem(itemId, 1);
                      player.getInventory().addOrDropItem(ItemId.ANGLER_HAT);
                      player
                          .getGameEncoder()
                          .sendMessage(
                              "Kylie Minnow exchanges your Spirit Angler kit for an angler hat.");
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.SPIRIT_ANGLER_TOP:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow can change your top back to normal.<br>You will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      player.getInventory().deleteItem(itemId, 1);
                      player.getInventory().addOrDropItem(ItemId.ANGLER_TOP);
                      player
                          .getGameEncoder()
                          .sendMessage(
                              "Kylie Minnow exchanges your Spirit Angler kit for an angler top.");
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.SPIRIT_ANGLER_WADERS:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow can change your waders back to normal.<br>You will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      player.getInventory().deleteItem(itemId, 1);
                      player.getInventory().addOrDropItem(ItemId.ANGLER_WADERS);
                      player
                          .getGameEncoder()
                          .sendMessage(
                              "Kylie Minnow exchanges your Spirit Angler kit for angler waders.");
                    }),
                new DialogueOption("Cancel")));
        break;
      case ItemId.SPIRIT_ANGLER_BOOTS:
        player.openDialogue(
            new OptionsDialogue(
                "<col=000000>Kylie Minnow can change your boots back to normal.<br>You will not<col=000000> get your spirit flakes back.",
                new DialogueOption(
                    "Proceed",
                    (c, s) -> {
                      player.getInventory().deleteItem(itemId, 1);
                      player.getInventory().addOrDropItem(ItemId.ANGLER_BOOTS);
                      player
                          .getGameEncoder()
                          .sendMessage(
                              "Kylie Minnow exchanges your Spirit Angler kit for angler boots.");
                    }),
                new DialogueOption("Cancel")));
        break;
    }
  }
}
