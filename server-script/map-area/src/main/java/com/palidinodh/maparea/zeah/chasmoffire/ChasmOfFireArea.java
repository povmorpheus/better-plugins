package com.palidinodh.maparea.zeah.chasmoffire;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(5789)
public class ChasmOfFireArea extends Area {}
