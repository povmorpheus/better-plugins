package com.palidinodh.maparea.fremennikprovince;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10559, 10560})
public class IcebergArea extends Area {}
