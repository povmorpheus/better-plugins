package com.palidinodh.maparea.feldiphills.hunterarea.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class MogreCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2515, 9291), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2514, 9297), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2515, 9302), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2521, 9290), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2520, 9296), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2521, 9302), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2526, 9300), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2525, 9291), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(4, new Tile(2525, 9295), NpcId.MOGRE_60));

    return spawns;
  }
}
