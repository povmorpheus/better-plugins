package com.palidinodh.maparea.asgarnia.donator.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.NIEVE)
class DzoneNieveNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (option.getIndex() == 0) {
      plugin.openMasterMenuDialogue();
    } else if (option.getIndex() == 2) {
      plugin.getChooseAssignment().openChooseMasterDialogue();
    } else if (option.getIndex() == 3) {
      player.openShop("slayer");
    } else if (option.getIndex() == 4) {
      plugin.getRewards().open();
    }
  }
}
