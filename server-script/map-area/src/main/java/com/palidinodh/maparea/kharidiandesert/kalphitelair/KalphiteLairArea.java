package com.palidinodh.maparea.kharidiandesert.kalphitelair;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13972)
public class KalphiteLairArea extends Area {}
