package com.palidinodh.maparea.kandarin.legendsguild.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LegendsGuildNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(6, new Tile(2727, 3379), NpcId.SIEGFRIED_ERKLE));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2733, 3374), NpcId.BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2691, 3332), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2702, 3330), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2707, 3335), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2695, 3340), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2705, 3341), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2714, 3335), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2718, 3332), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2718, 3338), NpcId.GRIZZLY_BEAR_21));
    spawns.add(new NpcSpawn(4, new Tile(2701, 3345), NpcId.GRIZZLY_BEAR_21));

    return spawns;
  }
}
