package com.palidinodh.maparea.morytania.sisterhoodsanctuary;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14999, 15000, 15001, 15255, 15256, 15257, 15511, 15512, 15513, 15515})
public class SisterhoodSanctuaryArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }
}
