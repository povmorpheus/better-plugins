package com.palidinodh.playermisc;

import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.playerplugin.loot.LootPlugin;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.worldevent.wildernesskey.WildernessKeyEvent;

public class PickupItem {

  public static void pickup(Player player, int itemId, int tileX, int tileY) {
    var plugin = player.getPlugin(LootPlugin.class);
    plugin.refreshItems();
    if (canUseLootWidget(player, itemId, tileX, tileY)) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.LOOT_1022);
    } else {
      complete(player, itemId, tileX, tileY);
    }
  }

  public static void complete(Player player, int itemId, int tileX, int tileY) {
    var result = player.getWorld().pickupMapItem(player, itemId, tileX, tileY);
    if (result == null) {
      return;
    }
    if (result.partialSuccess()
        && WildernessPlugin.isBloodyKey(itemId)
        && (player.getArea().inWilderness() || player.getArea().inPvpWorld())) {
      player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
      player.getMovement().setEnergy(0);
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>Carrying the key prevents you from teleporting.");
      if (WildernessPlugin.isActiveBloodierKey(itemId)) {
        player.getWorld().getWorldEvent(WildernessKeyEvent.class).updateTrackedLocation(player);
        player
            .getWorld()
            .sendMessage(
                "<col=ff0000>A "
                    + ItemDef.getName(itemId)
                    + " has been picked up by "
                    + player.getUsername()
                    + " at level "
                    + player.getArea().getWildernessLevel()
                    + " wilderness!");
      }
    }
  }

  private static boolean canUseLootWidget(Player player, int itemId, int tileX, int tileY) {
    if (player.getArea().inWilderness()) {
      return false;
    }
    var plugin = player.getPlugin(LootPlugin.class);
    if (player.getGameEncoder().getVarbit(VarbitId.IS_MOBILE) == 1) {
      if (!plugin.isMobileWidgetEnabled()) {
        return false;
      }
    } else {
      if (!plugin.isWidgetEnabled()) {
        return false;
      }
    }
    if (player.getX() != tileX) {
      return false;
    }
    if (player.getY() != tileY) {
      return false;
    }
    var itemSize = plugin.getItems().size();
    if (itemSize < 2) {
      return false;
    }
    if (itemSize > 15) {
      return false;
    }
    return true;
  }
}
