package com.palidinodh.playerplugin.bond.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.OLD_SCHOOL_BOND, ItemId.BOND_32318})
class BondItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(BondPlugin.class);
    switch (item.getId()) {
      case ItemId.OLD_SCHOOL_BOND:
        item.replace(new Item(ItemId.BOND_32318, 50));
        break;
      case ItemId.BOND_32318:
        plugin.setPouch(plugin.getPouch() + item.getAmount());
        item.remove();
        player.getGameEncoder().sendMessage("Your bonds have been added to your pouch.");
        break;
    }
  }
}
