package com.palidinodh.playerplugin.grandexchange;

import com.palidinodh.cache.clientscript2.GrandExchangeCreateOfferCs2;
import com.palidinodh.cache.clientscript2.GrandExchangeViewOfferCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PNumber;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GrandExchangeOffers {

  private Player player;
  private GrandExchangePlugin plugin;

  public void collectAll(int option) {
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return;
    }
    for (var i = plugin.getItems().size() - 1; i >= 0; i--) {
      var item = plugin.getItems().get(i);
      plugin.setRefresh(0);
      if (item.getReturnCoins() > 0) {
        plugin.setRefresh(0);
        if (!collect(i, true, option)) {
          break;
        }
      }
      if (item.getReturnAmount() > 0) {
        plugin.setRefresh(0);
        if (!collect(i, false, option)) {
          break;
        }
      }
    }
    plugin.update();
  }

  public boolean collect(int slot, boolean isCoins, int option) {
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return false;
    }
    if (slot < 0 || slot >= plugin.getItems().size()) {
      player.getGameEncoder().sendMessage("Invalid slot.");
      return false;
    }
    var exchangeItem = plugin.getItems().get(slot);
    var collected = 0;
    var coins = 0;
    Item item;
    if (isCoins) {
      item = new Item(ItemId.COINS, exchangeItem.getReturnCoins());
      coins = exchangeItem.getTotalCoins();
    } else {
      var itemId = exchangeItem.getId();
      var returnAmount = exchangeItem.getReturnAmount();
      var itemDef = ItemDefinition.getDefinition(itemId);
      if (returnAmount > 1 && !itemDef.isStackable() && itemDef.getNotedId() != -1) {
        itemId = itemDef.getNotedId();
      }
      item = new Item(itemId, returnAmount);
    }
    item.setAmount(player.getInventory().canAddAmount(item));
    if (item.getAmount() == 0) {
      player.getInventory().notEnoughSpace();
      return false;
    }
    if (!isCoins) {
      collected = item.getAmount();
    }
    player.getInventory().addOrDropItem(item);
    RequestManager.addGECollectOffer(player, slot, collected, coins);
    exchangeItem.collected(collected, coins);
    if (player.getWidgetManager().getInteractiveOverlay() == WidgetId.GRAND_EXCHANGE_OFFER_1026) {
      loadExisting(slot);
    }
    if (exchangeItem.canRemove()) {
      exchangeItem.setId(-1);
      plugin.getItems().remove(slot);
      if (player.getWidgetManager().getInteractiveOverlay() == WidgetId.GRAND_EXCHANGE_OFFER_1026) {
        player
            .getBank()
            .pinRequiredAction(
                () ->
                    player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
      }
    }
    plugin.update();
    player.log(PlayerLogType.EXCHANGE, "collected " + item.getLogName());
    return true;
  }

  public void view(int slot) {
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return;
    }
    player.putAttribute("exchange_slot", slot + 1);
    if (player.getWidgetManager().getInteractiveOverlay() != WidgetId.GRAND_EXCHANGE_OFFER_1026) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_OFFER_1026);
    }
    RequestManager.getInstance().addGEPriceAverage(player);
  }

  public void abort(int slot) {
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return;
    }
    if (slot < 0 || slot >= plugin.getItems().size()) {
      player.getGameEncoder().sendMessage("Invalid slot.");
      return;
    }
    RequestManager.addGEAbortOffer(player, slot);
    plugin.getItems().get(slot).setAborted(true);
    plugin.update();
    loadExisting(slot);
  }

  public void create(boolean isSell, int itemId) {
    var itemDef = ItemDefinition.getDefinition(itemId);
    var quantity = 1;
    if (isSell) {
      if (itemDef.isStackable()
          || itemDef.getNotedId() != -1 && player.getInventory().hasItem(itemDef.getNotedId())) {
        quantity = player.getInventory().getCount(itemId, itemDef.getNotedId());
      }
    }
    create(isSell, itemId, quantity, ItemDef.getConfiguredExchangePrice(itemId));
  }

  public void create(boolean isSell, int itemId, int quantity, int price) {
    if (player.getGameMode().isIronType()) {
      return;
    }
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return;
    }
    if (plugin.getItems().size() >= GrandExchangePlugin.MAX_OFFERS) {
      player.getGameEncoder().sendMessage("You can't create any more offers.");
      return;
    }
    player.putAttribute("exchange_sell", isSell);
    player.putAttribute("exchange_item_id", itemId + 1);
    player.putAttribute("exchange_item_quantity", quantity);
    player.putAttribute("exchange_item_price", price);
    if (player.getWidgetManager().getInteractiveOverlay() != WidgetId.GRAND_EXCHANGE_OFFER_1026) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_OFFER_1026);
    }
    loadCreated();
    RequestManager.getInstance().addGEPriceAverage(player);
  }

  public void loadExisting(int slot) {
    if (slot < 0 || slot >= plugin.getItems().size()) {
      return;
    }
    var item = plugin.getItems().get(slot);
    player.putAttribute("exchange_sell", item.isStateSelling());
    player.putAttribute("exchange_item_id", item.getId() + 1);
    player.putAttribute("exchange_item_quantity", item.getAmount());
    player.putAttribute("exchange_item_price", item.getPrice());
    var cs2 = GrandExchangeViewOfferCs2.builder();
    cs2.sell(item.isStateSelling());
    cs2.aborted(item.isAborted());
    cs2.itemId(item.getId());
    cs2.itemQuantity(item.getAmount());
    cs2.itemPrice(item.getPrice());
    cs2.defaultPrice(ItemDef.getConfiguredExchangePrice(item.getId()));
    cs2.exchangedQuantity(item.getExchangedAmount());
    cs2.exchangedPrice(item.getExchangedPrice());
    cs2.availableQuantity(item.getReturnAmount());
    cs2.availableCoins(item.getReturnCoins());
    player.getGameEncoder().sendClientScriptData(cs2.build());
  }

  public void loadCreated() {
    var cs2 = GrandExchangeCreateOfferCs2.builder();
    cs2.sell(player.getAttributeBool("exchange_sell"));
    cs2.itemId(player.getAttributeInt("exchange_item_id") - 1);
    cs2.itemQuantity(player.getAttributeInt("exchange_item_quantity"));
    cs2.itemPrice(player.getAttributeInt("exchange_item_price"));
    cs2.defaultPrice(
        ItemDef.getConfiguredExchangePrice(player.getAttributeInt("exchange_item_id") - 1));
    player.getGameEncoder().sendClientScriptData(cs2.build());
  }

  public void addBuy(int id, int amount, int price) {
    if (player.getGameMode().isIronType()) {
      return;
    }
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return;
    }
    if (player.isUsergroup(UserRank.YOUTUBER)) {
      return;
    }
    if (plugin.getItems().size() >= GrandExchangePlugin.MAX_OFFERS) {
      player.getGameEncoder().sendMessage("You can't create any more offers.");
      return;
    }
    if (id < 0) {
      player.getGameEncoder().sendMessage("Invalid id.");
      return;
    }
    if (amount < 1 || amount > Item.MAX_AMOUNT) {
      player
          .getGameEncoder()
          .sendMessage(
              "The amount can't exceed " + PNumber.abbreviateNumber(Item.MAX_AMOUNT) + ".");
      return;
    }
    if (price < 1 || price > Item.MAX_AMOUNT) {
      player
          .getGameEncoder()
          .sendMessage("The price can't exceed " + PNumber.abbreviateNumber(Item.MAX_AMOUNT) + ".");
      return;
    }
    if (!PNumber.canMultiplyInt(amount, price, Item.MAX_AMOUNT)) {
      player
          .getGameEncoder()
          .sendMessage(
              "The total cost can't exceed "
                  + PNumber.abbreviateNumber(Item.MAX_AMOUNT)
                  + " coins.");
      return;
    }
    var itemDef = ItemDefinition.getDefinition(id);
    if (itemDef.isNoted()) {
      player.getGameEncoder().sendMessage("You can't select noted items.");
      return;
    }
    if (!itemDef.isExchangeable()) {
      player.getGameEncoder().sendMessage("This item is unlistable.");
      return;
    }
    if (ItemDef.getUntradable(id)) {
      player.getGameEncoder().sendMessage("Can't select untradable items.");
      return;
    }
    if (ItemDef.isFree(id)) {
      player.getGameEncoder().sendMessage("Can't select free items.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < amount * price) {
      player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.COINS, amount * price);
    RequestManager.addGEBuyOffer(player, id, itemDef.getName(), amount, price);
    plugin.getItems().add(new GrandExchangeItem(GrandExchangeItem.STATE_BUYING, id, amount, price));
    plugin.update();
    player
        .getBank()
        .pinRequiredAction(
            () -> player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
    player.log(
        PlayerLogType.EXCHANGE,
        "buy offer for "
            + "[ID: "
            + id
            + "; Identifier: "
            + ItemId.valueOf(id)
            + "; Quantity: "
            + PNumber.formatNumber(amount)
            + "; Price: "
            + PNumber.formatNumber(price)
            + "] "
            + itemDef.getName()
            + " for "
            + PNumber.formatNumber(amount * (long) price)
            + " coins");
  }

  public void addSell(int id, int amount, int price) {
    if (player.getGameMode().isIronType()) {
      return;
    }
    if (player.isUsergroup(UserRank.YOUTUBER)) {
      return;
    }
    if (plugin.getRefresh() != 0) {
      player.getGameEncoder().sendMessage("Your items are currently refreshing.");
      return;
    }
    if (plugin.getItems().size() >= GrandExchangePlugin.MAX_OFFERS) {
      player.getGameEncoder().sendMessage("You can't create any more offers.");
      return;
    }
    if (id < 0) {
      player.getGameEncoder().sendMessage("Invalid id.");
      return;
    }
    if (amount < 1 || amount > Item.MAX_AMOUNT) {
      player
          .getGameEncoder()
          .sendMessage(
              "The amount can't exceed " + PNumber.abbreviateNumber(Item.MAX_AMOUNT) + ".");
      return;
    }
    if (price < 1 || price > Item.MAX_AMOUNT) {
      player
          .getGameEncoder()
          .sendMessage("The price can't exceed " + PNumber.abbreviateNumber(Item.MAX_AMOUNT) + ".");
      return;
    }
    if (!PNumber.canMultiplyInt(amount, price, Item.MAX_AMOUNT)) {
      player
          .getGameEncoder()
          .sendMessage(
              "The total cost can't exceed " + PNumber.abbreviateNumber(Item.MAX_AMOUNT) + ".");
      return;
    }
    var itemDef = ItemDefinition.getDefinition(id);
    if (itemDef.isNoted()) {
      player.getGameEncoder().sendMessage("You can't select noted items.");
      return;
    }
    if (!itemDef.isExchangeable()) {
      player.getGameEncoder().sendMessage("This item is unlistable.");
      return;
    }
    if (ItemDef.getUntradable(id)) {
      player.getGameEncoder().sendMessage("Can't select untradable items.");
      return;
    }
    if (ItemDef.isFree(id)) {
      player.getGameEncoder().sendMessage("Can't select free items.");
      return;
    }

    if (player.getInventory().getCount(id, itemDef.getNotedId()) < amount) {
      player.getGameEncoder().sendMessage("You don't have enough of that item to do this.");
      return;
    }
    var inventoryCount = player.getInventory().getCount(id);
    player.getInventory().deleteItem(id, Math.min(amount, inventoryCount));
    if (inventoryCount < amount) {
      player.getInventory().deleteItem(itemDef.getNotedId(), amount - inventoryCount);
    }
    RequestManager.addGESellOffer(player, id, itemDef.getName(), amount, price);
    plugin
        .getItems()
        .add(new GrandExchangeItem(GrandExchangeItem.STATE_SELLING, id, amount, price));
    player
        .getBank()
        .pinRequiredAction(
            () -> player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
    player.log(
        PlayerLogType.EXCHANGE,
        "created a sell offer for "
            + "[ID: "
            + id
            + "; Identifier: "
            + ItemId.valueOf(id)
            + "; Quantity: "
            + PNumber.formatNumber(amount)
            + "; Price: "
            + PNumber.formatNumber(price)
            + "] "
            + itemDef.getName()
            + " for a total of "
            + PNumber.formatNumber(amount * (long) price)
            + " coins");
  }
}
