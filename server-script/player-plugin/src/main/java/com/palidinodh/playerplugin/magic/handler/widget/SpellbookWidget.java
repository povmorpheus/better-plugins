package com.palidinodh.playerplugin.magic.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.magic.MagicPlugin;
import com.palidinodh.playerplugin.magic.SpellOnItem;
import com.palidinodh.playerplugin.magic.SpellOnMapObject;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;
import java.util.Arrays;

@ReferenceId(WidgetId.SPELLBOOK)
class SpellbookWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var spellbookChild = SpellbookChild.get(childId);
    if (spellbookChild == null) {
      return;
    }
    var spellTeleport = SpellTeleport.get(spellbookChild);
    if (spellTeleport != null && option == 0) {
      spellTeleport.teleport(player);
      return;
    }
    switch (spellbookChild) {
      case LUMBRIDGE_HOME_TELEPORT:
      case EDGEVILLE_HOME_TELEPORT:
      case LUNAR_HOME_TELEPORT:
      case ARCEUUS_HOME_TELEPORT:
        {
          var plugin = player.getPlugin(TeleportsPlugin.class).getMainTeleports();
          switch (option) {
            case 9:
              {
                plugin.selectTeleport(plugin.getRecent(0));
                break;
              }
          }
          break;
        }
      case SPELL_FILTERS:
        {
          switch (slot) {
            case 0:
              player.getMagic().setShowCombatSpells(!player.getMagic().getShowCombatSpells());
              break;
            case 1:
              player.getMagic().setShowTeleportSpells(!player.getMagic().getShowTeleportSpells());
              break;
            case 2:
              player.getMagic().setShowUtilitySpells(!player.getMagic().getShowUtilitySpells());
              break;
            case 3:
              player.getMagic().setShowLackLevelSpells(!player.getMagic().getShowLackLevelSpells());
              break;
            case 4:
              player.getMagic().setShowLackRunesSpells(!player.getMagic().getShowLackRunesSpells());
              break;
          }
          break;
        }
      case LOW_LEVEL_ALCHEMY:
      case HIGH_LEVEL_ALCHEMY:
        {
          if (option == 9) {
            player.getPlugin(MagicPlugin.class).alchWarningsDialogue();
          }
          break;
        }
      case TELEPORT_TO_BOUNTY_TARGET:
        {
          var plugin = player.getPlugin(BountyHunterPlugin.class);
          switch (option) {
            case 0:
              plugin.selectTeleportToTarget(false);
              break;
            case 9:
              {
                plugin.setTeleportWarning(!plugin.isTeleportWarning());
                player
                    .getGameEncoder()
                    .sendMessage("Teleport warning: " + plugin.isTeleportWarning());
                break;
              }
          }
          break;
        }
      case CHARGE:
        {
          if (player.getSkills().getLevel(Skills.MAGIC) < 80) {
            player
                .getGameEncoder()
                .sendMessage("Your Magic level is not high enough for this spell.");
            break;
          }
          if (!player.getMagic().hasRune(ItemId.FIRE_RUNE, 3)
              || !player.getMagic().hasRune(ItemId.BLOOD_RUNE, 3)
              || !player.getMagic().hasRune(ItemId.AIR_RUNE, 3)) {
            player.getMagic().notEnoughRunes();
            break;
          }
          if (player.getMagic().getChargeDelay() > 500) {
            player
                .getGameEncoder()
                .sendMessage("You can't recast that yet, your current Charge is too strong.");
            break;
          }
          player.getMagic().deleteRune(ItemId.FIRE_RUNE, 3);
          player.getMagic().deleteRune(ItemId.BLOOD_RUNE, 3);
          player.getMagic().deleteRune(ItemId.AIR_RUNE, 3);
          player.setAnimation(811);
          player.setGraphic(308, 124, 45);
          player.getMagic().setChargeDelay(600);
          player
              .getGameEncoder()
              .sendMessage("<col=ef1020>You feel charged with magic power.</col>");
          Diary.getDiaries(player)
              .forEach(d -> d.castSpell(player, spellbookChild, null, null, null));
          break;
        }
      case MAGIC_IMBUE:
        {
          if (player.getSkills().getLevel(Skills.MAGIC) < 82) {
            player
                .getGameEncoder()
                .sendMessage("You need a Magic level of 82 to cast Magic Imbue.");
            break;
          }
          if (!player.getMagic().hasRune(ItemId.FIRE_RUNE, 7)
              || !player.getMagic().hasRune(ItemId.WATER_RUNE, 7)
              || !player.getMagic().hasRune(ItemId.ASTRAL_RUNE, 2)) {
            player.getMagic().notEnoughRunes();
            break;
          }
          if (player.getMagic().getMagicImbueTime() > 0) {
            player
                .getGameEncoder()
                .sendMessage(
                    "You can cast Magic Imbue in "
                        + PTime.tickToSec(player.getMagic().getMagicImbueTime())
                        + " seconds.");
            break;
          }
          player.getGameEncoder().sendMessage("You are charged to combine runes!");
          player.getMagic().deleteRune(ItemId.FIRE_RUNE, 7);
          player.getMagic().deleteRune(ItemId.WATER_RUNE, 7);
          player.getMagic().deleteRune(ItemId.ASTRAL_RUNE, 2);
          player.getSkills().addXp(Skills.MAGIC, 86);
          player.getMagic().setMagicImbueTime(20);
          player.setGraphic(141, 100);
          player.setAnimation(722);
          Diary.getDiaries(player)
              .forEach(d -> d.castSpell(player, spellbookChild, null, null, null));
          break;
        }
      case VENGEANCE:
        {
          if (player.getSkills().getLevel(Skills.MAGIC) < 94
              || player.getController().getLevelForXP(Skills.DEFENCE) < 40) {
            player
                .getGameEncoder()
                .sendMessage(
                    "You need a Magic level of 94 and Defence level of 40 to cast Vengeance.");
            break;
          }
          var runes =
              Arrays.asList(
                  new Item(ItemId.EARTH_RUNE, 10),
                  new Item(ItemId.DEATH_RUNE, 2),
                  new Item(ItemId.ASTRAL_RUNE, 4));
          if (!player.getMagic().hasRunes(SpellbookChild.VENGEANCE, runes)) {
            player.getMagic().notEnoughRunes();
            break;
          }
          if (player.getMagic().getVengeanceDelay() > 0) {
            player
                .getGameEncoder()
                .sendMessage(
                    "You can cast Vengeance in "
                        + PTime.tickToSec(player.getMagic().getVengeanceDelay())
                        + " seconds.");
            break;
          }
          if (player.getMagic().getVengeanceCast()) {
            player.getGameEncoder().sendMessage("You already have Vengeance cast.");
            break;
          }
          player.getMagic().deleteRunes(SpellbookChild.VENGEANCE, runes);
          player.getSkills().addXp(Skills.MAGIC, 112);
          player.getMagic().setVengeanceCast(true);
          player.getMagic().setVengeanceDelay(Magic.VENGEANCE_DELAY);
          player.setGraphic(726, 100);
          player.setAnimation(8316);
          player.getController().sendMapSound(2907);
          Diary.getDiaries(player)
              .forEach(d -> d.castSpell(player, spellbookChild, null, null, null));
          break;
        }
      case GEOMANCY:
        {
          if (player.getSkills().getLevel(Skills.MAGIC) < 65) {
            player.getGameEncoder().sendMessage("You need a Magic level of 65 to cast this spell.");
            break;
          }
          if (!player.getMagic().hasRune(ItemId.ASTRAL_RUNE, 3)
              || !player.getMagic().hasRune(ItemId.NATURE_RUNE, 3)
              || !player.getMagic().hasRune(ItemId.EARTH_RUNE, 8)) {
            player.getMagic().notEnoughRunes();
            break;
          }
          if (player.getMagic().getGeomancyTime() > 0) {
            player
                .getGameEncoder()
                .sendMessage(
                    "You can cast Geomancy in "
                        + PTime.tickToSec(player.getMagic().getGeomancyTime())
                        + " seconds.");
            break;
          }
          player.getMagic().deleteRune(ItemId.ASTRAL_RUNE, 3);
          player.getMagic().deleteRune(ItemId.NATURE_RUNE, 3);
          player.getMagic().deleteRune(ItemId.EARTH_RUNE, 8);
          player.getSkills().addXp(Skills.MAGIC, 60);
          player.getMagic().setGeomancyTime(20);
          player.getFarming().openGeomancy();
          Diary.getDiaries(player)
              .forEach(d -> d.castSpell(player, spellbookChild, null, null, null));
          break;
        }
      case HUMIDIFY:
        {
          if (player.getSkills().getLevel(Skills.MAGIC) < 68) {
            player
                .getGameEncoder()
                .sendMessage("Your need a Magic level of 68 to cast this spell.");
            break;
          }
          if (!player.getMagic().hasRune(ItemId.FIRE_RUNE, 1)
              || !player.getMagic().hasRune(ItemId.ASTRAL_RUNE, 1)
              || !player.getMagic().hasRune(ItemId.WATER_RUNE, 3)) {
            player.getMagic().notEnoughRunes();
            break;
          }
          if (!player
              .getInventory()
              .hasItem(ItemId.BUCKET, ItemId.WATERING_CAN, ItemId.VIAL, ItemId.CLAY)) {
            player
                .getGameEncoder()
                .sendMessage("You have nothing in your inventory that this spell can humidify.");
            break;
          }
          if (player.getInventory().hasItem(ItemId.BUCKET)) {
            var bucketCount = player.getInventory().getCount(ItemId.BUCKET);
            for (int i = 0; i < bucketCount; i++) {
              player.getInventory().deleteItem(ItemId.BUCKET);
              player.getInventory().addItem(ItemId.BUCKET_OF_WATER);
            }
          }
          if (player.getInventory().hasItem(ItemId.WATERING_CAN)) {
            var wateringCanCount = player.getInventory().getCount(ItemId.WATERING_CAN);
            for (int i = 0; i < wateringCanCount; i++) {
              player.getInventory().deleteItem(ItemId.WATERING_CAN);
              player.getInventory().addItem(ItemId.WATERING_CAN_8);
            }
          }
          if (player.getInventory().hasItem(ItemId.VIAL)) {
            var vialCount = player.getInventory().getCount(ItemId.VIAL);
            for (int i = 0; i < vialCount; i++) {
              player.getInventory().deleteItem(ItemId.VIAL);
              player.getInventory().addItem(ItemId.VIAL_OF_WATER);
            }
          }
          if (player.getInventory().hasItem(ItemId.CLAY)) {
            var clayCount = player.getInventory().getCount(ItemId.CLAY);
            for (int i = 0; i < clayCount; i++) {
              player.getInventory().deleteItem(ItemId.CLAY);
              player.getInventory().addItem(ItemId.SOFT_CLAY);
            }
          }
          player.getMagic().deleteRune(ItemId.FIRE_RUNE, 1);
          player.getMagic().deleteRune(ItemId.ASTRAL_RUNE, 1);
          player.getMagic().deleteRune(ItemId.WATER_RUNE, 3);
          player.getSkills().addXp(Skills.MAGIC, 65);
          player.setGraphic(1061, 100);
          player.setAnimation(6294);
          break;
        }
      case TAN_LEATHER:
        {
          if (option == 0) {
            if (player.getSkills().getLevel(Skills.MAGIC) < 78) {
              player
                  .getGameEncoder()
                  .sendMessage("Your need a Magic level of 78 to cast this spell.");
              break;
            } else if (!player.getMagic().hasRune(ItemId.FIRE_RUNE, 5)
                || !player.getMagic().hasRune(ItemId.ASTRAL_RUNE, 2)
                || !player.getMagic().hasRune(ItemId.NATURE_RUNE, 1)) {
              player.getMagic().notEnoughRunes();
              break;
            } else if (player.getMagic().getTanLeatherTime() > 0) {
              break;
            } else if (!player
                .getInventory()
                .hasItem(
                    ItemId.COWHIDE,
                    ItemId.GREEN_DRAGONHIDE,
                    ItemId.GREEN_DRAGONHIDE,
                    ItemId.BLUE_DRAGONHIDE,
                    ItemId.RED_DRAGONHIDE,
                    ItemId.BLACK_DRAGONHIDE)) {
              player
                  .getGameEncoder()
                  .sendMessage("You have nothing in your inventory that this spell can tan.");
              break;
            }
            for (int i = 0; i < 5; i++) {
              if (player.getInventory().hasItem(ItemId.COWHIDE)) {
                if (player.getPlugin(MagicPlugin.class).isLeatherStatus()) {
                  player.getInventory().deleteItem(ItemId.COWHIDE);
                  player.getInventory().addItem(ItemId.LEATHER);
                } else {
                  player.getInventory().deleteItem(ItemId.COWHIDE);
                  player.getInventory().addItem(ItemId.HARD_LEATHER);
                }
              } else if (player.getInventory().hasItem(ItemId.GREEN_DRAGONHIDE)) {
                player.getInventory().deleteItem(ItemId.GREEN_DRAGONHIDE);
                player.getInventory().addItem(ItemId.GREEN_DRAGON_LEATHER);
              } else if (player.getInventory().hasItem(ItemId.BLUE_DRAGONHIDE)) {
                player.getInventory().deleteItem(ItemId.BLUE_DRAGONHIDE);
                player.getInventory().addItem(ItemId.BLUE_DRAGON_LEATHER);
              } else if (player.getInventory().hasItem(ItemId.RED_DRAGONHIDE)) {
                player.getInventory().deleteItem(ItemId.RED_DRAGONHIDE);
                player.getInventory().addItem(ItemId.RED_DRAGON_LEATHER);
              } else if (player.getInventory().hasItem(ItemId.BLACK_DRAGONHIDE)) {
                player.getInventory().deleteItem(ItemId.BLACK_DRAGONHIDE);
                player.getInventory().addItem(ItemId.BLACK_DRAGON_LEATHER);
              }
            }
            player.getMagic().deleteRune(ItemId.FIRE_RUNE, 5);
            player.getMagic().deleteRune(ItemId.ASTRAL_RUNE, 2);
            player.getMagic().deleteRune(ItemId.NATURE_RUNE, 1);
            player.getSkills().addXp(Skills.MAGIC, 81);
            player.getMagic().setTanLeatherTime(3);
            player.setGraphic(322, 100);
            player.setAnimation(712);
          } else if (option == 1) {
            player.getPlugin(MagicPlugin.class).tanLeatherSetupDialogue();
          }
          break;
        }
      case BONES_TO_PEACHES:
        {
          if (player.getSkills().getLevel(Skills.MAGIC) < 60) {
            player.getGameEncoder().sendMessage("You need a Magic level of 60 to cast this spell.");
            break;
          }
          if (!player.getMagic().hasRune(ItemId.EARTH_RUNE, 4)
              || !player.getMagic().hasRune(ItemId.WATER_RUNE, 4)
              || !player.getMagic().hasRune(ItemId.NATURE_RUNE, 2)) {
            player.getMagic().notEnoughRunes();
            break;
          }
          var bonesCount = player.getInventory().getCount(ItemId.BONES);
          var bigBonesCount = player.getInventory().getCount(ItemId.BIG_BONES);
          if (bonesCount == 0 && bigBonesCount == 0) {
            player.getGameEncoder().sendMessage("You aren't holding any bones!");
            break;
          }
          player.getMagic().deleteRune(ItemId.EARTH_RUNE, 4);
          player.getMagic().deleteRune(ItemId.WATER_RUNE, 4);
          player.getMagic().deleteRune(ItemId.NATURE_RUNE, 2);
          player.getSkills().addXp(Skills.MAGIC, 36);
          player.getInventory().deleteItem(ItemId.BONES, bonesCount);
          player.getInventory().deleteItem(ItemId.BIG_BONES, bigBonesCount);
          player.getInventory().addItem(ItemId.PEACH, bonesCount + bigBonesCount);
          player.setGraphic(141, 100);
          player.setAnimation(722);
          Diary.getDiaries(player)
              .forEach(d -> d.castSpell(player, spellbookChild, null, null, null));
          break;
        }
      default:
        break;
    }
  }

  @Override
  public boolean widgetOnWidget(
      Player player,
      int useWidgetId,
      int useChildId,
      int onWidgetId,
      int onChildId,
      int useSlot,
      int useItemId,
      int onSlot,
      int onItemId) {
    if (onWidgetId == WidgetId.INVENTORY) {
      var spellbookChild = SpellbookChild.get(useChildId);
      if (spellbookChild == null) {
        return true;
      }
      var spellOnItem = SpellOnItem.get(spellbookChild);
      if (spellOnItem != null) {
        return spellOnItem.onItem(player, onSlot, onItemId);
      }
      return true;
    }
    return false;
  }

  @Override
  public void widgetOnMapObject(
      Player player, int widgetId, int childId, int slot, MapObject mapObject) {
    var spellbookChild = SpellbookChild.get(childId);
    if (spellbookChild == null) {
      return;
    }
    var spellOnMapObject = SpellOnMapObject.get(spellbookChild);
    if (spellOnMapObject != null) {
      spellOnMapObject.onMapObject(player, mapObject);
    }
  }
}
