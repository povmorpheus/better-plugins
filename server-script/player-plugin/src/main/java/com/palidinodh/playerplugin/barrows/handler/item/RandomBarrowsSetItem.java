package com.palidinodh.playerplugin.barrows.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;

@ReferenceId(ItemId.RANDOM_BARROWS_SET_60067)
public class RandomBarrowsSetItem implements ItemHandler {

  private static final List<Integer> BARROWS_SETS =
      Arrays.asList(
          ItemId.GUTHANS_ARMOUR_SET,
          ItemId.VERACS_ARMOUR_SET,
          ItemId.DHAROKS_ARMOUR_SET,
          ItemId.TORAGS_ARMOUR_SET,
          ItemId.AHRIMS_ARMOUR_SET,
          ItemId.KARILS_ARMOUR_SET);

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.listRandom(BARROWS_SETS)));
  }
}
