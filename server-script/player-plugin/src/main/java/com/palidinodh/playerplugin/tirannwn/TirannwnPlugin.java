package com.palidinodh.playerplugin.tirannwn;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import lombok.Getter;

public class TirannwnPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Getter private TheGauntletSubPlugin theGauntlet = new TheGauntletSubPlugin();

  public void crystalWeaponSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Crystal Halberd",
            (c, s) -> {
              transformCrystalWeaponSeed(ItemId.CRYSTAL_HALBERD);
            }),
        new DialogueOption(
            "Crystal Bow",
            (c, s) -> {
              transformCrystalWeaponSeed(ItemId.CRYSTAL_BOW);
            }),
        new DialogueOption(
            "Crystal Shield",
            (c, s) -> {
              transformCrystalWeaponSeed(ItemId.CRYSTAL_SHIELD);
            }));
  }

  public void enhancedCrystalWeaponSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Blade of Saeldor",
            (c, s) -> {
              transformEnhancedCrystalWeaponSeed(ItemId.BLADE_OF_SAELDOR_INACTIVE);
            }),
        new DialogueOption(
            "Bow of Faerdhinen",
            (c, s) -> {
              transformEnhancedCrystalWeaponSeed(ItemId.BOW_OF_FAERDHINEN_INACTIVE);
            }));
  }

  public void crystalArmourSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Crystal Helm",
            (c, s) -> {
              transformCrystalArmourSeed(ItemId.CRYSTAL_HELM, 1);
            }),
        new DialogueOption(
            "Crystal Body",
            (c, s) -> {
              transformCrystalArmourSeed(ItemId.CRYSTAL_BODY, 3);
            }),
        new DialogueOption(
            "Crystal Legs",
            (c, s) -> {
              transformCrystalArmourSeed(ItemId.CRYSTAL_LEGS, 2);
            }));
  }

  private void transformCrystalWeaponSeed(int itemId) {
    if (!player.getInventory().hasItem(ItemId.CRYSTAL_WEAPON_SEED)) {
      player.getGameEncoder().sendMessage("You need a crystal weapon seed to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < 150_000) {
      player.getGameEncoder().sendMessage("You need 15,000 coins to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.CRYSTAL_WEAPON_SEED);
    player.getInventory().deleteItem(ItemId.COINS, 150_000);
    player.getInventory().addItem(itemId);
  }

  private void transformEnhancedCrystalWeaponSeed(int itemId) {
    if (!player.getInventory().hasItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED)) {
      player.getGameEncoder().sendMessage("You need an enhanced crystal weapon seed to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 82) {
      player.getGameEncoder().sendMessage("You need 82 Smithing to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 82) {
      player.getGameEncoder().sendMessage("You need 82 Crafting to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 100) {
      player.getGameEncoder().sendMessage("You need 100 crystal shards to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
    player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 100);
    player.getInventory().addItem(itemId);
    player.getSkills().addXp(Skills.SMITHING, 5_000);
    player.getSkills().addXp(Skills.CRAFTING, 5_000);
  }

  private void transformCrystalArmourSeed(int itemId, int seedCount) {
    if (player.getInventory().getCount(ItemId.CRYSTAL_ARMOUR_SEED) < seedCount) {
      player
          .getGameEncoder()
          .sendMessage("You need " + seedCount + " crystal armour seeds to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 74) {
      player.getGameEncoder().sendMessage("You need 74 Smithing to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 74) {
      player.getGameEncoder().sendMessage("You need 74 Crafting to do this.");
      return;
    }
    var shardCount = 50 * seedCount;
    if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < shardCount) {
      player.getGameEncoder().sendMessage("You need " + shardCount + " crystal shards to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.CRYSTAL_ARMOUR_SEED, seedCount);
    player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, shardCount);
    player.getInventory().addItem(itemId);
    player.getSkills().addXp(Skills.SMITHING, 2_500 * seedCount);
    player.getSkills().addXp(Skills.CRAFTING, 2_500 * seedCount);
  }
}
