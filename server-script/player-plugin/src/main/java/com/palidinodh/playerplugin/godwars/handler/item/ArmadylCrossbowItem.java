package com.palidinodh.playerplugin.godwars.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ARMADYL_CROSSBOW)
class ArmadylCrossbowItem implements ItemHandler {

  private static boolean itemOnNihil(Player player, Item useItem, Item onItem) {
    var crossbowItem = ItemHandler.matchItem(useItem, onItem, ItemId.ARMADYL_CROSSBOW);
    if (player.getInventory().getCount(ItemId.NIHIL_HORN) < 1) {
      player.getGameEncoder().sendMessage("You need a nihil horn to do this.");
      return true;
    }
    if (player.getInventory().getCount(ItemId.NIHIL_SHARD) < 250) {
      player.getGameEncoder().sendMessage("You need 250 nihil shards to do this.");
      return true;
    }
    player.getInventory().deleteItem(ItemId.NIHIL_HORN);
    player.getInventory().deleteItem(ItemId.NIHIL_SHARD, 250);
    crossbowItem.replace(new Item(ItemId.ZARYTE_CROSSBOW));
    return true;
  }

  private static boolean itemOnJadKit(Player player, Item useItem, Item onItem) {
    onItem.replace(new Item(ItemId.ARMADYL_CROSSBOW_OR_60049));
    useItem.remove();
    return true;
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.NIHIL_HORN)) {
      return itemOnNihil(player, useItem, onItem);
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_CROSSBOW, ItemId.ACB_ORNAMENT_KIT_60060)) {
      return itemOnJadKit(player, useItem, onItem);
    }
    return false;
  }
}
