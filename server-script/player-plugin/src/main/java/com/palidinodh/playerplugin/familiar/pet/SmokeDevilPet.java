package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class SmokeDevilPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_SMOKE_DEVIL, NpcId.SMOKE_DEVIL_6655, NpcId.SMOKE_DEVIL));
    return builder;
  }
}
