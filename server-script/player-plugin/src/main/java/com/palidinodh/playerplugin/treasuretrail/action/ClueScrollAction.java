package com.palidinodh.playerplugin.treasuretrail.action;

import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueChain;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class ClueScrollAction {

  private DialogueChain dialogueChain;
  private OptionsDialogue optionsDialogue;
  private int answerOptionSlot;

  public ClueScrollAction(DialogueChain dialogueChain) {
    this.dialogueChain = dialogueChain;
    for (var dialogue : dialogueChain.getDialogues()) {
      if (!(dialogue instanceof OptionsDialogue)) {
        continue;
      }
      optionsDialogue = (OptionsDialogue) dialogue;
      for (var i = 0; i < optionsDialogue.getOptions().size(); i++) {
        var option = optionsDialogue.getOptions().get(i);
        if (!option.getText().contains("<answer>")) {
          continue;
        }
        answerOptionSlot = i;
        optionsDialogue
            .getOptions()
            .set(
                i,
                new DialogueOption(option.getText().replace("<answer>", ""), option.getAction()));
        break;
      }
      break;
    }
  }

  public final boolean dialogueHook(
      Player player, ClueScrollType type, Dialogue dialogue, int slot) {
    if (dialogue != optionsDialogue) {
      return false;
    }
    if (slot != answerOptionSlot) {
      return false;
    }
    player.getPlugin(TreasureTrailPlugin.class).stageComplete(type);
    return true;
  }

  public boolean npcOptionHook(Player player, ClueScrollType type, Npc npc) {
    return false;
  }

  public boolean mapObjectOptionHook(Player player, ClueScrollType type, MapObject mapObject) {
    return false;
  }

  public boolean animationHook(Player player, ClueScrollType type, int animId) {
    return false;
  }

  public boolean digHook(Player player, ClueScrollType type) {
    return false;
  }
}
