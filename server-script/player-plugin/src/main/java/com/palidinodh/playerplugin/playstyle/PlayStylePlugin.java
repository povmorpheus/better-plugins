package com.palidinodh.playerplugin.playstyle;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.util.PString;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
public class PlayStylePlugin implements PlayerPlugin {

  private static final int GROUP_SIZE_LIMIT = 5;

  @Inject private transient Player player;

  private RsGameMode gameMode = RsGameMode.UNSET;
  @Setter private RsDifficultyMode difficultyMode = RsDifficultyMode.UNSET;
  private Map<Integer, String> group;

  @Override
  public void loadLegacy(Map<String, Object> map) {
    if (map.containsKey("player.gameMode")) {
      setGameMode((RsGameMode) map.get("player.gameMode"));
    }
    if (map.containsKey("player.difficultyMode")) {
      difficultyMode = (RsDifficultyMode) map.get("player.difficultyMode");
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("play_style_game_mode")) {
      return gameMode;
    }
    if (name.equals("play_style_set_game_mode")) {
      gameMode = (RsGameMode) args[0];
    }
    if (name.equals("play_style_difficulty_mode")) {
      return difficultyMode;
    }
    if (name.equals("play_style_group_mode")) {
      return isGroupMode() ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("play_style_is_group_interactable")) {
      return isGroupInteractable((Player) args[0]) ? Boolean.TRUE : Boolean.FALSE;
    }
    return null;
  }

  @Override
  public void login() {
    setGameMode(gameMode);
    refreshGroup();
  }

  public void setGameMode(RsGameMode gameMode) {
    this.gameMode = gameMode;
    switch (gameMode) {
      case IRONMAN:
        player.getGameEncoder().setVarp(499, 8);
        break;
      case HARDCORE_IRONMAN:
        player.getGameEncoder().setVarp(499, 24);
        break;
      default:
        player.getGameEncoder().setVarp(499, 0);
        break;
    }
  }

  public void enableGroup() {
    if (group == null) {
      group = new HashMap<>();
    }
  }

  public void disableGroup() {
    group = null;
  }

  public boolean isGroupMode() {
    return group != null;
  }

  public boolean isGroupInteractable(Player other) {
    return inGroup(other.getId());
  }

  public boolean inGroup(int userId) {
    return group != null && group.containsKey(userId);
  }

  public void groupOptionsDialogue() {
    if (group == null) {
      return;
    }
    refreshGroup();
    var lines = new ArrayList<String>();
    group.forEach((key, value) -> lines.add(value));
    var cs2 = ScreenSelectionCs2.builder();
    cs2.title("Group Management");
    cs2.entryLines(6);
    player.openDialogue(
        new LargeOptions2Dialogue(
            cs2,
            new DialogueOption("Group Usernames:<br>" + PString.toString(lines, "<br>")),
            new DialogueOption(
                "Invite Clan Chat: Invite users in the Clan Chat to your group.",
                (c, s) -> {
                  inviteGroup();
                }),
            new DialogueOption(
                "Purge to Clan Chat: Remove group users not in your Clan Chat.<br>"
                    + PString.toString(getPurgeToUsernames(), "<br>"),
                (c, s) -> {
                  player.openOptionsDialogue(
                      new DialogueOption(
                          "Purge - Can't be undone!",
                          (c1, s1) -> {
                            purgeGroup();
                          }),
                      new DialogueOption("Nevermind."));
                })));
  }

  private void refreshGroup() {
    if (player.getWorld() == null) {
      return;
    }
    if (group == null) {
      return;
    }
    var removals = new ArrayList<Integer>();
    for (var entry : group.entrySet()) {
      var other = player.getWorld().getPlayerById(entry.getKey());
      if (other == null) {
        continue;
      }
      var otherStyle = other.getPlugin(PlayStylePlugin.class);
      if (!otherStyle.inGroup(player.getId())) {
        removals.add(other.getId());
        continue;
      }
      entry.setValue(other.getUsername().toLowerCase());
      otherStyle.getGroup().replace(player.getId(), player.getUsername().toLowerCase());
    }
    if (!removals.isEmpty()) {
      removals.forEach(r -> group.remove(r));
    }
  }

  private void inviteGroup() {
    if (group == null) {
      player.getGameEncoder().sendMessage("You didn't enable group mode during account creation.");
      return;
    }
    if (player.getMessaging().getClanChatUserId() == -1
        || Clan.isGlobal(player.getMessaging().getClanChatUsername())) {
      player.getGameEncoder().sendMessage("You must be in a personal Clan Chat to invite.");
      return;
    }
    if (!player.getMessaging().isClanChatOwner()) {
      player.getGameEncoder().sendMessage("You must be the owner of the Clan Chat to invite.");
      return;
    }
    var groupPlayers = new ArrayList<Player>();
    for (var userId : group.keySet()) {
      var groupPlayer = player.getWorld().getPlayerById(userId);
      if (groupPlayer == null) {
        continue;
      }
      if (player.getMessaging().getClanChatUserId()
          != groupPlayer.getMessaging().getClanChatUserId()) {
        continue;
      }
      groupPlayers.add(groupPlayer);
    }
    if (groupPlayers.size() != group.size()) {
      player
          .getGameEncoder()
          .sendMessage("Everyone in the group must be in the same world and Clan Chat to invite.");
      return;
    }
    var clanChatPlayers = new ArrayList<Player>();
    for (var user : player.getMessaging().getClanChatUsers()) {
      var ccPlayer = player.getWorld().getPlayerByUsername(user.getUsername());
      if (player == ccPlayer) {
        continue;
      }
      var ccStyle = ccPlayer.getPlugin(PlayStylePlugin.class);
      ccStyle.refreshGroup();
      if (inGroup(ccPlayer.getId()) != ccStyle.inGroup(player.getId())) {
        player
            .getGameEncoder()
            .sendMessage(
                "Your currently listed group conflicts with "
                    + ccPlayer.getUsername()
                    + "'s currently listed group; invitiations couldn't be sent.");
        return;
      }
      if (ccPlayer == null) {
        player
            .getGameEncoder()
            .sendMessage(ccPlayer.getUsername() + " is offline or in a different world.");
        continue;
      }
      if (groupPlayers.contains(ccPlayer)) {
        continue;
      }
      if (ccStyle.getGroup() == null) {
        player
            .getGameEncoder()
            .sendMessage(
                ccPlayer.getUsername() + " didn't enable group mode during account creation.");
        continue;
      }
      if (!ccStyle.getGroup().isEmpty()) {
        player
            .getGameEncoder()
            .sendMessage(ccPlayer.getUsername() + " has previously joined a group.");
        continue;
      }
      clanChatPlayers.add(ccPlayer);
    }
    if (groupPlayers.size() + clanChatPlayers.size() > GROUP_SIZE_LIMIT) {
      player
          .getGameEncoder()
          .sendMessage("The new group would exceed the size limit of " + GROUP_SIZE_LIMIT + ".");
      return;
    }
    clanChatPlayers.forEach(
        p -> {
          p.openDialogue(
              new OptionsDialogue(
                  "Group Invite: " + player.getUsername(),
                  new DialogueOption(
                      "Accept group invitation.",
                      (c, s) -> {
                        var pStyle = p.getPlugin(PlayStylePlugin.class);
                        pStyle.joinGroup(player);
                      }),
                  new DialogueOption("Decline group invitation.")));
        });
    player.getGameEncoder().sendMessage("Group invitations have been sent.");
  }

  private void joinGroup(Player other) {
    if (group == null) {
      player.getGameEncoder().sendMessage("You didn't enable group mode during account creation.");
      other.getGameEncoder().sendMessage("They didn't enable group mode during account creation.");
      return;
    }
    var otherStyle = other.getPlugin(PlayStylePlugin.class);
    if (otherStyle.getGroup() == null) {
      player.getGameEncoder().sendMessage("They didn't enable group mode during account creation.");
      other.getGameEncoder().sendMessage("You didn't enable group mode during account creation.");
      return;
    }
    if (!group.isEmpty()) {
      player.getGameEncoder().sendMessage("You have previously joined a group.");
      other.getGameEncoder().sendMessage("They have previously joined a group.");
      return;
    }
    if (otherStyle.getGroup().size() >= GROUP_SIZE_LIMIT) {
      player.getGameEncoder().sendMessage("Their group is already at the size limit.");
      other.getGameEncoder().sendMessage("Your group is already at the size limit.");
      return;
    }
    if (other.getMessaging().getClanChatUserId() == -1
        || Clan.isGlobal(other.getMessaging().getClanChatUsername())) {
      player.getGameEncoder().sendMessage("You must be in a personal Clan Chat to join.");
      other.getGameEncoder().sendMessage("You must be in a personal Clan Chat to invite.");
      return;
    }
    if (!other.getMessaging().isClanChatOwner()) {
      player.getGameEncoder().sendMessage("They must be the owner of the Clan Chat to join.");
      other.getGameEncoder().sendMessage("You must be the owner of the Clan Chat to invite.");
      return;
    }
    var players = new ArrayList<Player>();
    players.add(other);
    for (var userId : otherStyle.getGroup().keySet()) {
      if (other.getId() == userId) {
        continue;
      }
      var groupPlayer = player.getWorld().getPlayerById(userId);
      if (groupPlayer == null) {
        continue;
      }
      if (other.getMessaging().getClanChatUserId()
          != groupPlayer.getMessaging().getClanChatUserId()) {
        continue;
      }
      var groupStyle = groupPlayer.getPlugin(PlayStylePlugin.class);
      groupStyle.refreshGroup();
      if (inGroup(groupPlayer.getId()) != groupStyle.inGroup(player.getId())) {
        player
            .getGameEncoder()
            .sendMessage(
                "Your currently listed group conflicts with "
                    + groupPlayer.getUsername()
                    + "'s currently listed group; join couldn't be completed.");
        return;
      }
      players.add(groupPlayer);
    }
    if (players.size() != Math.max(otherStyle.getGroup().size(), 1)) {
      player
          .getGameEncoder()
          .sendMessage("Everyone in the group must be in the same world and Clan Chat to join.");
      other
          .getGameEncoder()
          .sendMessage("Everyone in the group must be in the same world and Clan Chat to invite.");
      return;
    }
    group.clear();
    group.put(player.getId(), player.getUsername().toLowerCase());
    players.forEach(p -> group.put(p.getId(), p.getUsername().toLowerCase()));
    players.forEach(
        p -> {
          p.getGameEncoder().sendMessage(player.getUsername() + " has joined the group!");
          var pStyle = p.getPlugin(PlayStylePlugin.class);
          if (player == p) {
            return;
          }
          pStyle.getGroup().clear();
          pStyle.getGroup().putAll(group);
        });
    player.getGameEncoder().sendMessage("You have joined the group!");
  }

  private void purgeGroup() {
    if (group == null) {
      player.getGameEncoder().sendMessage("You didn't enable group mode during account creation.");
      return;
    }
    if (player.getMessaging().getClanChatUserId() == -1
        || Clan.isGlobal(player.getMessaging().getClanChatUsername())) {
      player.getGameEncoder().sendMessage("You must be in a personal Clan Chat to purge.");
      return;
    }
    if (!player.getMessaging().isClanChatOwner()) {
      player.getGameEncoder().sendMessage("You must be the owner of the Clan Chat to purge.");
      return;
    }
    if (group.isEmpty()) {
      player.getGameEncoder().sendMessage("You haven't yet joined or created a group.");
      return;
    }
    var players = new ArrayList<Player>();
    for (var userId : group.keySet()) {
      var groupPlayer = player.getWorld().getPlayerById(userId);
      if (groupPlayer == null) {
        continue;
      }
      if (player.getMessaging().getClanChatUserId()
          != groupPlayer.getMessaging().getClanChatUserId()) {
        continue;
      }
      var groupStyle = groupPlayer.getPlugin(PlayStylePlugin.class);
      groupStyle.refreshGroup();
      if (inGroup(groupPlayer.getId()) != groupStyle.inGroup(player.getId())) {
        player
            .getGameEncoder()
            .sendMessage(
                "Your currently listed group conflicts with "
                    + groupPlayer.getUsername()
                    + "'s currently listed group; purge couldn't be completed.");
        return;
      }
      players.add(groupPlayer);
    }
    var ccGroupCount = 0;
    for (var u : player.getMessaging().getClanChatUsers()) {
      var ccPlayer = player.getWorld().getPlayerByUsername(u.getUsername());
      if (ccPlayer == null) {
        continue;
      }
      if (!inGroup(ccPlayer.getId())) {
        continue;
      }
      ccGroupCount++;
    }
    if (players.size() != ccGroupCount) {
      player
          .getGameEncoder()
          .sendMessage("Everyone remaining in the group must be in the same world to purge.");
      return;
    }
    if (players.size() > player.getMessaging().getClanChatUsers().size()) {
      player.getGameEncoder().sendMessage("Everyone in the Clan Chat must be part of your group.");
      return;
    }
    group.clear();
    group.put(player.getId(), player.getUsername().toLowerCase());
    players.forEach(p -> group.put(p.getId(), p.getUsername().toLowerCase()));
    players.forEach(
        p -> {
          p.getGameEncoder().sendMessage(player.getUsername() + " has purged the group!");
          if (player == p) {
            return;
          }
          var pStyle = p.getPlugin(PlayStylePlugin.class);
          pStyle.getGroup().clear();
          pStyle.getGroup().putAll(group);
        });
  }

  private List<String> getPurgeToUsernames() {
    var list = new ArrayList<String>();
    for (var userId : group.keySet()) {
      var groupPlayer = player.getWorld().getPlayerById(userId);
      if (groupPlayer == null) {
        continue;
      }
      if (player.getMessaging().getClanChatUserId()
          != groupPlayer.getMessaging().getClanChatUserId()) {
        continue;
      }
      list.add(groupPlayer.getUsername());
    }
    return list;
  }
}
