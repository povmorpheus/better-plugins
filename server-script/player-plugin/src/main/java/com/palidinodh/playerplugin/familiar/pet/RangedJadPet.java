package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class RangedJadPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.TINY_RANGED_JAD_60045,
            NpcId.TINY_RANGED_JAD_16072,
            NpcId.TINY_RANGED_JAD_16071));
    return builder;
  }
}
