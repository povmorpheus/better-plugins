package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.random.PRandom;

class ChambersOfXericPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.OLMLET, NpcId.OLMLET, NpcId.OLMLET_7520));
    builder.entry(new Pet.Entry(ItemId.PUPPADILE, NpcId.PUPPADILE, NpcId.PUPPADILE_8201));
    builder.entry(new Pet.Entry(ItemId.TEKTINY, NpcId.TEKTINY, NpcId.TEKTINY_8202));
    builder.entry(new Pet.Entry(ItemId.VANGUARD, NpcId.VANGUARD_8198, NpcId.VANGUARD_8203));
    builder.entry(new Pet.Entry(ItemId.VASA_MINIRIO, NpcId.VASA_MINIRIO, NpcId.VASA_MINIRIO_8204));
    builder.entry(new Pet.Entry(ItemId.VESPINA, NpcId.VESPINA, NpcId.VESPINA_8205));
    builder.optionVariation(
        (p, n) -> {
          if (!p.getCombat().getMetamorphicDust()) {
            p.getGameEncoder()
                .sendMessage("You haven't unlocked the ability to metamorphose your Olmlet.");
            return;
          }
          var transformable =
              new int[] {
                NpcId.OLMLET,
                NpcId.TEKTINY,
                NpcId.VESPINA,
                NpcId.VANGUARD,
                NpcId.PUPPADILE,
                NpcId.VASA_MINIRIO
              };
          p.getPlugin(FamiliarPlugin.class).transformPet(PRandom.arrayRandom(transformable));
        });
    return builder;
  }
}
