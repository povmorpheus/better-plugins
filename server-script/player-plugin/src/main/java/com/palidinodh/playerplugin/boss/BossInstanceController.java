package com.palidinodh.playerplugin.boss;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class BossInstanceController extends PController {

  @Inject private Player player;
  @Getter private int npcId;
  @Getter @Setter private boolean respawnBoss;
  @Getter private long startTime = PTime.nanoTime();
  @Getter private List<Npc> bossNpcs = new ArrayList<>();
  @Setter private boolean keepExitTileOnDeath;
  @Setter private boolean removeItemsOnStop;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("boss_instance_controller")) {
      return Boolean.TRUE;
    }
    if (name.equals("boss_instance_controller_name")) {
      return NpcDefinition.getName(npcId);
    }
    if (name.equals("boss_instance_controller_time")) {
      return Math.max(0, 60 - PTime.betweenNanoToMin(startTime));
    }
    if (name.equals("boss_instance_controller_check_expiration")) {
      return checkExpiration();
    }
    return null;
  }

  @Override
  public void startHook() {
    player.setLargeVisibility();
    player.getCombat().setDamageInflicted(0);
    setExitTile(new Tile(player));
  }

  @Override
  public void stopHook() {
    if (player == null) {
      return;
    }
    removeSpecialItems();
    if (removeItemsOnStop) {
      player.getInventory().clear();
      player.getEquipment().clear();
      player.getEquipment().weaponUpdate(true);
    }
    player.setDefaultVisibility();
    player.getWidgetManager().removeOverlay();
    player.getWidgetManager().removeFullOverlay();
    var plugin = player.getPlugin(BossPlugin.class);
    plugin.restore();
    plugin.getItems().clear();
    plugin.getMapObjects().clear();
    if (!respawnBoss) {
      removeNpcs(bossNpcs);
      setEntity(null);
      player = null;
    }
  }

  @Override
  public void instanceChangedHook(Controller controller) {
    if (isInstanced() && player != null) {
      player.setLargeVisibility();
    }
    if (controller == null || controller == this) {
      return;
    }
    if (!controller.is(BossInstanceController.class)) {
      return;
    }
    var bc = controller.as(BossInstanceController.class);
    respawnBoss = bc.isRespawnBoss();
    startTime = bc.getStartTime();
    npcId = bc.getNpcId();
    bossNpcs = bc.getBossNpcs();
  }

  @Override
  public void tickHook() {
    checkExpiration();
    if (player != null && player.isLocked() && !player.isVisible()) {
      setEntity(null);
      player = null;
    }
  }

  @Override
  public void applyDeadHook() {
    if (!keepExitTileOnDeath) {
      setExitTile(null);
    }
    stopWithTeleport();
  }

  @Override
  public void applyDeadCompleteStartHook() {
    if (removeItemsOnStop) {
      player.getInventory().clear();
      player.getEquipment().clear();
    }
    removeSpecialItems();
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem) {
    if (!mapItem.getDef().getUntradable()
        && (isFood(mapItem.getId()) || isDrink(mapItem.getId()))) {
      mapItem.setAlwaysAppear();
    }
    if (mapItem.getId() == ItemId.DAWNBRINGER) {
      mapItem.setAlways();
      mapItem.clearOwner();
    }
    return mapItem;
  }

  public void setBossInstance(int bossId, boolean respawn, List<NpcSpawn> spawns) {
    npcId = bossId;
    respawnBoss = respawn;
    if (spawns != null) {
      for (var spawn : spawns) {
        var npc = addNpc(spawn);
        npc.setLargeVisibility();
        bossNpcs.add(npc);
      }
    }
  }

  public void expire() {
    startTime = 0;
  }

  private boolean checkExpiration() {
    if (!respawnBoss && getPlayers().isEmpty()) {
      return true;
    }
    boolean isExpired = startTime == 0 || PTime.betweenNanoToMin(startTime) > 60;
    if (isExpired && bossNpcs != null) {
      if (respawnBoss) {
        for (Player player2 : getPlayers()) {
          player2.getGameEncoder().sendMessage("<col=ff0000>The instance has expired!");
        }
      } else if (player != null) {
        player.getGameEncoder().sendMessage("<col=ff0000>The instance has expired!");
      }
      for (Npc npc : bossNpcs) {
        if (npc == null) {
          continue;
        }
        npc.getSpawn().respawnable(false);
      }
      bossNpcs = null;
    }
    return isExpired;
  }

  private void removeSpecialItems() {
    if (player.carryingItem(ItemId.DAWNBRINGER)) {
      addMapItem(new Item(ItemId.DAWNBRINGER), player, player);
      player.getInventory().deleteAll(ItemId.DAWNBRINGER);
      player.getEquipment().deleteAll(ItemId.DAWNBRINGER);
    }
  }
}
