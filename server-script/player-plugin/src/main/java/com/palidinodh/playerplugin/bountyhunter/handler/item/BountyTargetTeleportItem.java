package com.palidinodh.playerplugin.bountyhunter.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BOUNTY_TARGET_TELEPORT)
class BountyTargetTeleportItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getPlugin(BountyHunterPlugin.class).canTeleportToTarget(true)) {
      player.getPlugin(BountyHunterPlugin.class).selectTeleportToTarget(true);
    }
  }
}
