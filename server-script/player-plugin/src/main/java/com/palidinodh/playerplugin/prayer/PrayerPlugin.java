package com.palidinodh.playerplugin.prayer;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.playerplugin.bond.BondPlugin;

public class PrayerPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Override
  public void npcKilled(Npc npc) {
    if (npc.getCombatDef().isTypeSpectral()) {
      var ectoplasmator = player.getInventory().hasItem(ItemId.ECTOPLASMATOR);
      if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.DEVOTION_ECTOPLASMATOR)
          && player.hasItem(ItemId.ECTOPLASMATOR)) {
        ectoplasmator = true;
      }
      if (ectoplasmator) {
        player.getSkills().addXp(Skills.PRAYER, (int) (npc.getCombat().getMaxHitpoints() * 0.2));
      }
    }
  }
}
