package com.palidinodh.playerplugin.godwars.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.FROZEN_KEY_PIECE_ARMADYL,
  ItemId.FROZEN_KEY_PIECE_BANDOS,
  ItemId.FROZEN_KEY_PIECE_ZAMORAK,
  ItemId.FROZEN_KEY_PIECE_SARADOMIN
})
class FrozenKeyPieceItem implements ItemHandler {

  private static final int[] PIECES = {
    ItemId.FROZEN_KEY_PIECE_ARMADYL,
    ItemId.FROZEN_KEY_PIECE_BANDOS,
    ItemId.FROZEN_KEY_PIECE_ZAMORAK,
    ItemId.FROZEN_KEY_PIECE_SARADOMIN
  };

  private static void assemble(Player player) {
    if (!player.getInventory().hasItems(PIECES)) {
      player.getGameEncoder().sendMessage("You need all the key pieces to do this.");
      return;
    }
    player.getInventory().deleteItems(PIECES);
    player.getInventory().addItem(ItemId.FROZEN_KEY_26356);
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    assemble(player);
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    assemble(player);
    return true;
  }
}
