package com.palidinodh.playerplugin.fossilisland;

import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PString;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MyceliumLocationType {
  HOUSE_ON_THE_HILL(new Tile(3764, 3880, 1), new Tile(3764, 3789, 1)),
  VERDANT_VALLEY(new Tile(3758, 3756), new Tile(3757, 3756)),
  STICKY_SWAMP(new Tile(3677, 3755), new Tile(3676, 3755)),
  MUSHROOM_MEADOW(new Tile(3674, 3871), new Tile(3676, 3871));

  private final Tile mapObjectTile;
  private final Tile teleportTile;

  public static MyceliumLocationType get(int index) {
    return index >= 0 && index < values().length ? values()[index] : null;
  }

  public static MyceliumLocationType getByObjectTile(Tile tile) {
    for (var type : values()) {
      if (!type.getMapObjectTile().matchesTile(tile)) {
        continue;
      }
      return type;
    }
    return null;
  }

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace("_", " "));
  }
}
