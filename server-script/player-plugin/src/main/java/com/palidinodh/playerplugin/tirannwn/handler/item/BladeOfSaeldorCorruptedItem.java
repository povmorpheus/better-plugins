package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BLADE_OF_SAELDOR_C,
  ItemId.BLADE_OF_SAELDOR_C_25870,
  ItemId.BLADE_OF_SAELDOR_C_25872,
  ItemId.BLADE_OF_SAELDOR_C_25874,
  ItemId.BLADE_OF_SAELDOR_C_25876,
  ItemId.BLADE_OF_SAELDOR_C_25878,
  ItemId.BLADE_OF_SAELDOR_C_25880,
  ItemId.BLADE_OF_SAELDOR_C_25882
})
class BladeOfSaeldorCorruptedItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge the blade? <br> You will not get back any of the shards.",
                new DialogueOption(
                    "Yes, turn it back into a normal Blade of Saeldor!",
                    (c, s) -> item.replace(new Item(ItemId.BLADE_OF_SAELDOR_INACTIVE))),
                new DialogueOption("No!")));
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var blades =
        ItemHandler.matchId(
            useItem,
            onItem,
            ItemId.BLADE_OF_SAELDOR_C,
            ItemId.BLADE_OF_SAELDOR_C_25870,
            ItemId.BLADE_OF_SAELDOR_C_25872,
            ItemId.BLADE_OF_SAELDOR_C_25874,
            ItemId.BLADE_OF_SAELDOR_C_25876,
            ItemId.BLADE_OF_SAELDOR_C_25878,
            ItemId.BLADE_OF_SAELDOR_C_25880,
            ItemId.BLADE_OF_SAELDOR_C_25882);
    if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_HEFIN)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_ITHELL)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25870));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_IORWERTH)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25872));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_TRAHAEARN)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25874));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_CADARN)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25876));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_CRWYS)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25878));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_MEILYR)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25880));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, blades, ItemId.CRYSTAL_OF_AMLODD)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Blade of saeldor (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BLADE_OF_SAELDOR_C_25882));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    }
    return false;
  }
}
