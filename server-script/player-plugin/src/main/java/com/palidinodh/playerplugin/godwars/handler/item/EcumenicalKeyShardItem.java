package com.palidinodh.playerplugin.godwars.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ECUMENICAL_KEY_SHARD)
class EcumenicalKeyShardItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (item.getAmount() < 50) {
      player.getGameEncoder().sendMessage("You need 50 shards to combine them.");
      return;
    }
    item.remove(50);
    player.getInventory().addOrDropItem(ItemId.ECUMENICAL_KEY);
  }
}
