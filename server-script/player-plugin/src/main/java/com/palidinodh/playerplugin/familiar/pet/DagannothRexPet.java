package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class DagannothRexPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.PET_DAGANNOTH_REX, NpcId.DAGANNOTH_REX_JR_6641, NpcId.DAGANNOTH_REX_JR));
    return builder;
  }
}
