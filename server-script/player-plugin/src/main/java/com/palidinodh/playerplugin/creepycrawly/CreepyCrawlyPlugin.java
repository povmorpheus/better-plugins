package com.palidinodh.playerplugin.creepycrawly;

import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import lombok.Getter;
import lombok.Setter;

public class CreepyCrawlyPlugin implements PlayerPlugin {

  @Getter @Setter private int points;

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("creepy crawly points")) {
      return points;
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("creepy crawly points")) {
      points += amount;
    }
  }
}
