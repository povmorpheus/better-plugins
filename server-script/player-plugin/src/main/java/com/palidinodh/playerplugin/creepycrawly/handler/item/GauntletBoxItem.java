package com.palidinodh.playerplugin.creepycrawly.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.GAUNTLET_BOX_60057)
public class GauntletBoxItem implements ItemHandler {

  private static final List<RandomItem> REGULAR_ITEMS =
      RandomItem.buildList(
          new RandomItem(ItemId.RUNE_FULL_HELM_NOTED, 2, 4),
          new RandomItem(ItemId.RUNE_CHAINBODY_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PLATELEGS_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PLATESKIRT_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_HALBERD_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PICKAXE_NOTED, 1, 2),
          new RandomItem(ItemId.DRAGON_HALBERD_NOTED),
          new RandomItem(ItemId.COSMIC_RUNE, 160, 240),
          new RandomItem(ItemId.NATURE_RUNE, 100, 140),
          new RandomItem(ItemId.LAW_RUNE, 80, 140),
          new RandomItem(ItemId.CHAOS_RUNE, 180, 300),
          new RandomItem(ItemId.DEATH_RUNE, 100, 160),
          new RandomItem(ItemId.BLOOD_RUNE, 80, 140),
          new RandomItem(ItemId.MITHRIL_ARROW, 800, 1_200),
          new RandomItem(ItemId.ADAMANT_ARROW, 400, 600),
          new RandomItem(ItemId.RUNE_ARROW, 200, 300),
          new RandomItem(ItemId.DRAGON_ARROW, 30, 80),
          new RandomItem(ItemId.UNCUT_SAPPHIRE_NOTED, 20, 60),
          new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 10, 50),
          new RandomItem(ItemId.UNCUT_RUBY_NOTED, 5, 30),
          new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 3, 6),
          new RandomItem(ItemId.BATTLESTAFF_NOTED, 4, 8),
          new RandomItem(ItemId.COINS, 20_000, 80_000));

  private static void openRegularBox(Player player) {
    var logName = "The Gauntlet";
    var count = player.getCombat().logNPCKill(logName, -1);
    player.getInventory().deleteItem(ItemId.GAUNTLET_BOX_60057);
    player.getInventory().addOrDropItem(ItemId.CRYSTAL_SHARD, PRandom.randomI(3, 7));
    for (var i = 0; i < 2; i++) {
      player.getInventory().addOrDropItem(RandomItem.getItem(REGULAR_ITEMS));
    }
    if (PRandom.randomE(25) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.ELITE.getBoxId());
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(120, ItemId.CRYSTAL_WEAPON_SEED, NpcId.YOUNGLLEF))) {
      player.getCombat().logNPCItem(logName, ItemId.CRYSTAL_WEAPON_SEED, 1, count);
      player.getInventory().addOrDropItem(ItemId.CRYSTAL_WEAPON_SEED);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(120, ItemId.CRYSTAL_ARMOUR_SEED, NpcId.YOUNGLLEF))) {
      player.getCombat().logNPCItem(logName, ItemId.CRYSTAL_ARMOUR_SEED, 1, count);
      player.getInventory().addOrDropItem(ItemId.CRYSTAL_ARMOUR_SEED);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(2_000, ItemId.ENHANCED_CRYSTAL_WEAPON_SEED, NpcId.YOUNGLLEF))) {
      player.getCombat().logNPCItem(logName, ItemId.ENHANCED_CRYSTAL_WEAPON_SEED, 1, count);
      player.getInventory().addOrDropItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
    }
    if (PRandom.inRange(
        1, player.getCombat().getDropRateDenominator(2_000, ItemId.YOUNGLLEF, NpcId.YOUNGLLEF))) {
      player.getCombat().logNPCItem(logName, ItemId.YOUNGLLEF, 1, count);
      player.getInventory().addOrDropItem(ItemId.YOUNGLLEF);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(4_000, ItemId.CORRUPTED_YOUNGLLEF, NpcId.YOUNGLLEF))) {
      player.getCombat().logNPCItem(logName, ItemId.CORRUPTED_YOUNGLLEF, 1, count);
      player.getInventory().addOrDropItem(ItemId.CORRUPTED_YOUNGLLEF);
    }
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "open":
        openRegularBox(player);
        break;
    }
  }
}
