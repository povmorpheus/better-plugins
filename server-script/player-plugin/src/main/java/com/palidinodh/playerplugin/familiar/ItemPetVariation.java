package com.palidinodh.playerplugin.familiar;

import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;

public interface ItemPetVariation {

  void run(Player player, Npc npc, Item item);
}
