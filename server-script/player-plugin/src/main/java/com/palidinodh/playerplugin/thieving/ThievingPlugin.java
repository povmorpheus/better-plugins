package com.palidinodh.playerplugin.thieving;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.random.PRandom;
import lombok.Getter;

public class ThievingPlugin implements PlayerPlugin {

  private static final int DODGY_NECKLACE_CHARGES = 10;

  @Inject private transient Player player;

  @Getter private int dodgyNecklaceCharges = DODGY_NECKLACE_CHARGES;

  public void breakDodgyNecklace() {
    if (dodgyNecklaceCharges == DODGY_NECKLACE_CHARGES) {
      player.getGameEncoder().sendMessage("Your dodgy necklace is fully charged.");
      return;
    }
    dodgyNecklaceCharges = DODGY_NECKLACE_CHARGES;
    player.getEquipment().setItem(Equipment.Slot.NECK, null);
  }

  public boolean deincrimentDodgyNecklaceCharges() {
    if (player.getEquipment().getNeckId() != ItemId.DODGY_NECKLACE) {
      return false;
    }
    if (!PRandom.inRange(1, 4)) {
      return false;
    }
    if (--dodgyNecklaceCharges > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "Your dodgy necklace protects you. <col=ff0000>It has "
                  + dodgyNecklaceCharges
                  + " charges left.</col>");
      return true;
    }
    dodgyNecklaceCharges = DODGY_NECKLACE_CHARGES;
    player.getEquipment().setItem(Equipment.Slot.NECK, null);
    player
        .getGameEncoder()
        .sendMessage(
            "Your dodgy necklace protects you. <col=ff0000>It then crumbles to dust.</col>");
    return true;
  }
}
