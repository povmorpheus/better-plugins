package com.palidinodh.worldevent.grandexchangepawnshop.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.grandexchangepawnshop.GrandExchangePawnShopEvent;

@ReferenceName("pawnshop")
class PawnShopCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Inject private GrandExchangePawnShopEvent event;

  @Override
  public String getExample(String name) {
    return "on/off";
  }

  @Override
  public void execute(Player player, String name, String message) {
    event.setEnabled(message.equals("on"));
    player.getGameEncoder().sendMessage("Pawn shop: " + event.isEnabled());
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::pawnshop " + message);
  }
}
