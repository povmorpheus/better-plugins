package com.palidinodh.worldevent.groupboss.handler.mapobject;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.groupboss.GroupBossEvent;

@ReferenceId(ObjectId.CAVE_ENTRANCE_11833)
class CaveEntranceMapObject implements MapObjectHandler {

  @Inject private GroupBossEvent event;

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Jad Event (Safe)",
            (c, s) -> {
              event.enterTeleport(player);
            }),
        new DialogueOption(
            "Fight Cave",
            (c, s) -> {
              player.openOldDialogue("tzhaar", 0);
            }));
  }
}
