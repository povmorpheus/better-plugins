package com.palidinodh.worldevent.creepycrawly;

import com.palidinodh.util.PTime;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CreepyCrawlyAbilityType {
  REGENERATE(3, (int) PTime.secToTick(15)),
  SPECIAL_ATTACK_RESTORE(7, (int) PTime.secToTick(60)),
  HURRICANE(11, (int) PTime.secToTick(10)),
  OMNIPOWER(15, (int) PTime.secToTick(30));

  private final int optionId;
  private final int cooldown;

  public int getEnableId() {
    return optionId + 1;
  }

  public int getDisableId() {
    return optionId + 2;
  }

  public int getTextId() {
    return optionId + 3;
  }

  public static CreepyCrawlyAbilityType getByOptionId(int id) {
    for (var type : values()) {
      if (id != type.getOptionId()) {
        continue;
      }
      return type;
    }
    return null;
  }
}
