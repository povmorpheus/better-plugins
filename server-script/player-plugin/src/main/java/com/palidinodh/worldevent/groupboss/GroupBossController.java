package com.palidinodh.worldevent.groupboss;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.tile.Tile;

public class GroupBossController extends PController {

  @Inject private Player player;
  private Npc jad;

  public GroupBossController(Npc jad) {
    this.jad = jad;
  }

  @Override
  public void startHook() {
    player.setLargeVisibility();
    setExitTile(new Tile(player));
    setTeleportsDisabled(true);
    setKeepItemsOnDeath(true);
    setStopWithDeath(true);
  }

  @Override
  public void stopHook() {
    player.restore();
    player.setDefaultVisibility();
    player.getWidgetManager().removeOverlay();
    player.getWidgetManager().removeFullOverlay();
  }
}
