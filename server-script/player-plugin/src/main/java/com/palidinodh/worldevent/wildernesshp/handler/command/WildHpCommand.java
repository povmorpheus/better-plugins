package com.palidinodh.worldevent.wildernesshp.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.wildernesshp.WildernessHpEvent;

@ReferenceName("wildhp")
class WildHpCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private WildernessHpEvent event;

  @Override
  public String getExample(String name) {
    return "true/false";
  }

  @Override
  public void execute(Player player, String name, String message) {
    event.setEnabled(Boolean.parseBoolean(message));
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::wildhp " + message);
  }
}
