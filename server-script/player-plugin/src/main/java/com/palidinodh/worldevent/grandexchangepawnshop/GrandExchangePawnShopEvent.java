package com.palidinodh.worldevent.grandexchangepawnshop;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.item.Shop;
import com.palidinodh.osrscore.model.item.ShopItemList;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.util.PCollection;
import lombok.Getter;
import lombok.Setter;

public class GrandExchangePawnShopEvent extends WorldEvent {

  @Inject private transient World world;
  @Getter @Setter private transient boolean enabled = true;

  @Getter private Shop shop = new Shop();

  public GrandExchangePawnShopEvent() {
    super(1);
  }

  @Override
  public void load() {
    shop.setName("Pawn Shop");
    shop.setReferenceName("pawn_shop");
    shop.setBuyType(Shop.BuyType.ALL_TRADABLE);
    shop.setBuyExchangeMultiplier(0.8);
    shop.setSellExchangeMultiplier(1.2);
    shop.setSort(Shop.Sort.ACTIVITY_50K);
    shop.setDeclinedItems(DeclinedItems.LIST);
    shop.setMaintainPlayerStockValue(50_000);
    if (shop.getSections() == null) {
      shop.setSections(PCollection.toList(new ShopItemList()));
    }
    shop.getSections().forEach(s -> s.capacity(256));
  }

  @Override
  public void execute() {
    shop.tick();
    var section = shop.getSections().get(0);
    while (section.size() >= section.getCapacity()) {
      section.removeItem(section.size() - 1);
    }
  }
}
