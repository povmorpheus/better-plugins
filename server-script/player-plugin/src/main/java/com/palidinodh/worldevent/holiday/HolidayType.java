package com.palidinodh.worldevent.holiday;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.model.tile.Tile.Direction;
import com.palidinodh.util.PString;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HolidayType {
  EASTER(
      ItemId.EGG_TOKEN_60037,
      new NpcSpawn(new Tile(3092, 3516), NpcId.BUNNY_MAN),
      new NpcSpawn(64, new Tile(2135, 5534, 7), NpcId.BUNNY_16062),
      null,
      new Tile(2143, 5525, 7)),
  HALLOWEEN(
      ItemId.PUMPKIN_TOKEN_32338,
      new NpcSpawn(new Tile(3092, 3516), NpcId.DEATH_16050),
      new NpcSpawn(64, new Tile(3036, 5361, 4), NpcId.FRAGMENT_OF_SEREN_16049),
      null,
      new Tile(3038, 5347, 4)),
  CHRISTMAS(
      ItemId.SNOWBALL_TOKEN_32339,
      new NpcSpawn(new Tile(3092, 3516), NpcId.JACK_FROST_16052),
      new NpcSpawn(32, new Tile(2778, 3858, 4), NpcId.PENGUIN_16051),
      Arrays.asList(
          new NpcSpawn(Direction.WEST, new Tile(2775, 3836, 4), NpcId.PING),
          new NpcSpawn(Direction.WEST, new Tile(2775, 3837, 4), NpcId.PONG)),
      new Tile(2773, 3836, 4));

  private final int itemId;
  private final NpcSpawn shopNpcSpawn;
  private final NpcSpawn bossNpcSpawn;
  private final List<NpcSpawn> bossSupportNpcSpawns;
  private final Tile bossTeleportTile;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace('_', ' '));
  }
}
