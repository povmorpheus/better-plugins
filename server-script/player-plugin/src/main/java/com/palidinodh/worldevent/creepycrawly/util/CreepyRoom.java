package com.palidinodh.worldevent.creepycrawly.util;

import com.palidinodh.osrscore.model.tile.Tile;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Singular;

@Builder
@Getter
public class CreepyRoom {

  private int id;
  @Singular private List<Tile> npcSpawns;
  @Singular private List<Tile> chests;
  @Setter private boolean unlocked;
}
