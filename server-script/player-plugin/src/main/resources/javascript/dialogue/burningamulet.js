var entries = new ArrayList();
var title = "";
var lines = new ArrayList();
var actions = new ArrayList();

title = "Select an Option";
lines.add("Chaos Temple <col=ff0000>(Wilderness)</col>");
actions.add("close|script");
lines.add("Bandit Camp <col=ff0000>(Wilderness)</col>");
actions.add("close|script");
lines.add("Lava Maze <col=ff0000>(Wilderness)</col>");
actions.add("close|script");
var obj0 = new DialogueEntry();
entries.add(obj0);
obj0.setSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

instance = new DialogueScript() {
    execute: function(player, index, childId, slot) {
        if (player.isLocked()) {
            return;
        }
        if (index == 0) {
            if (!player.getController().canTeleport(30, true)) {
                return;
            }
            var tile = null;
            if (slot == 0) {
                player.getMovement().animatedTeleport(new Tile(3233, 3637), Magic.NORMAL_MAGIC_ANIMATION_START,
                Magic.NORMAL_MAGIC_ANIMATION_END, Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
                player.getController().stopWithTeleport();
                player.getCombat().clearHitEvents();
            } else if (slot == 1) {
                player.getMovement().animatedTeleport(new Tile(3039, 3651), Magic.NORMAL_MAGIC_ANIMATION_START,
                Magic.NORMAL_MAGIC_ANIMATION_END, Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
                player.getController().stopWithTeleport();
                player.getCombat().clearHitEvents();
            } else if (slot == 2) {
                player.getMovement().animatedTeleport(new Tile(3027, 3839), Magic.NORMAL_MAGIC_ANIMATION_START,
                Magic.NORMAL_MAGIC_ANIMATION_END, Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
                player.getController().stopWithTeleport();
                player.getCombat().clearHitEvents();
            }
        }
    },

    getDialogueEntries: function() {
        return entries;
    }
}
