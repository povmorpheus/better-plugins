package com.palidinodh.command.overseer;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("barricade")
class BarricadeCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    return "- adds a barricade under you";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.getController().addMapObject(new MapObject(ObjectId.BARRICADE, 10, 0, player));
    player.getGameEncoder().sendMessage("Added a barricade.");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " added a barricade.");
  }
}
