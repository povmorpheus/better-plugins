package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.playstyle.PlayStylePlugin;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("setdiff")
class SetDifficultyCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var player2 = player.getWorld().getPlayerByUsername(username);
    var difficulty = 0;
    if (messages.length == 2) {
      difficulty = Integer.parseInt(messages[1]);
    }
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    }
    var plugin = player2.getPlugin(PlayStylePlugin.class);
    switch (difficulty) {
      case 0:
        plugin.setDifficultyMode(RsDifficultyMode.NORMAL);
        player2
            .getGameEncoder()
            .sendMessage("Your difficulty has been set to Normal mode by " + player.getUsername());
        player
            .getGameEncoder()
            .sendMessage("You set " + player2.getUsername() + " difficulty to Normal");
        break;
      case 1:
        plugin.setDifficultyMode(RsDifficultyMode.HARD);
        player2
            .getGameEncoder()
            .sendMessage("Your difficulty has been set to Hard mode by " + player.getUsername());
        player
            .getGameEncoder()
            .sendMessage("You set " + player2.getUsername() + " difficulty to Hard");
        break;
      case 2:
        plugin.setDifficultyMode(RsDifficultyMode.ELITE);
        player2
            .getGameEncoder()
            .sendMessage("Your difficulty has been set to Elite mode by " + player.getUsername());
        player
            .getGameEncoder()
            .sendMessage("You set " + player2.getUsername() + " difficulty to Elite");
        break;
      default:
        player.getGameEncoder().sendMessage("0=Normal, 1=Hard, 2=Elite");
        break;
    }
  }
}
