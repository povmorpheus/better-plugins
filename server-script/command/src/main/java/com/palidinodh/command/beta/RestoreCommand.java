package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("restore")
class RestoreCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    player.restore();
    player.rejuvenate();
    player.getCombat().setSpecialAttackAmount(PCombat.MAX_SPECIAL_ATTACK);
    player.getGameEncoder().sendMessage("You feel restored..");
  }
}
