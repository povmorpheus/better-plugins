package com.palidinodh.command.all;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.BadWords;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;

@ReferenceName("yell")
class YellCommand implements CommandHandler {

  @Override
  public String getExample(String name) {
    return "- Yells out your message";
  }

  @Override
  public boolean canUse(Player player) {
    return player.isUsergroup(UserRank.SAPPHIRE_MEMBER)
        || player.isUsergroup(UserRank.OVERSEER)
        || player.isStaff();
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (player.getMessaging().getYellDelay() > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need to wait "
                  + PTime.tickToSec(player.getMessaging().getYellDelay())
                  + " seconds before you can yell again.");
      return;
    }
    if (player.getMessaging().isYellMuted()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You were yellmuted by "
                  + player.getMessaging().getYellMutedByUsername()
                  + " for "
                  + player.getMessaging().getMutedYellMinutes()
                  + " minutes");
      player
          .getGameEncoder()
          .sendMessage(
              "You need to wait "
                  + player.getMessaging().yellMuteLeft()
                  + " minutes before you can yell again.");
      return;
    }
    if (player.inJail()) {
      player.getGameEncoder().sendMessage("You can not yell while in jail..");
      return;
    }
    if (player.getMessaging().isMuted()) {
      player.getGameEncoder().sendMessage("You can not yell while muted.");
      return;
    }
    if (BadWords.containsBadWord(message)) {
      player.getGameEncoder().sendMessage("Please don't use that word.");
      return;
    }
    if (!player.isUsergroup(UserRank.ADMINISTRATOR)) {
      message = PString.cleanString(PString.removeHtml(message));
    }
    if (message.contains("a funny feeling")) {
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>You have a funny feeling like you're being followed.</col>");
      return;
    }
    var yellDelay = player.getPlugin(BondPlugin.class).getDonatorRank().getYellCooldown();
    if (yellDelay <= 0 && !player.isStaff()) {
      return;
    }
    if (player.isStaff()) {
      yellDelay = (int) PTime.secToTick(5);
    }
    player
        .getWorld()
        .sendMessage(
            player,
            "[<col=ff0000>Yell</col>] "
                + player.getMessaging().getIconImage()
                + player.getUsername()
                + ": "
                + message);
    player.getMessaging().setYellDelay(yellDelay);
    player.log(PlayerLogType.YELL, message);
    DiscordBot.sendMessage(DiscordChannel.YELL_CHAT_LOG, player.getUsername() + ": " + message);
  }
}
