package com.palidinodh.command.seniormod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("resetlevel")
class ResetLevelCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "\"username or userid\" skill_name";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0];
    var skillName = messages[1].toLowerCase();
    var skillId = -1;
    for (var i = 0; i < Skills.SKILL_NAMES.length; i++) {
      var aSkillName = Skills.SKILL_NAMES[i].toLowerCase();
      if (aSkillName.isEmpty()) {
        continue;
      }
      if (!skillName.equals(aSkillName)) {
        continue;
      }
      skillId = i;
      break;
    }
    if (skillId == -1) {
      player.getGameEncoder().sendMessage("Unable to find the skill.");
      return;
    }
    var targetPlayer = player.getWorld().getPlayerByUsername(username);
    if (targetPlayer == null) {
      var userID = -1;
      userID = Integer.parseInt(username);
      if (userID != -1) {
        targetPlayer = player.getWorld().getPlayerById(userID);
      }
    }
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username);
      return;
    }
    targetPlayer
        .getGameEncoder()
        .sendMessage(player.getUsername() + " has reset your " + Skills.SKILL_NAMES[skillId] + ".");
    player
        .getGameEncoder()
        .sendMessage(
            "You have reset "
                + targetPlayer.getUsername()
                + "'s "
                + Skills.SKILL_NAMES[skillId]
                + ".");
    var skillLevel = skillId == Skills.HITPOINTS ? 10 : 1;
    targetPlayer.getSkills().setXP(skillId, Skills.XP_TABLE[skillLevel]);
    targetPlayer.getSkills().setLevel(skillId, skillLevel);
    targetPlayer.getSkills().setCombatLevel();
    targetPlayer.getGameEncoder().sendSkillLevel(skillId);
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername()
            + " has reset "
            + Skills.SKILL_NAMES[skillId]
            + " for "
            + targetPlayer.getUsername()
            + ".");
  }
}
