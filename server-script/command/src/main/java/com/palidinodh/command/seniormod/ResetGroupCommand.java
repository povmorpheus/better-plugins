package com.palidinodh.command.seniormod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.playstyle.PlayStylePlugin;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("resetgroup")
class ResetGroupCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var player2 = player.getWorld().getPlayerByUsername(message);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    var plugin = player2.getPlugin(PlayStylePlugin.class);
    var group = plugin.getGroup();
    if (group == null) {
      player.getGameEncoder().sendMessage(player2.getUsername() + " isn't in a group.");
      return;
    }
    if (group.isEmpty()) {
      player.getGameEncoder().sendMessage(player2.getUsername() + " has never join a group.");
      return;
    }
    group.clear();
    player2
        .getGameEncoder()
        .sendMessage("Your ironman group has been reset by " + player.getUsername());
    player.getGameEncoder().sendMessage("Success");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername() + " reset ironman group for " + player2.getUsername() + ".");
  }
}
