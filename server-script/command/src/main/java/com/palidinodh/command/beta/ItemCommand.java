package com.palidinodh.command.beta;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PNumber;

@ReferenceName("item")
class ItemCommand implements CommandHandler, Beta {

  @Override
  public String getExample(String name) {
    return "id_or_name (quantity)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var id =
        messages[0].matches("[0-9]+")
            ? Integer.parseInt(messages[0])
            : ItemId.valueOf(messages[0].replace(" ", "_").toUpperCase());
    if (id == -1) {
      player.getGameEncoder().sendMessage("Couldn't find item.");
      return;
    }
    var amount = messages.length == 2 ? PNumber.getNumber(messages[1]) : 1;
    player.getInventory().addItem(id, amount);
  }
}
