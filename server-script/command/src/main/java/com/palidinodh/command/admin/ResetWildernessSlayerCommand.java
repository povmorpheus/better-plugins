package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("resetwildslayer")
class ResetWildernessSlayerCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var player2 = player.getWorld().getPlayerByUsername(message);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    player2.pluginScript("slayer_reset_wilderness_task");
    player2
        .getGameEncoder()
        .sendMessage("Your wilderness slayer task has been reset by " + player.getUsername());
    player.getGameEncoder().sendMessage("Success");
  }
}
