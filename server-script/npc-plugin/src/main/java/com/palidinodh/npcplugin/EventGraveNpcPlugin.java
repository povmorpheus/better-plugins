package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.GRAVE_16061)
class EventGraveNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;
  private int despawnDelay = 25;

  @Override
  public void tick() {
    if (despawnDelay-- == 0) {
      npc.getWorld().removeNpc(npc);
    }
  }
}
